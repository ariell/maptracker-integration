# Dependencies:
# apt install jq
# apt install termux-api
# apt install curl


#url="http://10.23.9.150:8080/data/platformdata.api.Platform/ANDROID.TERMUX"
url="https://maptracker.live/data/platformdata.api.Platform/ANDROID.TERMUX"

while :
do
    echo "Getting new fix..."
    # Testing
    #data='{"latitude":20.85898936, "longitude": -154.72251273, "accuracy": 15.0, "bearing": 42.9, "speed": 1}'

    # Android
    data=$(termux-location)

    lat=$(echo -e $data | jq '.latitude' | awk '{printf "%0.8f", $0}')
    lon=$(echo -e $data | jq '.longitude' | awk '{printf "%0.8f", $0}')
    uncertainty=$(echo -e $data | jq '.accuracy' | awk '{printf "%0.1f", $0}')
    heading=$(echo -e $data | jq '.bearing' | awk '{printf "%0.1f", $0}')
    speed=$(echo -e $data | jq '.speed' | awk '{printf "%0.1f", $0}')

    payload="{\"pose\":{\"lat\":$lat,\"lon\":$lon,\"uncertainty\":$uncertainty,\"heading\":$heading,\"speed\":$speed},\"alert\":{},\"stat\":{}}"

    echo "Posting to API..."
    curl -H "Content-Type: application/json" -X POST -d $payload $url
	sleep 10
done

