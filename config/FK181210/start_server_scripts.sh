#!/usr/bin/env bash
# Start maptracker along with all supporting processes
# Used for SOI FALKOR Cruise FK180119


cd ~/themaptracker
screen -dmS maptracker_server env/bin/python ~/themaptracker/start_webserver.py

cd ~/maptracker-integration
screen -dmS maptracker_tddocserver env/bin/python platform_scripts/telodyne_slocumglider_apipost.py -u http://10.23.10.205 --docserver 10.23.9.58 --gliderid nemesis --gliderid sentinel
screen -dmS maptracker_whoidocserver env/bin/python platform_scripts/telodyne_slocumglider_apipost.py -u http://10.23.10.205 --docserver 128.128.102.141 --gliderid sentinel
#screen -dmS maptracker_soiais env/bin/python platform_scripts/ais_receiver_udp_apipost.py -a 10005 -u http://localhost -m 319005600 -t 983191003=ATREU.AIS
screen -dmS maptracker_soifalkor env/bin/python platform_scripts/soi_falkor_udp_apipost.py -u http://10.23.10.205 --recvport 38014
screen -dmS maptracker_subastian env/bin/python platform_scripts/soi_subastien_apipost.py -u http://10.23.10.205
screen -dmS maptracker_slocummsn env/bin/python platform_scripts/load_csv_missions_from_dirs.py -d 300 -k SLOCUM --base_path /mnt/nfs/ParticipantData/afriedman/maptracker_support/slocum_missions/ -c yellow
screen -dmS maptracker_subastianmsn env/bin/python platform_scripts/load_csv_missions_from_dirs.py -d 300 -k ROV.SUBASTIEN -u http://10.23.10.205 --base_path /mnt/nfs/ParticipantData/afriedman/maptracker_support/subastian_missions/ -c yellow
screen -dmS maptracker_sqannotations env/bin/python platform_scripts/load_squidle_annotations.py -u http://10.23.10.205 -k SQ.ANNOTATIONS -d 300


screen -dmS maptracker_cloudpost_platforms -t FALKOR.SHIP_STATUS -t ROV.SUBASTIEN -t SLOCUM.nemesis -t SLOCUM.sentinel -d 10
screen -dmS maptracker_cloudpost_missions env/bin/python platform_scripts/cloudpost_remote_platforms.py  -m ROV.SUBASTIEN -m SLOCUM -m ROV.SUBASTIEN-old -m SLOCUM-old -d 60
screen -dmS maptracker_cloudpost_sqannotations env/bin/python platform_scripts/cloudpost_remote_platforms.py  -m SQ.ANNOTATIONS -d 600


#screen -dmS maptracker_soisubastien env/bin/python platform_scripts/soi_falkor_udp_apipost.py -u http://10.23.10.205 --recvport 38014

# Also need start ACFR script in LCM environment
