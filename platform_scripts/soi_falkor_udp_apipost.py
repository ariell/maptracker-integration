#from __future__ import print_function
import socket
from contextlib import closing
import thread

from platform_tools.platformdata import APIPlatform, get_args, parser

PLATFORM_KEY = "FALKOR.SHIP_STATUS"
DEBUG = False

default_port = 38014

parser.add_argument("-r", "--recvport", default=default_port, type=int, help="Port to receive UDP data (default: %d" % default_port)


class SOIFalkorPlatform(APIPlatform):
    def receive_udp_data(self, platform_key, port):
        """
        NOTE: This should be a thread that subscribes to a UDP feed or similar to receive nav updates. It should
        construct the nav data object as done here, but when new data is received.
        """
        host = ''
        #port = 38014
        bufsize = 4096
        heading = 0.0
        speed = 0.0

        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        with closing(sock):
            sock.bind((host, port))
            while True:
                data_raw = sock.recv(bufsize).decode('utf-8')
                try:
                    if data_raw.startswith("$INHDT"):
                        heading = float(data_raw.split(",")[1])         # hdg in deg
                    elif data_raw.startswith("$INVTG"):
                        speed = float(data_raw.split(",")[5])*0.514444  # speed in  m
                    elif data_raw.startswith("$INGGA"):
                        vals = data_raw.split(",")
                        lat_sign = -1 if vals[3] == 'S' else +1
                        lon_sign = -1 if vals[5] == 'W' else +1
                        lat = float(vals[2][:2]) + float(vals[2][2:])/60
                        lon = float(vals[4][:3]) + float(vals[4][3:])/60
                        uncertainty = float(vals[8])
                        data = {
                            "alert": {},
                            "pose": {
                                "lat": lat*lat_sign,  # float, decimal degrees
                                "lon": lon*lon_sign,  # float, decimal degrees
                                "depth": 0.0,         # float, m
                                "heading": heading,   # float, degrees, range: 0:360
                                "speed": speed,       # float, m/s
                                "uncertainty": uncertainty  # float, m
                            },
                            "stat": {}
                        }
                        self.update_platform_data(platform_key, data)
                        if DEBUG:
                            print(data)
                except Exception as e:
                    print("Unable to decode: {}".format(data_raw))


if __name__ == '__main__':
    # get cli args
    args = parser.parse_args()

    # Instantiate class
    platform = SOIFalkorPlatform(maptracker_url=args.url, maptracker_port=args.port, command_server_port=args.command_port)

    # Start data threads
    platform.receive_udp_data(PLATFORM_KEY, args.recvport)
