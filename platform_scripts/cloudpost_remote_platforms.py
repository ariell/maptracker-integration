
from platform_tools.platformdata import post_data
from time import sleep
import requests
import argparse

# "targets/acfr-ship-falkor.json",
# "targets/acfr-nga.json",
# "targets/acfr-nga.usbl.json",
# "targets/umich-iver.json",
# "targets/umich-iver.usbl.json",
# "targets/umich-iver.iridium.json",
# "targets/whoi-slocum.json",
# "targets/acfr-nav-orangeBox.json",
# "targets/acfr-sirius.usbl.json",
# "targets/acfr-sirius.json",
# "targets/uri-lfloat.json",
# "targets/uri-lfloat.usbl.json",
# "targets/acfr-holt.json",
# "targets/acfr-holt.usbl.json",

REMOTE_URL = "http://130.211.155.120"
REMOTE_PORT = 8080
LOCAL_URL = "http://localhost"
LOCAL_PORT = 8080

TARGET_URL_PTN = "{local_url}:{local_port}/data/platformdata.api.Platform/{key}"
MISSION_URL_PTN = "{local_url}:{local_port}/get_mission/platformdata.api.Platform/{key}"

platform_keys = ["FALKOR.SHIP_STATUS", "ROV.SUBASTIEN", "SLOCUM.nemesis", "SLOCUM.sentinel", "ATREU.AIS", "ANDROID.TERMUX"]
mission_keys = ["ROV.SUBASTIEN", "SLOCUM", "ROV.SUBASTIEN-old", "SLOCUM-old", "SQ.ANNOTATIONS"]

parser = argparse.ArgumentParser()
parser.add_argument('--base_path', default="/mnt/nfs/ParticipantData/afriedman/maptracker_support/slocum_sfmc_scrapes", type=str, help="Full path to mission directories")
parser.add_argument("-r", "--remote_url", default=REMOTE_URL, type=str, help="Remote url to post data (default %s)" % REMOTE_URL)
parser.add_argument("--remote_port", default=REMOTE_PORT, type=int, help="Remote port to post data (default %d)" % REMOTE_PORT)
parser.add_argument("-l", "--local_url", default=LOCAL_URL, type=str, help="Local url to retrieve data (default %s)" % LOCAL_URL)
parser.add_argument("--local_port", default=REMOTE_PORT, type=int, help="Local port to retrieve data (default %d)" % LOCAL_PORT)
parser.add_argument("-t", "--target_key", default=[], type=str, help="Add target keys, eg: -t {}".format(" -t ".join(platform_keys)), action='append', dest='target_keys')
parser.add_argument("-m", "--mission_key", default=[], type=str, help="Add target keys, eg: -m {}".format(" -m ".join(mission_keys)), action='append', dest='mission_keys')
parser.add_argument("-d", "--delay", default=10, type=int, help="Delay between updates (default 10)")


# Target
# http://localhost:8080/data/platformdata.api.Platform/DROPIVER.IRIDIUM
# Missions
# http://localhost:8080/get_mission/platformdata.api.Platform/ROV.SUBASTIEN
target_data = {}

def post_key_list(key_list, remote_url, remote_port, local_url, local_port, data_type="platform", url_pattern=TARGET_URL_PTN):
    global target_data
    for k in key_list:
        try:
            data = requests.get(url_pattern.format(local_url=local_url, local_port=local_port, key=k)).json()
            cache_key = "platform-"+k
            do_post = target_data.get(cache_key, 0) < data.get("msgts", 0)
            if do_post:
                post_data(k, data, data_type=data_type, maptracker_url=remote_url, maptracker_port=remote_port)
                target_data[cache_key] = data.get("msgts", 0)
            else:
                print ("Not updating {}, msgts={}".format(k, data.get("msgts")))
        except Exception as e:
            print ("*** ERROR posting to remote: {} ({})! - {}".format(k, "platform", e))



args = parser.parse_args()

print(args.target_keys)
print(args.mission_keys)

while True:

    # post to remote
    print ("Posting platforms...")
    post_key_list(args.target_keys, args.remote_url, args.remote_port, args.local_url, args.local_port,
                  data_type="platform", url_pattern=TARGET_URL_PTN)
    print ("Posting missions...")
    post_key_list(args.mission_keys, args.remote_url, args.remote_port, args.local_url, args.local_port,
                  data_type="mission", url_pattern=MISSION_URL_PTN)
    sleep(args.delay)
