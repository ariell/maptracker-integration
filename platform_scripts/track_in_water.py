from platform_tools.platformdata import post_data
from time import sleep
import requests
import lcm
import sys

LCMROOT ='/home/auv/git/acfr-lcm'
sys.path.append('{0}/build/lib/python{1}.{2}/dist-packages/perls/lcmtypes/'.format(LCMROOT, sys.version_info[0], sys.version_info[1]))

from senlcm import evologics_ping_control_t

settings = {
    "server": "10.23.10.205",
    "port": 8080,
    "update_interval": 10,
    "channel" : "EVOLOGICS_PING_CONTROL",
    "data": [
        # platforms
        {"id":  2, "ping_rate": 10, "send_fixes": False, "url": "http://{server}:{port}/data/platformdata.api.Platform/FALKOR.USBL_FIX.PFLOAT"},
        {"id": 12, "ping_rate": 10, "send_fixes": False, "url": "http://{server}:{port}/data/platformdata.api.Platform/FALKOR.USBL_FIX.DROPIVER"},
        {"id": 10, "ping_rate": 10, "send_fixes": True, "url": "http://{server}:{port}/data/platformdata.api.Platform/FALKOR.USBL_FIX.SIRIUS"},
    ]
}


if __name__ == "__main__":
    msg = evologics_ping_control_t()
    lc = lcm.LCM()
    while True:
        for t in settings['data']:
            try:
                url = t["url"].format(**settings)
                r = requests.get(url)
                data = r.json()

                msg.target_id = t['id']
                msg.ping_rate = t['ping_rate']
                msg.send_fixes = t['send_fixes']

                if data["state"] == "online":
                    msg.in_water = True
                elif data["state"] == "stationary" and data["statemsg"] == "Deployed!":
                    msg.in_water = True
                else:
                    msg.in_water = False

                print t['id'], msg.in_water
                lc.publish(settings["channel"], msg.encode())
            except Exception as e:
                print "Except:", e
        sleep(settings["update_interval"])
