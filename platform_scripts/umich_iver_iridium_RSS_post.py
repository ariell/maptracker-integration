
from time import sleep
from platform_tools.platformdata import APIPlatform, get_args
import pprint
import feedparser
from parse import parse, search

platform_key = 'DROPIVER.IRIDIUM'
RSS_URL = 'https://zapier.com/engine/rss/2895871/dropiveriridium/'
DELAY = 60

class UMIverIridium(APIPlatform):
    def start_threads(self, platform_key):
        last_heard_time = None
        while True:
            try:
                d = feedparser.parse(url)
                message_body = d.entries[0].description
                message_time = d.entries[0].published

                if (message_time != last_heard_time):
                    mission_data = {
                        'pose': {
                            'lat': float(search("lat={}, ", message_body)[0]),
                            'lon': float(search("long={},", message_body)[0]),
                            'uncertainty': float(11),
                            'speed': float(search("spd={},", message_body)[0])*0.514444,   # convert knots to m/s
                            'heading': float(search("hdg={},", message_body)[0])
                        },
                        'stat': {
                            # "sent-UTC": data["timestamp"],
                            "received": message_time,
                            'bat': int(float(search("bat={},", message_body)[0])),
                            'mode': search("mode={},", message_body)[0],
                            'wpt#': search("Wp={},", message_body)[0],
                            # 'sent': message_time,
                            # "error": data["Error"]
                        },
                        'alert': {}
                    }
                    # pprint.pprint(mission_data)
                    last_heard_time = message_time
                    #pprint.pprint(mission_data)
                    self.update_platform_data(platform_key, mission_data)
            except Exception as e:
                err_str = 'Issue with fetching RSS data: %s' %repr(e)
                print(err_str)
                self.log_platform_message(platform_key, 'warning', err_str)
            sleep(DELAY)


if __name__ == "__main__":

    # get cli args
    args = get_args()

    # Instantiate class
    platform = UMIverIridium(maptracker_url=args["url"], maptracker_port=args["port"])

    # Start data threads
    platform.start_threads(platform_key)

    # keep main thread alive
    platform.serve_forever()
