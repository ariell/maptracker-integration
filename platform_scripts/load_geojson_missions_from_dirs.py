import argparse
from time import sleep
from platform_tools.platformdata import post_data
from geojson import Point, FeatureCollection, LineString, Feature
from glob import glob
import json

ARCHIVE_MP_PATTERN = '{}/archive/*.json'
CURRENT_MP_PATTERN = '{}/current/*.json'

parser = argparse.ArgumentParser()
parser.add_argument('--base_path', default="/mnt/nfs/ParticipantData/afriedman/maptracker_support/slocum_sfmc_scrapes", type=str, help="Full path to mission directories")
parser.add_argument("-k", "--platform_key", default="SLOCUM.sfmc", type=str, help="Platform key to post to maptracker")
parser.add_argument("-p", "--port", default=8080, type=int, help="Port for maptracker server")
parser.add_argument("-u", "--url", default="http://10.23.10.205", type=str, help="URL for maptracker server (format: http://ipaddress)")
parser.add_argument("-d", "--delay", default=0, type=int, help="Continuously update at defined delay (otherwise just once)")    # Set options


def missions_to_geojson(fpath):
    feature_list = []
    for fname in glob(fpath):
        with open(fname) as json_data:
            data = json.load(json_data)
        features = data.get("features",[])

        for f in features:
            properties = f.get("properties",{})

            print(properties)

        feature_list += features
    return FeatureCollection(feature_list)

def process_missions(base_path, platform_key, port, url):
    current_missions = missions_to_geojson(CURRENT_MP_PATTERN.format(base_path))
    #print(current_missions)
    post_data(platform_key, current_missions, data_type="mission", maptracker_port=port, maptracker_url=url)

    archive_missions = missions_to_geojson(ARCHIVE_MP_PATTERN.format(base_path))
    #print(archive_missions)
    post_data(platform_key+"-old", archive_missions, data_type="mission", maptracker_port=port, maptracker_url=url)


def main():
    # Parse input args
    args = parser.parse_args()

    if args.delay > 0:
        while True:
            try:
                process_missions(args.base_path, args.platform_key, args.port, args.url)
            except Exception as e:
                print ("*** ERROR: cannot process mission {}".format(e))
            sleep(args.delay)
    else:
        process_missions(args.base_path, args.platform_key, args.port, args.url)

if __name__ == "__main__":
    main()
