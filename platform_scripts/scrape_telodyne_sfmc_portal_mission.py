"""
Script to log in to Telodyne SFMC portal to scrape iridium messages for slocum gliders

NOTE: requires a valid login session from a chrome browser on the local machine. Script will scrape the session cookies
for the login credentials from the browser and use those for querying the API.



"""


import argparse
import requests
# from bs4 import BeautifulSoup
from pycookiecheat import chrome_cookies
import json
from platform_tools.platformdata import post_data
from time import sleep
import traceback
import pickle
import os

# suppress SSL warnings
requests.packages.urllib3.disable_warnings()

# Geojson:
# https://sfmc.webbresearch.com/sfmc/deployment-requests/get-map-features-for-deployment-limited/299
#
# Field IDs:
# https://sfmc.webbresearch.com/sfmc/deployment-requests/get-surface-sensor-types/299
#
# Field Values:
# https://sfmc.webbresearch.com/sfmc/deployment-requests/get-surface-sensor-values-limited/299/395


# login_url = "https://sfmc.webbresearch.com/sfmc/login"
DEFAULT_SFMC_DEPL_ID = 299
URL_PTRN_GEOJSON = "https://sfmc.webbresearch.com/sfmc/deployment-requests/get-map-features-for-deployment-limited/%d"      # % (sfmc_deployment_id)
URL_PTRN_SENSOR_VALUES = "https://sfmc.webbresearch.com/sfmc/deployment-requests/get-surface-sensor-values-limited/%d/%d"   # % (sfmc_deployment_id, sensor_id)
URL_PTRN_SENSOR_TYPES = "https://sfmc.webbresearch.com/sfmc/deployment-requests/get-surface-sensor-types/%d"                # % (sfmc_deployment_id)
DEFAULT_PLATFORM_KEY = "SLOCUM.nemesis"

# This file
this_file = os.path.realpath(__file__)
auth_file = os.path.join(os.path.dirname(this_file), "authentication", "telodyne_sfmc_rcamilli_login_cookies_FK181210.pkl")

# Parser
parser = argparse.ArgumentParser()
parser.add_argument("-i", "--sfmc_id", default=DEFAULT_SFMC_DEPL_ID, type=int, help="Numeric deployment ID from SFMC")
parser.add_argument("-a", "--auth_file", default=auth_file, type=str, help="Path to authentication cookie cache file")
parser.add_argument("-k", "--platform_key", default=DEFAULT_PLATFORM_KEY, type=str, help="Platform key to post to maptracker")
parser.add_argument("-p", "--port", default=8080, type=int, help="Port for maptracker server")
parser.add_argument("-u", "--url", default="http://10.23.10.205", type=str, help="URL for maptracker server (format: http://ipaddress)")
parser.add_argument("-d", "--delay", default=0, type=int, help="Continuously update at defined delay (otherwise just once)")    # Set options



NOMINAL_SPEED = 0.5  # m/s
NOMINAL_UNC = 10.0  # m


# sensor_types = [
#     # {"id": 395, "name": "c_wpt_lat", "units": "lat"},
#     # {"id": 396, "name": "c_wpt_lon", "units": "lon"},
#     {"id": 658, "name": "m_battery", "units": "volts"},                    # !!!   12-16.8
#     # {"id": 1933, "name": "m_coulomb_amphr", "units": "amp-hrs"},
#     # {"id": 1937, "name": "m_coulomb_amphr_total", "units": "amp-hrs"},
#     # {"id": 1934, "name": "m_coulomb_current", "units": "amp"},
#     # {"id": 362, "name": "m_final_water_vx", "units": "m/s"},
#     # {"id": 363, "name": "m_final_water_vy", "units": "m/s"},
#     # {"id": 755, "name": "m_iridium_signal_strength", "units": "nodim"},
#     {"id": 670, "name": "m_leakdetect_voltage", "units": "volts"},         # !!!
#     {"id": 672, "name": "m_leakdetect_voltage_forward", "units": "volts"},  # !!!
#     # {"id": 514, "name": "m_lithium_battery_relative_charge", "units": "%"},
#     # {"id": 776, "name": "m_thruster_watthr", "units": "watt-hrs"},
#     # {"id": 165, "name": "m_tot_num_inflections", "units": "nodim"},
#     # {"id": 664, "name": "m_vacuum", "units": "inHg"},
#     {"id": 358, "name": "m_water_vx", "units": "m/s"},                     # !!!
#     {"id": 359, "name": "m_water_vy", "units": "m/s"},                     # !!!
#     # {"id": 222, "name": "u_use_current_correction", "units": "nodim"},
#     # {"id": 397, "name": "x_last_wpt_lat", "units": "lat"},
#     # {"id": 398, "name": "x_last_wpt_lon", "units": "lon"}
# ]

include_sensor_types = ["m_battery", "m_leakdetect_voltage", "m_leakdetect_voltage_forward", "m_water_vx", "m_water_vy"]

# all_sensor_types = ["m_battery", "m_leakdetect_voltage", "m_leakdetect_voltage_forward", "m_water_vx", "m_water_vy",
#                     "c_wpt_lat", "c_wpt_lon", "m_coulomb_amphr", "m_coulomb_amphr_total", "m_coulomb_current",
#                     "m_final_water_vx", "m_final_water_vy", "m_iridium_signal_strength", "m_lithium_battery_relative_charge",
#                     "m_thruster_watthr", "m_tot_num_inflections", "m_vacuum", "u_use_current_correction", "x_last_wpt_lat",
#                     "x_last_wpt_lon"]


def get_chrome_cookies():
    if os.path.isfile(auth_file):
        # If cookies auth exists, load it
        with open(auth_file, 'rb') as f:  # Python 3: open(..., 'rb')
            cookies = pickle.load(f)
    else:
        # Otherwise get it and save it
        cookies = chrome_cookies(URL_PTRN_GEOJSON)
        with open(auth_file, 'wb') as f:  # Python 3: open(..., 'wb')
            pickle.dump(cookies, f)

    print (cookies)

    return cookies


def get_sensor_types(cookies, sfmc_id):
    # Get sensor types
    print ("Getting sensor types...", end="")
    sensor_types = requests.get(URL_PTRN_SENSOR_TYPES % sfmc_id, cookies=cookies, verify=False).json()
    print ("...Done!")
    print(sensor_types)
    return sensor_types


def update_position(cookies, platform_key, sensor_types, sfmc_id, port, url):
    print("Checking for new position...", end="")
    data = requests.get(URL_PTRN_GEOJSON % sfmc_id, cookies=cookies, verify=False).json()
    print ("...Done!")
    platform_data = {}


    # SCRAPE POSITION FROM GEOJSON, AND THEN GET SENSOR VALUES
    for f in data.get("features"):
        if f.get("properties", {}).get("isLatestGliderPosition"):
            ts = f.get("properties", {}).get("time")
            print("Checking current position...", end="")
            current_platform_data = requests.get("{}:{}/data/platformdata.api.Platform/{}".format(url, port,platform_key)).json()
            print ("...Done!")
            last_updated = current_platform_data.get("stat", {}).get("timestamp")
            if last_updated != ts:
                # last_updated = ts
                ll = f.get("geometry", {}).get("coordinates", [None, None])
                platform_data["pose"] = {
                    "lat": ll[1],
                    "lon": ll[0],
                    "speed": NOMINAL_SPEED,
                    "uncertainty": NOMINAL_UNC
                }

                platform_data["alert"] = {}
                platform_data["stat"] = {"timestamp": ts}
                for s in sensor_types:
                    if s.get("name") in include_sensor_types:
                        print("Checking sensor: {}".format(s.get("name")), end="")
                        sensor_data_list = requests.get(URL_PTRN_SENSOR_VALUES % (sfmc_id, s.get("id")), cookies=cookies, verify=False).json()
                        print ("...Done!")
                        sensor_data = sensor_data_list[-1]
                        platform_data["stat"][s.get("name")] = "{} {}".format(sensor_data.get("value", None), s.get("units",""))

                        if "m_leakdetect_voltage" == s.get("name"):
                            platform_data["alert"]["leak"] = sensor_data.get("value", 0) < 2.4
                        if "m_leakdetect_voltage_forward" == s.get("name"):
                            platform_data["alert"]["leak-F"] = sensor_data.get("value", 0) < 2.4
                        elif "m_battery" == s.get("name"):
                            platform_data["stat"]["bat"] = int(min(100, (sensor_data.get("value", 0) - 12) / (17 - 12) * 100))
                            platform_data["alert"]["bat<12V"] = sensor_data.get("value", 0) <= 12

                print(current_platform_data)
                print("Posting platform data...", end="")
                print(platform_data)
                post_data(platform_key, platform_data, data_type="platform", maptracker_port=port, maptracker_url=url)
                print ("...Done!")

            break  # break when latest position feature is found

    # POST ENTIRE GEOJSON?
    #post_data(platform_key, data, data_type="mission", maptracker_port=port, maptracker_url=url)


def main():
    # Parse input args
    args = parser.parse_args()
    cookies = get_chrome_cookies()

    # Update sensor types
    sensor_types = get_sensor_types(cookies, args.sfmc_id)

    while True:
        try:
            update_position(cookies, args.platform_key, sensor_types, args.sfmc_id, args.port, args.url)
        except Exception as e:
            print ("*** ERROR: cannot process mission {}".format(e))
            traceback.print_exc()

        if args.delay <= 0:
            break
        else:
            sleep(args.delay)


if __name__ == "__main__":
    main()
