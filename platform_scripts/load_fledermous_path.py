import argparse
from time import sleep

import re
from platform_tools.platformdata import post_data
from geojson import Point, FeatureCollection, LineString, Feature
import pandas as pd
import os


def mission_to_geojson(fname, color="grey"):
    data = pd.read_table(fname, header=None, delimiter=" ")

    # print data

    coords = []
    feature_list = []
    for index, r in data.iterrows():
        coords.append([r[1], r[2]])

        point = Point([float(r[1]), float(r[2])])
        feature_list.append(Feature(geometry=point, properties={
            "options": {"radius": 5, "fillColor": color, "color": "black", "weight": 1, "opacity": 0.5, "fillOpacity": 0.5},
            "info": {"Depth": r[3], "Distance": r[0]}
        }))

    #print (coords)

    feature_list.append(Feature(geometry=LineString(coords), properties={
        "options": {"color": color, "weight": 2, "opacity": 0.5, "dashArray": "5,5"},
        "info": {"Type": "LineString: Path", "Name": os.path.basename(fname)}
    }))

    # print feature_list
    return FeatureCollection(feature_list)
    # print mission_data
    # print coords


def main():
    # Parse input args
    parser = argparse.ArgumentParser()
    parser.add_argument('mission_path', action="store", type=str, help="Full path to mission file")
    parser.add_argument("-k", "--platform_key", default="whoi.slocum", type=str, help="Platform key to post to maptracker")
    parser.add_argument("-c", "--color", default="yellow", type=str, help="Colour to plot mission")
    parser.add_argument("-p", "--port", default=8080, type=int, help="Port for maptracker server")
    parser.add_argument("-u", "--url", default="http://10.23.10.205", type=str, help="URL for maptracker server (format: http://ipaddress)")
    parser.add_argument("-d", "--delay", default=0, type=int, help="Continuously update at defined delay (otherwise just once)")    # Set options

    args = parser.parse_args()

    if args.delay > 0:
        while True:
            try:
                mission_data = mission_to_geojson(args.mission_path, color=args.color)
                post_data(args.platform_key, mission_data, data_type="mission", maptracker_port=args.port, maptracker_url=args.url)
            except Exception as e:
                print ("*** ERROR: cannot update mission {}".format(e))
            sleep(args.delay)
    else:
        mission_data = mission_to_geojson(args.mission_path, color=args.color)
        post_data(args.platform_key, mission_data, data_type="mission", maptracker_port=args.port, maptracker_url=args.url)

if __name__ == "__main__":
    main()
