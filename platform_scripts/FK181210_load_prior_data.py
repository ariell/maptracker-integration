import argparse
from time import sleep

import re
from platform_tools.platformdata import post_data
from geojson import Point, FeatureCollection, LineString, Feature
import pandas as pd
import os

key_file = '/mnt/nfs/ParticipantData/afriedman/maptracker_support/prior_features/sahling_seafloor_featureskey.txt'
feat_file = '/mnt/nfs/ParticipantData/afriedman/maptracker_support/prior_features/sahling_seafloor_features.txt'
title = "Sahling et. el."


def mission_to_geojson(featfname, keyfname, color="grey"):
    data = pd.read_csv(featfname, delimiter="\t")
    keys = pd.read_csv(keyfname, delimiter="\t")

    feature_data = pd.merge(data, keys, on='id')

    feature_list = []
    for index, row in feature_data.iterrows():
        point = Point([float(row["lon"]), float(row["lat"])])
        feature_list.append(Feature(geometry=point, properties={
            "options": {"radius": 5, "fillColor": row.get("color", color), "color": "black", "weight": 1, "opacity": 0.3, "fillOpacity": 0.3},
            "info": {" Feature no.": row["no"], "Depth": row["dep"], " Feature  type": row["descriptor"], "Source": title, "Lat":row["lat"], "Lon":row["lon"]}
        }))

    # print feature_list
    return FeatureCollection(feature_list)
    # print mission_data
    # print coords


def main():
    # Parse input args
    parser = argparse.ArgumentParser()
    parser.add_argument("-k", "--platform_key", default="FEATURES.PRIOR", type=str, help="Platform key to post to maptracker")
    parser.add_argument("-c", "--color", default="yellow", type=str, help="Colour to plot mission")
    parser.add_argument("-p", "--port", default=8080, type=int, help="Port for maptracker server")
    parser.add_argument("-u", "--url", default="http://10.23.10.205", type=str, help="URL for maptracker server (format: http://ipaddress)")
    parser.add_argument("-d", "--delay", default=0, type=int, help="Continuously update at defined delay (otherwise just once)")    # Set options

    args = parser.parse_args()

    featfname = feat_file
    keyfname = key_file

    if args.delay > 0:
        while True:
            try:
                mission_data = mission_to_geojson(featfname, keyfname, color=args.color)
                post_data(args.platform_key, mission_data, data_type="mission", maptracker_port=args.port, maptracker_url=args.url)
            except Exception as e:
                print ("*** ERROR: cannot update mission {}".format(e))
            sleep(args.delay)
    else:
        mission_data = mission_to_geojson(featfname, keyfname, color=args.color)
        #print(mission_data)
        post_data(args.platform_key, mission_data, data_type="mission", maptracker_port=args.port, maptracker_url=args.url)

if __name__ == "__main__":
    main()
