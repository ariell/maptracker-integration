import argparse
from time import sleep

import re
from platform_tools.platformdata import post_data
from geojson import Point, FeatureCollection, LineString, Feature
import pandas as pd
import os
from glob import glob

ARCHIVE_MP_PATTERN = '{}/archive/*.csv'
CURRENT_MP_PATTERN = '{}/current/*.csv'

parser = argparse.ArgumentParser()
parser.add_argument('--base_path', default="/mnt/nfs/ParticipantData/afriedman/maptracker_support/subastian_missions", type=str, help="Full path to mission directories")
parser.add_argument("-k", "--platform_key", default="ROV.SUBASTIEN", type=str, help="Platform key to post to maptracker")
parser.add_argument("-c", "--color", default="yellow", type=str, help="Colour to plot mission")
parser.add_argument("-p", "--port", default=8080, type=int, help="Port for maptracker server")
parser.add_argument("-u", "--url", default="http://10.23.10.205", type=str, help="URL for maptracker server (format: http://ipaddress)")
parser.add_argument("-d", "--delay", default=0, type=int, help="Continuously update at defined delay (otherwise just once)")    # Set options


def missions_to_geojson(fpath, color="grey"):
    feature_list = []
    for fname in glob(fpath):
        basename = os.path.basename(fname)
        data = pd.read_table(fname, delimiter=",")
        # print data
        coords = []
        point_list = []
        for index, r in data.iterrows():
            ll = [float(r["Long"]), float(r["Lat"])]
            coords.append(ll)

            point = Point(ll)
            point_list.append(Feature(geometry=point, properties={
                "options": {"radius": 3, "fillColor": color, "color": "black", "weight": 1, "opacity": 0.5, "fillOpacity": 0.5},
                "info": {"Depth": r["Depth"], "Lat":r["Lat"],"Lon":r["Long"], "Distance": r["Distance"], "Name": basename}
            }))

        #print (coords)

        feature_list.append(Feature(geometry=LineString(coords), properties={
            "options": {"color": color, "weight": 2, "opacity": 0.5, "dashArray": "5,5"},
            "info": {"Type": "LineString: Path", "Name": basename}
        }))
        feature_list += point_list

    # print feature_list
    return FeatureCollection(feature_list)
    # print mission_data
    # print coords

def process_missions(base_path, platform_key, port, url, color):
    current_missions = missions_to_geojson(CURRENT_MP_PATTERN.format(base_path), color=color)
    #print(current_missions)
    post_data(platform_key, current_missions, data_type="mission", maptracker_port=port, maptracker_url=url)

    archive_missions = missions_to_geojson(ARCHIVE_MP_PATTERN.format(base_path), color="grey")
    # print(archive_missions)
    post_data(platform_key+"-old", archive_missions, data_type="mission", maptracker_port=port, maptracker_url=url)


def main():
    # Parse input args
    args = parser.parse_args()

    if args.delay > 0:
        while True:
            try:
                process_missions(args.base_path, args.platform_key, args.port, args.url, args.color)
            except Exception as e:
                print ("*** ERROR: cannot process mission {}".format(e))
            sleep(args.delay)
    else:
        process_missions(args.base_path, args.platform_key, args.port, args.url, args.color)

if __name__ == "__main__":
    main()
