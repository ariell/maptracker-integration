import threading, traceback
class Publisher:
    def __init__(self):
        self.subscribers = {}
        self.subscribers["calllater"] = [self.DoCallLater]
        self.deferredEvents = []
        
    def AddSubscriber(self, topic, subscriber):
        self.subscribers.setdefault(topic,[]).append(subscriber)
        
    def RemoveSubscriber(self, topic, subscriber):
        if self.subscribers.has_key(topic):
            if subscriber in self.subscribers[topic]:
                self.subscribers[topic].remove(subscriber)
                if not len(self.subscribers[topic]):
                    del self.subscribers[topic]
    
    def FireUIEvent(self, topic, *args,**kwargs):
        if (threading.currentThread().getName()) == 'MainThread':
            return self.FireEvent(topic,*args,**kwargs)
        else:
            self.deferredEvents.append((topic,)+(args,kwargs))
    
    def CallLater(self, func, *args):
        self.FireUIEvent("calllater", func, *args)
        
    def DoCallLater(self, func, *args):
        return func(*args)

    def FireEvent(self, topic, *args, **kw):
        retValue = []
        if self.subscribers.has_key(topic):
            for e in self.subscribers[topic]:
                try:
                    retValue.append(e(*args,**kw))
                except:
                    traceback.print_exc()
        return retValue
    
    def OnIdle(self):
        if len(self.deferredEvents):
            d,self.deferredEvents = self.deferredEvents,[]
            for a in d:
                self.FireEvent(a[0],*a[1],**a[2])

pub = Publisher()
