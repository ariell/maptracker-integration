'''
Created on May 9, 2011

@author: rtrout

Date        Author                 Comment
2011-06-06  rtrout@teledyne.com    Added gliders home directory
2012-01-30  rtrout@teledyne.com    Added dockserver requests for glider surface sensor names
                                   and sensor data values (defined in config.srf).
2012-02-02  lcooney@teledyne.com   Add getDockProxy() fn in GliderProxy
2012-02-15  rtrout@teledyne.com    Added various code comments.
2012-03-29  rtrout@teledyne.com   Added GliderConnectResponse and GliderCommandRequest packets for
                                  receiving glider dialog and sending glider commands.
2012-10-03  rtrout@teledyne.com   Added moveGlider and addGlider to DockProxy class to support
                                  hosting dockserver administration scripts.
                                  Added new event (ON_MESSAGE) notification to subscribers of dock status packet
                                  received.
2012-10-04  rtrout@teledyne.com   Parameterized application information - client_id and client_version
                                  so dockserver could tell which client was making a request.
2012-10-05  rtrout@teledyne.com   Added removeGlider and backupGlider to DockProxy in support
                                  of hosting dockserver administration scripts.
2012-10-08  rtrout@teledyne.com   Added restoreGlider method to DockProxy in support of hosting
                                  dockserver administration scripts.
2012-10-09  rtrout@teledyne.com   DockProxy.openConnection - added optional argument to control subscribing
                                  to dockserver log events.  Default is to subscribe (True).
2012-10-16  rtrout@teledyne.com   Added addHost and removeHost to DockProxy class in support of dockserver
                                  hosting administration.
2012-10-24  rtrout@teledyne.com   Added class DataProxy.
2012-10-25  rtrout@teledyne.com   Added DockProxy methods to add and remove hosting customers.
2012-10-29  rtrout@teledyne.com   Added DockProxy methods to add and remove hosting users.
2012-10-30  rtrout@teledyne.com   Added DataProxy methods to add, remove, and mod users.
2012-11-14  rtrout@teledyne.com   Refactored host changes class hierarchy
2012-11-27  rtrout@teledyne.com   DataProxy methods for administering dataserver host subscriptions to dockserver hosts.
2014-07-31  rtrout@teledyne.com   Moved code from control center project to python-dockproxy project.
'''

import packet
import publisher
import threading
import sys
import os
import getpass

# Amount of time control center waits for a dockserver response to a specific packet
# before a timeout exception is thrown.
CONNECTION_RESPONSE_TIMEOUT = 30.0 # In seconds


###############################################################################
# Encapsulates information related to events like on-add-glider and
# on-event-log-page.  Instances passed to event listeners at the time of the
# event.
#
class DockProxyEvent(object):
    '''
    Attributes:
    dock_proxy - (DockProxy) The DockProxy instance that fired this event.
    event_object - (object) A user object instance associated with this event.
    '''
    ###########################################################################
    # Class DockProxy constructor.
    #
    # Arguments:
    #  dock_proxy - (DockProxy) The DockProxy instance that fired the event.
    #  event_object - (object) Event specific data.  For example, GliderProxy
    #    for on-add-glider event.
    #
    def __init__(self, dock_proxy, event_object):
        '''
        Attributes:
          dock_proxy - (DockProxy) The proxy of the dockserver that manages this glider.
          event_object - (object) Information or data specific to the event.
        '''
        self.dock_proxy = dock_proxy
        self.event_object = event_object

    ###########################################################################
    # Returns the DockProxy instance associated with this event - the source of
    # the event.
    #
    # Returns:
    #  DockProxy - Dockproxy instance associated with this event.
    #    Source of the event.
    #
    def getDockProxy(self):
        return self.dock_proxy

    ###########################################################################
    # Returns the specific object associated with this event - class depends on
    # the event.
    #
    # Returns:
    #  object - Instance of user data associated with this event.
    #
    def getEventObject(self):
        return self.event_object

###############################################################################
# Client side class that acts as a proxy object to an actual dockserver on a remote
# machine.
#
# Publish / Subscribe Interface - This class supports the following events.
#
# onAddGlider(DockProxyEvent) subscribe to the topic DockProxy.ON_ADD_GLIDER.
#  Fired when a glider connects to this dockserver for the very first time.
#
# onRemoveGlider(DockProxyEvent) subscribe to the topic DockProxy.ON_REMOVE_GLIDER.
#  Fired when a glider is no longer managed by the actual dockserver.
#
# onClose(DockProxy) subscribe to the topic DockProxy.ON_CLOSE.
#  Fired when the network connection to the dockserver is closed.
#
# onEventLogPage(DockProxyEvent) subscribe to the topic DockProxy.ON_EVENT_LOG_PAGE.
#  Fired when a glider log page is received from dockserver.
#
# onEventLogNotification(DockProxyEvent) subscribe to the topic DockProxy.ON_EVENT_LOG_NOTIFICATION
#  Fired when dockserver notifies this proxy of a new event in the glider log.
#
class DockProxy(object):
    '''
    Attributes:
      host - (string) Domain name or IP address of the actual dockserver
      port - (int) The port number of the listening socket on the dockserver.
      server_host - (string) The hosting dockserver identifier (i.e., 'twr' in //localhost:6564/twr)
      packet_channel - (PacketChannel) The network connection with the dockserver.
      gliders - (dictionary) Keys are glider names as strings; values are GliderProxy
        instances.
      glider_names - (Set) Set of glider names as strings.
      gliders_home - (string) Glider home directory on the actual dockserver machine
        (typically /var/opt/gmc/gliders/).
      proxy_publisher - (publisher.Publisher) Maintains dock proxy event subscribers and performs
        event notifications.
      is_synchronized_event - (threading.Event) Used to signal when this proxy is synchronized
        with its actual dockserver (via receiving a DockConfigurationResponse packet).
      gliders_lock - (threading.RLock) Serializes multi-thread access to the attribute gliders.
      client_id - (string) The client ID of the application using this dockserver proxy.  Sent to
        the actual dockserver to determine feature support.
      client_version - (string) The client version of the application using this dockserver proxy.
        Sent to the actual dockserver to determine feature support.
    '''

    # Class Constants
    DEFAULT_DOCKSERVER_CLIENT_PORT = 6564
    # Default hosting dockserver identifier
    DEFAULT_DOCKSERVER_HOST = 'default'
    # Default client ID and version.  These should be set by the client application for correct operation.
    DEFAULT_CLIENT_ID = 'Teledyne-Webb Mission Planner'
    DEFAULT_CLIENT_VERSION = '1.0'
    # Subscriber events
    ON_ADD_GLIDER = 'onAddGlider(DockProxyEvent)'
    ON_REMOVE_GLIDER = 'onRemoveGlider(DockProxyEvent)'
    ON_CLOSE = 'onClose(DockProxy)'
    ON_EVENT_LOG_PAGE = 'onEventLogPage(DockProxyEvent)'
    ON_EVENT_LOG_NOTIFICATION = 'onEventLogNotification(DockProxyEvent)'
    ON_MESSAGE = 'onMessage(DockProxyEvent)'
    # Host change actions
    ADD_MEMBERSHIP_ACTION = 1
    DELETE_MEMBERSHIP_ACTION = 2

    ###########################################################################
    # Class DockProxy constructor.
    #
    # Arguments:
    #  host - (string) Host domainname or dotted IP address.
    #  port - (int) TCP/IP port that dockserver listens for connections.
    #
    def __init__(self, host, port=DEFAULT_DOCKSERVER_CLIENT_PORT):
        '''
        Initialize this dockproxy and set up for openConnection call.
        '''
        self.host = host
        self.port = port
        self.client_id = DockProxy.DEFAULT_CLIENT_ID
        self.client_version = DockProxy.DEFAULT_CLIENT_VERSION
        self.server_host = DockProxy.DEFAULT_DOCKSERVER_HOST
        self.packet_channel = None
        self.gliders = {}
        self.glider_names = set()
        self.gliders_home = None
        self.proxy_publisher = publisher.Publisher()
        # Event to signal that this instance is synchronized to its actual dockserver
        # to threads attempting to read this instance's state.
        self.is_synchronized_event = threading.Event()
        # Lock used to serialize thread access to self.gliders and self.glider_names
        self.gliders_lock = threading.RLock()


    ###########################################################################
    # Class destructor - releases all network resources.
    #
    def __del__(self):
        '''
        Destructor - close all glider proxy network connections and this dock proxy instance's
        network connection.
        '''
        self.closeAllConnections()

    ###########################################################################
    # PUBLIC Interface
    ###########################################################################

    ###########################################################################
    # Open a continuous network connection to the actual dockserver machine.
    # Subscribe to dockserver's event log.
    #
    # Exceptions:
    #  ConnectionError - thrown if unable to establish a network connection to
    #    the actual dockserver machine.
    #
    def openConnection(self, request_log_events = True):
        '''
        Start a thread that opens a socket to the dockserver, send a dock configuration
        packet and then enter loop listening for dockserver packets and processing them.
        '''
        try:
            if self.packet_channel:
                # If a connection is open, close it
                self.closeConnection()
            # open a new connection
            self.packet_channel = packet.PacketChannel(self.host, self.port, self.client_id, self.client_version)
            self.packet_channel.setServerHost(self.server_host)
            # Subscribe to packet channel events.
            self.packet_channel.addSubscriber(packet.PacketChannel.ON_PACKET, self.onPacket)
            self.packet_channel.addSubscriber(packet.PacketChannel.ON_CLOSE, self.onClose)
            self.packet_channel.openChannel()
        except:
            # If unable to open a connection, then notify caller.
            raise ConnectionError(self, 'Unable to open network connection to Dock Server.')
        # Get dockserver gliders
        #self.synchronizeToDock()
        if request_log_events:
            # subscribe to event log events from the dockserver.
            self.packet_channel.sendPacket(packet.EventLogRequestPacket(packet.EventLogRequestPacket.SUBSCRIBE_TO_EVENT_LOG))

    ###########################################################################
    # Close the network connection to this client's dockserver machine.
    #
    # Exceptions:
    #  ConnectionError - Thrown if unable to close the network connection to the dockserver.
    #
    def closeConnection(self):
        '''
        Close the packet channel to this dock proxy's dockserver.
        '''
        try:
            if self.packet_channel:
                self.packet_channel.removeSubscriber(packet.PacketChannel.ON_PACKET, self.onPacket)
                self.packet_channel.removeSubscriber(packet.PacketChannel.ON_CLOSE, self.onClose)
                self.packet_channel.closeChannel()
        except:
            raise ConnectionError(self, 'Unable to close network connection to Dock Server.')
        finally:
            self.packet_channel = None

    ###########################################################################
    # Used for hosting dockserver administration.  Opens a connection to the
    # passed host such that dock status packets will be received.
    #
    # Arguments:
    #  host - (string) The hosting dockserver to connect to
    #  user_id - (string) The connecting user identifier for authentication purposes.
    #
    def openHost(self):
        self.packet_channel.sendPacket(packet.DockConnectRequest())

    ###########################################################################
    # Add the passed subscriber function as the method to call when the passed
    # event topic is fired.
    #
    # Arguments:
    #  topic - (string) The event to subscribe to.
    #  subscriber - (function) The method to call when the passed event topic is fired
    #    (a listener for the event).
    #
    # Exceptions:
    #  ProxyError - Thrown if unable to add subscriber to the passed event.
    #
    def addSubscriber(self, topic, subscriber):
        '''
        Add the passed subscriber to be notified when events related to the passed topic occur.
        '''
        # Add the passed subscriber as an event listener
        try:
            self.proxy_publisher.AddSubscriber(topic, subscriber)
        except:
            raise ProxyError(self, 'Unable to add subscriber to event: ' + topic)

    ###########################################################################
    # Remove the passed subscriber from the passed event topic.  This subscriber
    # will no longer receive event notifications for this topic.
    #
    # Arguments:
    #  topic - (string) The event to UN-subscribe to.
    #  subscriber - (function) The method to remove from event notification
    #    (a listener for the event).
    #
    # Exceptions:
    #  ProxyError - Thrown if unable to remove the subscriber from the event.
    #
    def removeSubscriber(self, topic, subscriber):
        '''
        Remove the passed subscriber from those clients notified when events related to the passed
        topic occur.
        '''
        # Remove the passed subscriber from packet event notification.
        try:
            self.proxy_publisher.RemoveSubscriber(topic, subscriber)
        except:
            raise ProxyError(self, 'Unable to remove subscriber from event: ' + topic)

    ###########################################################################
    # Close this dock proxy's connection and all of the glider proxy
    # connections managed by this dock proxy.
    #
    def closeAllConnections(self):
        '''
        Close all open glider proxy network channels managed by this dockserver.
        Also, close this dock proxy's network channel.
        '''
        for glider_proxy in self.gliders.values():
            glider_proxy.closeConnection()
        self.closeConnection()

    ###########################################################################
    # Request from dockserver the log page with the passed page ID.  This page is
    # returned via subscribing to the ON_EVENT_LOG_PAGE event (i.e., not by this
    # function call).
    #
    # Arguments:
    #  page_id - (string) The ID of the base log page.  Format is YYYYMMDD.
    #  previous_page - (boolean) If True, request is for the previous page to
    #    the passed page_id.
    #  next_page - (boolean) If True, request is for the next page to the passed
    #    page_id.
    #
    def requestLogPage(self, page_id, previous_page, next_page):
        #print 'DockProxy.requestLogPage...'
        if previous_page:
            request_packet = packet.EventLogRequestPacket(packet.EventLogRequestPacket.PREVIOUS_PAGE_REQUEST, log_page = page_id)
        elif next_page:
            request_packet = packet.EventLogRequestPacket(packet.EventLogRequestPacket.NEXT_PAGE_REQUEST, log_page = page_id)
        else:
            request_packet = packet.EventLogRequestPacket(packet.EventLogRequestPacket.PAGE_REQUEST, log_page = page_id)
        self.packet_channel.sendPacket(request_packet)
        #print 'DockProxy.requestLogPage: Done'

    ###########################################################################
    # Send the user annotation (new log entry) to dockserver for recording
    # purposes.  Dockserver will assign a unique ID and return the entry for
    # display.
    #
    # Arguments:
    #  log_entry - (packet.EventLogEntry) The manual log entry to add to the dockserver
    #    event log.
    #
    def requestAddLogEntry(self, log_entry):
        self.packet_channel.sendPacket(packet.EventLogRequestPacket(packet.EventLogRequestPacket.ADD_EVENT, log_entry = log_entry))

    ###########################################################################
    # Returns a new dictionary instance of all managed gliders.  Keys are glider names (string)
    # and corresponding Values are GliderProxys.  Caller is blocked until this
    # client is synchronized with the dockserver.
    #
    # Returns:
    #  A dictionary whose keys are the names (string) of gliders managed by the actual
    #  dockserver and values are instances of GliderProxy (with network connections closed).
    #
    # Exceptions:
    #  TimeoutError - Thrown if the dockserver does not response to synchronizing
    #    within the timeout period.
    #  ProxyError - Thrown if unable to get glider information from dockserver.
    #
    def getGliders(self):
        '''
        Return a new dictionary instance of all gliders managed by this dock proxy.  Dictionary keys
        are glider names as strings and dictionary values are the corresponding glider proxy
        instances - caller must call openConnection() on these glider proxys to establish a
        network connection to the dock server.
        '''
        #print 'DockProxy.getGliders.'
        # Calling thread must wait until this instance is synchronized.
        try:
            # Clear the synchronize event flag
            self.is_synchronized_event.clear()
            # Send the dock configuration request
            self.synchronizeToDock()
            #print 'DockProxy.getGliders: Just called synchronize.  waiting...'
            # Wait for the response to the configuration request
            self.is_synchronized_event.wait(CONNECTION_RESPONSE_TIMEOUT)
            #print 'DockProxy.getGliders: ...done waiting for dock configuration'
            if not self.is_synchronized_event.isSet():
                # If timed out waiting for DockProxy synchronization, then
                # raise an exception.
                #print 'DockProxy.getGliders: timed out waiting for dock configuration'
                raise TimeoutError(self, 'Timeout waiting for DockProxy to synchronize.')
            # Return a shallow copy of the glider dictionary
            try:
                #print 'DockProxy.getGliders: about to copy glider list'
                # synchronize with other threads for the shared resource - self.gliders
                self.gliders_lock.acquire()
                # copy it while it's locked.
                return self.gliders.copy()
            finally:
                # Release the lock upon return
                self.gliders_lock.release()
        except TimeoutError:
            raise
        except:
            raise ProxyError(self, 'Unable to get gliders from Dock Server.')

    ###########################################################################
    # Returns the dockserver's hostname and port number.
    #
    # Returns:
    #  string - Dockserver host name and port number
    #
    def getName(self):
        '''
        Return this dock proxy's string identifier (or name).  This identifier is the dockserver's
        domain name with the network port appended.
        '''
        return self.host + ':' + str(self.port)

    ###########################################################################
    # Returns the gliders' home directory path on the actual dockserver.  If not
    # synchronized, returns None.
    #
    # NOTE: gliders_home is not set until this instance is synchronized by a call
    #       to getGliders().
    #
    def getGlidersHome(self):
        '''
        Return the glider home directory's full path on this proxy's corresponding actual dockserver.
        '''
        return self.gliders_home

    ###########################################################################
    # Returns the hostname or IP address of the actual dockserver machine.
    #
    # Returns:
    #  string - The dockserver's hostname
    #
    def getHost(self):
        '''
        Return this dock proxy's dockserver domain name or host name.
        '''
        return self.host

    ###########################################################################
    # Returns the TCP/IP listening port number of the actual dockserver machine.
    #
    # Returns:
    #  int - Dockserver's port number
    #
    def getPort(self):
        '''
        Return this dock proxy's network port used to connection with its dock server.
        '''
        return self.port

    ###########################################################################
    # Returns the user application's client ID as known to dockserver.
    #
    # Returns:
    #  string - The application's ID.
    #
    def getClientID(self):
        return self.client_id

    ###########################################################################
    # Returns the user application's client version as known to dockserver.
    #
    # Returns:
    #  string - The application's version as understood by dockserver.
    #
    def getClientVersion(self):
        return self.client_version

    ###########################################################################
    # Returns the server host name of this proxy's actual hosting dockserver.
    #
    # Returns:
    #  string - server host name
    #
    def getServerHost(self):
        return self.server_host

    ###########################################################################
    # Sets the hosting dockserver identifier
    #
    # Arguments:
    #  server_host - (string) The identifier of the hosting dockserver.
    #
    def setServerHost(self, server_host):
        self.server_host = server_host

    ###########################################################################
    # Sets the hosting dockserver identifier
    #
    # Arguments:
    #  client_id - (string) The calling application's client ID (as known to
    #    dockserver).  Used to determine feature support by dockserver.
    #  client_version - (string) The calling application's client version (as
    #    understood by dockserver).  Used to determine feature support by
    #    dockserver.
    #
    def setClientVersion(self, client_id, client_version):
        self.client_id = client_id
        self.client_version = client_version

    ###########################################################################
    # Just a test method
    #
    def helloWorld(self):
        print ('Hello World from Dockserver')

    ###########################################################################
    # PRIVATE Interface
    ###########################################################################

    ###########################################################################
    # Synchronizes this clients list of managed gliders with the actual dockserver's
    # glider list by sending a DockConfigurationRequest packet to the dockserver.
    # Dockserver's response arrives asynchronously via ON_PACKET event.
    #
    # Exceptions:
    #  ConnectionError - Thrown if unable to send a DockConfiguration packet to
    #    dockserver.
    #
    def synchronizeToDock(self):
        '''
        Force this dock proxy to synchronize with its corresponding dockserver.  That is
        update this proxy's list of managed gliders to match that of its corresponding
        dockserver.
        '''
        try:
            # Send a configuration request to tigger dockserver reponding with a configuration
            # response packet.  This proxy syncs up when this response packet arrives.
            self.packet_channel.sendPacket(packet.DockConfigurationRequest())
        except:
            # If unable to send packet, notify caller
            raise ConnectionError(self, 'Unable to send DockConfigurationRequest packet.')

    ###########################################################################
    # Update this client's list of managed gliders and notify event subscribers
    # of any changes in this list.
    #
    # Arguments:
    #  packet_received - (DockConfigurationResponse) The packet from dockserver
    #    that contains its glider and serial port configuration.
    #
    def update(self, packet_received):
        '''
        Update this proxy's glider name set and notify subscribers of new and
        removed gliders.
        '''
        try:
            # Acquire the lock needed to access the shared resources - gliders and glider_names
            # instance variables.
            self.gliders_lock.acquire()
            # Get gliders home directory
            self.gliders_home = packet_received.getGlidersHome()
            # Compute new gliders managed by the dockserver
            add_glider_names = packet_received.getGliderNames() - self.glider_names
            # Compute gliders no longer managed by the dockserver
            remove_glider_names = self.glider_names - packet_received.getGliderNames()
            # Set the new list of managed glider names
            self.glider_names = packet_received.getGliderNames()
            # Add the new gliders to this proxy
            for glider_name in add_glider_names:
                glider = GliderProxy(self, glider_name)
                self.gliders[glider_name] = glider
                # Notify subscribers of new gliders
                self.proxy_publisher.FireEvent(DockProxy.ON_ADD_GLIDER, DockProxyEvent(self, glider))
            # Remove gliders no longer managed by this proxy
            for glider_name in remove_glider_names:
                glider = self.gliders[glider_name]
                glider.closeConnection()
                del self.gliders[glider_name]
                # Notify subscribers of removed glider.
                self.proxy_publisher.FireEvent(DockProxy.ON_REMOVE_GLIDER, DockProxyEvent(self, glider))
        finally:
            # In all cases, release the lock on gliders and glider_names
            self.gliders_lock.release()

    ###########################################################################
    # Event handler for PacketChannel receive packets Event.
    #
    # Threading - packet.PacketChannel.receivePacketThread
    #
    # Arguments:
    #  packet_received - (subclass of Packet) The packet just received from the
    #    dockserver.
    #
    def onPacket(self, packet_received):
        '''
        Handles onPacket events from a PacketChannel instance.  Called each time a packet arrives
        from the dockserver.
        '''
        #print 'In DockProxy.onPacket for ' + self.host + ':' + str(self.port) + '...'
        #print str(packet_received)

        # Handle packets
        if packet_received.getAction() == packet.Packet.DOCK_CONFIGURATION:
            # If a dock configuration packet is received, then synchronize gliders with the actual
            # dockserver.
            #print 'DockProxy.onPacket: Got dock configuration packet'
            self.update(packet_received)
            #print 'DockProxy.onPacket: Back from dock configuration event'
            # Signal any waiting threads that this instance is synchronized.
            self.is_synchronized_event.set()
        if packet_received.getAction() == packet.Packet.LOG_PAGE:
            #print 'DockProxy.onPacket: Got log page packet'
            # If received packet is an event log page, then notify GUI to display it.
            self.proxy_publisher.FireEvent(DockProxy.ON_EVENT_LOG_PAGE, DockProxyEvent(self, packet_received.getLogPage()))
            #print 'DockProxy.onPacket: Back from log page event'
        if packet_received.getAction() == packet.Packet.LOG_NOTIFICATION:
            #print 'DockProxy.onPacket: Got log notification packet'
            # If received packet is an event log notification, then notify the GUI
            # to display it.
            self.proxy_publisher.FireEvent(DockProxy.ON_EVENT_LOG_NOTIFICATION, DockProxyEvent(self, packet_received.getLogEntry()))
            #print 'DockProxy.onPacket: Back from log notification event'
        if packet_received.getAction() == packet.Packet.DOCK_STATUS:
            # If received packet is a dock status packet, then notify subscribers
            #print 'Received DOCK_STATUS packet.  Fire ON_MESSAGE event.'
            self.proxy_publisher.FireEvent(DockProxy.ON_MESSAGE, DockProxyEvent(self, packet_received.getMessage()))

    ###########################################################################
    # Packet channel event handler for the channel close event.  Notifies all
    # DockProxy subscribers of the ON_CLOSE event.
    #
    # Threading - packet.PacketChannel.peer_monitor_timer thread and
    # packet.PacketChannel.asyncore.loop thread
    #
    def onClose(self):
        '''
        Handle the onClose event from a PacketChannel instance.  Called when the network channel
        is closed by the peer (or some inbetween router, etc...)
        '''
        #print 'In DockProxy.onClose for ' + self.host + ':' + str(self.port) + '...'
        # Pass this event to subscribers for handling as they wish.
        self.proxy_publisher.FireEvent(DockProxy.ON_CLOSE, self)

    ###########################################################################
    # Used only for hosting dockserver administration.  Moves the passed glider
    # from this dockserver proxy's management to the passed dockserver proxy's
    # management.
    #
    # Arguments:
    #  glider_name - (string) Name of the glider to delete from this dock proxy
    #    and add to the passed dock proxy.
    #  destination_host - (string) The hosting dockserver to add the passed
    #    glider to.
    #
    def moveGlider(self, glider_name, destination_host):
        try:
            change = packet.GliderChange(packet.GliderChange.MOVE_GLIDER, glider_name, destination_host)
            self.packet_channel.sendPacket(packet.DockChangesPacket(change))
        except:
            # If unable to send packet, notify caller
            raise ConnectionError(self, 'Unable to send DockChangesPacket packet to move a glider.')


    ###########################################################################
    # Used only for hosting dockserver administration.  Adds the passed glider
    # to the managed glider list of this dock proxy.
    #
    # Arguments:
    #  glider_name - (string) Name of the glider to add to this dock proxy's
    #    list of managed gliders.
    #
    def addGlider(self, glider_name):
        try:
            change = packet.GliderChange(packet.GliderChange.ADD_GLIDER, glider_name)
            self.packet_channel.sendPacket(packet.DockChangesPacket(change))
        except:
            # If unable to send packet, notify caller
            raise ConnectionError(self, 'Unable to send DockChangesPacket packet to add a glider.')

    ###########################################################################
    # Used only for hosting dockserver administration.  Removes the passed glider
    # from the managed glider list of this dock proxy and deletes all associated
    # files.
    #
    # Arguments:
    #  glider_name - (string) Name of the glider to remove from this dock proxy's
    #    list of managed gliders and glider home directory.
    #
    def removeGlider(self, glider_name):
        try:
            change = packet.GliderChange(packet.GliderChange.REMOVE_GLIDER, glider_name)
            self.packet_channel.sendPacket(packet.DockChangesPacket(change))
        except:
            # If unable to send packet, notify caller
            raise ConnectionError(self, 'Unable to send DockChangesPacket packet to remove a glider.')

    ###########################################################################
    # Used only for hosting dockserver administration.  Backups the passed
    # glider's files.
    #
    # Arguments:
    #  glider_name - (string) Name of the glider whose files will be backedup.
    #
    def backupGlider(self, glider_name):
        try:
            change = packet.GliderChange(packet.GliderChange.BACKUP_GLIDER, glider_name)
            self.packet_channel.sendPacket(packet.DockChangesPacket(change))
        except:
            # If unable to send packet, notify caller
            raise ConnectionError(self, 'Unable to send DockChangesPacket packet to backup a glider.')

    ###########################################################################
    # Used only for hosting dockserver administration.  Restores the passed
    # glider backup filename.
    #
    # Arguments:
    #  glider_backup_file - (string) Name of the glider backup file that will be restored.
    #
    def restoreGlider(self, glider_name, glider_backup_file):
        try:
            change = packet.GliderChange(packet.GliderChange.RESTORE_GLIDER, glider_name, glider_backup_file)
            self.packet_channel.sendPacket(packet.DockChangesPacket(change))
        except:
            # If unable to send packet, notify caller
            raise ConnectionError(self, 'Unable to send DockChangesPacket packet to restore a glider.')

    ###########################################################################
    # Used only for hosting dockserver administration.  Creates the passed host
    # on the connected dockserver.
    #
    # Arguments:
    #  host_name - (string) Name of the host dockserver to create on this proxy's
    #    dockserver.
    #
    def addHost(self, host_name, config_filename):
        try:
            changes = packet.HostChanges(packet.HostChanges.DOCK_HOST_CHANGES_TAG)
            change = packet.HostHostChange(packet.HostChange.ADD, host_name, filename = config_filename)
            changes.addChange(change)
            self.packet_channel.sendPacket(packet.DockChangesPacket(changes))
        except:
            # If unable to send packet, notify caller
            raise ConnectionError(self, 'Unable to send DockChangesPacket packet to add a dock host.')

    ###########################################################################
    # Used only for hosting dockserver administration.  Removes the passed host
    # from this proxy's dockserver.
    #
    # Arguments:
    #  host_name - (string) Name of the host to remove from this dock proxy's
    #    actual dockserver.
    #
    def removeHost(self, host_name):
        try:
            changes = packet.HostChanges(packet.HostChanges.DOCK_HOST_CHANGES_TAG)
            change = packet.HostHostChange(packet.HostChange.DELETE, host_name)
            changes.addChange(change)
            self.packet_channel.sendPacket(packet.DockChangesPacket(changes))
        except:
            # If unable to send packet, notify caller
            raise ConnectionError(self, 'Unable to send DockChangesPacket packet to delete a dock host.')

    ###########################################################################
    # Used only for hosting dockserver administration.  Creates the passed customer
    # on the connected dockserver.
    #
    # Arguments:
    #  customer_name - (string) Name of the customer to create on this proxy's
    #    dockserver.
    #
    def addCustomer(self, customer_name):
        try:
            changes = packet.HostChanges(packet.HostChanges.DOCK_HOST_CHANGES_TAG)
            change = packet.HostCustomerChange(packet.HostChange.ADD,
                                           name = customer_name)
            changes.addChange(change)
            self.packet_channel.sendPacket(packet.DockChangesPacket(changes))
        except:
            # If unable to send packet, notify caller
            raise ConnectionError(self, 'Unable to send DockChangesPacket packet to add a customer.')

    ###########################################################################
    # Used only for hosting dockserver administration.  Removes the passed customer
    # from this proxy's dockserver.
    #
    # Arguments:
    #  customer_name - (string) Name of the customer to remove from this dock proxy's
    #    actual dockserver.
    #
    def removeCustomer(self, customer_name):
        try:
            changes = packet.HostChanges(packet.HostChanges.DOCK_HOST_CHANGES_TAG)
            change = packet.HostCustomerChange(packet.HostChange.DELETE,
                                           name = customer_name)
            changes.addChange(change)
            self.packet_channel.sendPacket(packet.DockChangesPacket(changes))
        except:
            # If unable to send packet, notify caller
            raise ConnectionError(self, 'Unable to send DockChangesPacket packet to delete a customer.')

    ###########################################################################
    # Used only for hosting dockserver administration.  Creates the passed user
    # on the connected dockserver.
    #
    # Arguments:
    #  user_name - (string) Name of the user to create on this proxy's
    #    dockserver.
    #  customer_name - (string) Add the passed user as a member of this customer.
    #    Can be None.
    #
    def addUser(self, user_name, customer_name):
        try:
            changes = packet.HostChanges(packet.HostChanges.DOCK_HOST_CHANGES_TAG)
            userChange = packet.HostUserChange(packet.HostChange.ADD, user_name)
            changes.addChange(userChange)
            if customer_name:
                # If a customer name is passed, then add this user as a member.
                memberChange = packet.HostMemberChange(packet.HostChange.ADD, user_name, customer_name)
                changes.addChange(memberChange)
            self.packet_channel.sendPacket(packet.DockChangesPacket(changes))
        except:
            # If unable to send packet, notify caller
            raise ConnectionError(self, 'Unable to send DockChangesPacket packet to add a user.')

    ###########################################################################
    # Used only for hosting dockserver administration.  Removes the passed user
    # from this proxy's dockserver.
    #
    # Arguments:
    #  user_name - (string) Name of the user to remove from this dock proxy's
    #    actual dockserver.
    #
    def removeUser(self, user_name):
        try:
            changes = packet.HostChanges(packet.HostChanges.DOCK_HOST_CHANGES_TAG)
            change = packet.HostUserChange(packet.HostChange.DELETE, user_name)
            changes.addChange(change)
            self.packet_channel.sendPacket(packet.DockChangesPacket(changes))
        except:
            # If unable to send packet, notify caller
            raise ConnectionError(self, 'Unable to send DockChangesPacket packet to delete a user.')

    ###########################################################################
    # Used only for hosting dockserver administration.  Modifies the passed user
    # passed on the passed information - only add / remove user from passed customer
    # group for now.
    #
    # Arguments:
    #  action - (int) User modify action - add / remove from customer group.
    #  user_name - (string) Name of user to modify.
    #  customer_name - (string) Add or remove the passed user to this customer.
    #
    def modUser(self, action, user_name, customer_name):
        try:
            if not customer_name:
                # If no customer name provided, notify caller.
                raise ProxyError(self, 'No customer name provided.  DockChangesPacket to modify a user cancelled.')
            if action == DockProxy.ADD_MEMBERSHIP_ACTION:
                host_change = packet.HostChange.ADD
            else:
                host_change = packet.HostChange.DELETE
            changes = packet.HostChanges(packet.HostChanges.DOCK_HOST_CHANGES_TAG)
            change = packet.HostMemberChange(host_change, user_name, customer_name)
            changes.addChange(change)
            self.packet_channel.sendPacket(packet.DockChangesPacket(changes))
        except:
            # If unable to send packet, notify caller
            raise ConnectionError(self, 'Unable to send DockChangesPacket packet to modify a user.')

    def modPrivilege(self, customer_name, host_name, view_host, view_gliders, view_serial_ports):
        '''
        Used only for hosting dockserver administration.  Modifies the privileges
        of the passed customer on the passed dockserver host.

        Arguments:
          customer_name - (string) modify this customer's privileges.
          host_name - (string) the dockserver host whose customer privileges are modified.
          view_hosts - (True, False, None) grant / revoke / no change to view host privilege
            If granted, allows the passed customer to connection to this host.
          view_gliders - (True, False, None) grant / revoke / no change the passed customer
            to view the passed host's glider I/O.
          view_serial_ports - (True, False, None) grant / revoke / no change the passed customer's
            privilege to view the passed host's serial port I/O.
        '''
        try:
            if not customer_name or not host_name:
                # If no customer name or no host_nameprovided, notify caller.
                raise ProxyError(self, 'No customer name or host name provided.  DockChangesPacket to modify a customer privilege cancelled.')

            if view_host is None and view_gliders is None and view_serial_ports is None:
                # If no changes to make, then don't send a change request packet to dockserver
                return

            changes = packet.HostChanges(packet.HostChanges.DOCK_HOST_CHANGES_TAG)
            change = packet.DockHostPrivilegeChange(customer_name, host_name, view_host, view_gliders, view_serial_ports)
            changes.addChange(change)
            self.packet_channel.sendPacket(packet.DockChangesPacket(changes))
        except:
            # If unable to send packet, notify caller
            raise ConnectionError(self, 'Unable to send DockChangesPacket packet to modify customer privileges.')

###############################################################################
# Proxy class for data server information and data that resides on an actual
# dataserver machine.
#
class DataProxy(object):
    '''
    Attributes:
      host - (string) Domain name or IP address of the actual dataserver
      port - (int) The port number of the listening socket on the dataserver.
      server_host - (string) The hosting dataserver identifier (i.e., 'twr' in //localhost:6600/twr)
      packet_channel - (PacketChannel) The network connection with the dataserver.
      client_id - (string) The client ID of the application using this dataserver proxy.  Sent to
        the actual dataserver to determine feature support.
      client_version - (string) The client version of the application using this dataserver proxy.
        Sent to the actual dataserver to determine feature support.
      proxy_publisher - (publisher.Publisher) Maintains dock proxy event subscribers and performs
        event notifications.
    '''

    # Class Constants
    DEFAULT_DATASERVER_CLIENT_PORT = 6600
    # Default hosting dataserver identifier
    DEFAULT_DATASERVER_HOST = 'default'
    # Default client ID and version.  These should be set by the client application for correct operation.
    DEFAULT_CLIENT_ID = 'Teledyne-Webb Mission Planner'
    DEFAULT_CLIENT_VERSION = '1.0'

    # Host change actions
    ADD_MEMBERSHIP_ACTION = 1
    DELETE_MEMBERSHIP_ACTION = 2

    # Subscriber events
    ON_CLOSE = 'onClose(DataProxy)'
    ON_MESSAGE = 'onMessage(DockProxyEvent)'

    ###########################################################################
    # Class DataProxy constructor.
    #
    # Arguments:
    #  host - (string) Host domainname or dotted IP address.
    #  port - (int) TCP/IP port that dataserver listens for connections.
    #
    def __init__(self, host, port=DEFAULT_DATASERVER_CLIENT_PORT):
        '''
        Initialize this dataproxy and set up to send packets to actual dataserver.
        '''
        self.host = host
        self.port = port
        self.client_id = DataProxy.DEFAULT_CLIENT_ID
        self.client_version = DataProxy.DEFAULT_CLIENT_VERSION
        self.server_host = DataProxy.DEFAULT_DATASERVER_HOST
        self.packet_channel = None
        self.proxy_publisher = publisher.Publisher()

    ###########################################################################
    # Sets the hosting dataserver identifier
    #
    # Arguments:
    #  dataserver_host - (string) The identifier of the hosting dataserver.
    #
    def setServerHost(self, server_host):
        self.server_host = server_host

    ###########################################################################
    # Sets the hosting dataserver identifier
    #
    # Arguments:
    #  client_id - (string) The calling application's client ID (as known to
    #    dataserver).  Used to determine feature support by dataserver.
    #  client_version - (string) The calling application's client version (as
    #    understood by dataserver).  Used to determine feature support by
    #    dataserver.
    #
    def setClientVersion(self, client_id, client_version):
        self.client_id = client_id
        self.client_version = client_version

    ###########################################################################
    # Returns the server host name of this proxy's actual hosting dataserver.
    #
    # Returns:
    #  string - server host name
    #
    def getServerHost(self):
        return self.server_host

    ###########################################################################
    # Returns the dataserver's hostname and port number.
    #
    # Returns:
    #  string - Dataserver host name and port number
    #
    def getName(self):
        '''
        Return this dock proxy's string identifier (or name).  This identifier is the dockserver's
        domain name with the network port appended.
        '''
        return self.host + ':' + str(self.port)

    ###########################################################################
    # Open a continuous network connection to the actual dataserver machine.
    #
    # Exceptions:
    #  ConnectionError - thrown if unable to establish a network connection to
    #    the actual dataserver machine.
    #
    def openConnection(self, request_log_events = True):
        '''
        Start a thread that opens a socket to the dataserver and then enter
        loop listening for dataserver packets and processing them.
        '''
        try:
            if self.packet_channel:
                # If a connection is open, close it
                self.closeConnection()
            # open a new connection
            self.packet_channel = packet.PacketChannel(self.host, self.port, self.client_id, self.client_version)
            self.packet_channel.setServerHost(self.server_host)
            self.packet_channel.addSubscriber(packet.PacketChannel.ON_PACKET, self.onPacket)
            self.packet_channel.addSubscriber(packet.PacketChannel.ON_CLOSE, self.onClose)
            self.packet_channel.openChannel()
        except:
            # If unable to open a connection, then notify caller.
            raise ConnectionError(self, 'Unable to open network connection to Data Server.')

    ###########################################################################
    # Close the network connection to this client's dataserver machine.
    #
    # Exceptions:
    #  ConnectionError - Thrown if unable to close the network connection to the dataserver.
    #
    def closeConnection(self):
        '''
        Close the packet channel to this dock proxy's dataserver.
        '''
        try:
            if self.packet_channel:
                self.packet_channel.removeSubscriber(packet.PacketChannel.ON_PACKET, self.onPacket)
                self.packet_channel.removeSubscriber(packet.PacketChannel.ON_CLOSE, self.onClose)
                self.packet_channel.closeChannel()
        except:
            raise ConnectionError(self, 'Unable to close network connection to Data Server.')
        finally:
            self.packet_channel = None

    ###########################################################################
    # Used only for hosting dataserver administration.  Creates the passed host
    # on the connected dataserver.
    #
    # Arguments:
    #  host_name - (string) Name of the host dataserver to create on this proxy's
    #    dataserver.
    #
    def addHost(self, host_name, config_filename):
        try:
            changes = packet.HostChanges(packet.HostChanges.DATA_HOST_CHANGES_TAG)
            change = packet.HostHostChange(packet.HostChange.ADD, host_name, filename = config_filename)
            changes.addChange(change)
            self.packet_channel.sendPacket(packet.DockChangesPacket(changes))
        except:
            # If unable to send packet, notify caller
            raise ConnectionError(self, 'Unable to send DockChangesPacket packet to add a data host.')

    ###########################################################################
    # Used only for hosting dataserver administration.  Removes the passed host
    # from this proxy's dataserver.
    #
    # Arguments:
    #  host_name - (string) Name of the host to remove from this dock proxy's
    #    actual dataserver.
    #
    def removeHost(self, host_name):
        try:
            changes = packet.HostChanges(packet.HostChanges.DATA_HOST_CHANGES_TAG)
            change = packet.HostHostChange(packet.HostChange.DELETE, host_name)
            changes.addChange(change)
            self.packet_channel.sendPacket(packet.DockChangesPacket(changes))
        except:
            # If unable to send packet, notify caller
            raise ConnectionError(self, 'Unable to send DockChangesPacket packet to delete a data host.')

    ###########################################################################
    # Used only for hosting dataserver administration.  Creates the passed customer
    # on the connected dataserver.
    #
    # Arguments:
    #  customer_name - (string) Name of the customer to create on this proxy's
    #    dataserver.
    #
    def addCustomer(self, customer_name):
        try:
            changes = packet.HostChanges(packet.HostChanges.DATA_HOST_CHANGES_TAG)
            change = packet.HostCustomerChange(packet.HostChange.ADD,
                                           name = customer_name)
            changes.addChange(change)
            self.packet_channel.sendPacket(packet.DockChangesPacket(changes))
        except:
            # If unable to send packet, notify caller
            raise ConnectionError(self, 'Unable to send DockChangesPacket packet to add a customer.')

    ###########################################################################
    # Used only for hosting dataserver administration.  Removes the passed customer
    # from this proxy's dataserver.
    #
    # Arguments:
    #  customer_name - (string) Name of the customer to remove from this dock proxy's
    #    actual dataserver.
    #
    def removeCustomer(self, customer_name):
        try:
            changes = packet.HostChanges(packet.HostChanges.DATA_HOST_CHANGES_TAG)
            change = packet.HostCustomerChange(packet.HostChange.DELETE,
                                           name = customer_name)
            changes.addChange(change)
            self.packet_channel.sendPacket(packet.DockChangesPacket(changes))
        except:
            # If unable to send packet, notify caller
            raise ConnectionError(self, 'Unable to send DockChangesPacket packet to delete a customer.')

    ###########################################################################
    # Used only for hosting dataserver administration.  Creates the passed user
    # on the connected dataserver.
    #
    # Arguments:
    #  user_name - (string) Name of the user to create on this proxy's
    #    dataserver.
    #  customer_name - (string) Add the passed user as a member of this customer.
    #    Can be None.
    #
    def addUser(self, user_name, customer_name):
        try:
            changes = packet.HostChanges(packet.HostChanges.DATA_HOST_CHANGES_TAG)
            userChange = packet.HostUserChange(packet.HostChange.ADD, user_name)
            changes.addChange(userChange)
            if customer_name:
                # If a customer name is passed, then add this user as a member.
                memberChange = packet.HostMemberChange(packet.HostChange.ADD, user_name, customer_name)
                changes.addChange(memberChange)
            self.packet_channel.sendPacket(packet.DockChangesPacket(changes))
        except:
            # If unable to send packet, notify caller
            raise ConnectionError(self, 'Unable to send DockChangesPacket packet to add a user.')

    ###########################################################################
    # Used only for hosting dataserver administration.  Removes the passed user
    # from this proxy's dataserver.
    #
    # Arguments:
    #  user_name - (string) Name of the user to remove from this data proxy's
    #    actual dataserver.
    #
    def removeUser(self, user_name):
        try:
            changes = packet.HostChanges(packet.HostChanges.DATA_HOST_CHANGES_TAG)
            change = packet.HostUserChange(packet.HostChange.DELETE, user_name)
            changes.addChange(change)
            self.packet_channel.sendPacket(packet.DockChangesPacket(changes))
        except:
            # If unable to send packet, notify caller
            raise ConnectionError(self, 'Unable to send DockChangesPacket packet to delete a user.')

    ###########################################################################
    # Used only for hosting dataserver administration.  Modifies the passed user
    # passed on the passed information - only add / remove user from passed customer
    # group for now.
    #
    # Arguments:
    #  action - (int) User modify action - add / remove from customer group.
    #  user_name - (string) Name of user to modify.
    #  customer_name - (string) Add or remove the passed user to this customer.
    #
    def modUser(self, action, user_name, customer_name):
        try:
            if not customer_name:
                # If no customer name provided, notify caller.
                raise ProxyError(self, 'No customer name provided.  DockChangesPacket to modify a user cancelled.')
            if action == DataProxy.ADD_MEMBERSHIP_ACTION:
                host_change = packet.HostChange.ADD
            else:
                host_change = packet.HostChange.DELETE
            changes = packet.HostChanges(packet.HostChanges.DATA_HOST_CHANGES_TAG)
            change = packet.HostMemberChange(host_change, user_name, customer_name)
            changes.addChange(change)
            self.packet_channel.sendPacket(packet.DockChangesPacket(changes))
        except:
            # If unable to send packet, notify caller
            raise ConnectionError(self, 'Unable to send DockChangesPacket packet to modify a user.')

    def modPrivilege(self, customer_name, host_name, view_data):
        '''
        Used only for hosting dataserver administration.  Modifies the privileges
        of the passed customer on the passed dataserver host.

        Arguments:
          customer_name - (string) modify this customer's privileges.
          host_name - (string) the dataserver host whose customer privileges are modified.
          view_data - (True, False, None) grant / revoke / no change to view host data privilege
            If granted, allows the passed customer to connection to this host.
        '''
        try:
            if not customer_name or not host_name:
                # If no customer name or no host_nameprovided, notify caller.
                raise ProxyError(self, 'No customer name or host name provided.  DockChangesPacket to modify a customer privilege cancelled.')

            if view_data is None:
                # If no changes to make, then don't send a change request packet to dataserver
                return

            changes = packet.HostChanges(packet.HostChanges.DATA_HOST_CHANGES_TAG)
            change = packet.DataHostPrivilegeChange(customer_name, host_name, view_data)
            changes.addChange(change)
            self.packet_channel.sendPacket(packet.DockChangesPacket(changes))
        except:
            # If unable to send packet, notify caller
            raise ConnectionError(self, 'Unable to send DockChangesPacket packet to modify customer privileges.')

    ###########################################################################
    # Add the passed subscriber function as the method to call when the passed
    # event topic is fired.
    #
    # Arguments:
    #  topic - (string) The event to subscribe to.
    #  subscriber - (function) The method to call when the passed event topic is fired
    #    (a listener for the event).
    #
    # Exceptions:
    #  ProxyError - Thrown if unable to add subscriber to the passed event.
    #
    def addSubscriber(self, topic, subscriber):
        '''
        Add the passed subscriber to be notified when events related to the passed topic occur.
        '''
        # Add the passed subscriber as an event listener
        try:
            self.proxy_publisher.AddSubscriber(topic, subscriber)
        except:
            raise ProxyError(self, 'Unable to add subscriber to event: ' + topic)

    ###########################################################################
    # Remove the passed subscriber from the passed event topic.  This subscriber
    # will no longer receive event notifications for this topic.
    #
    # Arguments:
    #  topic - (string) The event to UN-subscribe to.
    #  subscriber - (function) The method to remove from event notification
    #    (a listener for the event).
    #
    # Exceptions:
    #  ProxyError - Thrown if unable to remove the subscriber from the event.
    #
    def removeSubscriber(self, topic, subscriber):
        '''
        Remove the passed subscriber from those clients notified when events related to the passed
        topic occur.
        '''
        # Remove the passed subscriber from packet event notification.
        try:
            self.proxy_publisher.RemoveSubscriber(topic, subscriber)
        except:
            raise ProxyError(self, 'Unable to remove subscriber from event: ' + topic)

    def addSubscription(self, dataserver_host_name, dockserver_host_uri, dataserver_host_proxy_uri = None):
        '''
        Add a dockserver data subscription of the passed dockserver_host_uri to the passed
        dataserver_host_name on the local dataserver.

        Arguments:
          dataserver_host_name - (string) The dockserver data subscription will be added to this
            dataserver host name.
          dockserver_host_uri - (string) The dockserver host that will notify the passed dataserver
            host when new glider data arrives.
          dataserver_host_proxy_uri - (string) Optional URI of the dataserver host's proxy.  If not provided
            the dockserver host will use the subscription return address to find the dataserver host.
        '''
        try:
            if not dataserver_host_name or not dockserver_host_uri:
                # If no dataserver host name or dockserver host uri provided, notify caller.
                raise ProxyError(self, 'No dataserver host name or dockserver host URI provided.  DockChangesPacket to add dataserver data subscription cancelled.')

            change = packet.DockSubscriptionChange(packet.DockSubscriptionChange.ADD, dataserver_host_name, dockserver_host_uri)
            change.setDataHostProxyUri(dataserver_host_proxy_uri)
            self.packet_channel.sendPacket(packet.DockChangesPacket(change))
        except:
            # If unable to send packet, notify caller
            raise ConnectionError(self, 'Unable to send DockChangesPacket packet to add dataserver data subscription.')

    def removeSubscription(self, dataserver_host_name, dockserver_host_uri):
        '''
        Delete the glider data subscription that the passed dataserver_host_name has to the passed
        dockserver_host_uri.

        Arguments:
          dataserver_host_name - (string) The dockserver data subscription will be removed from this
            local dataserver host name.
          dockserver_host_uri - (string) The dockserver data subscription to remove from the passed
            dataserver host name.
        '''
        try:
            if not dataserver_host_name or not dockserver_host_uri:
                # If no dataserver host name or dockserver host uri provided, notify caller.
                raise ProxyError(self, 'No dataserver host name or dockserver host URI provided.  DockChangesPacket to remove dataserver data subscription cancelled.')

            change = packet.DockSubscriptionChange(packet.DockSubscriptionChange.DELETE, dataserver_host_name, dockserver_host_uri)
            self.packet_channel.sendPacket(packet.DockChangesPacket(change))
        except:
            # If unable to send packet, notify caller
            raise ConnectionError(self, 'Unable to send DockChangesPacket packet to add dataserver data subscription.')

    def modSubscription(self, dataserver_host_name, dockserver_host_uri, include_pattern = None, exclude_pattern = None):
        '''
        Modify the dockserver data subscription identified by the passed dockserver_host_uri of the
        passed dataserver_host_name by replacing its glider name include and exclude regular expressions
        with the passed patterns.

        Arguments:
          dataserver_host_name - (string) The local dataserver host whose subscriptions will be modified.
          dockserver_host_uri - (string) The specific dockserver host subscription to modify.
          include_pattern - (string) The new glider name include regular expression.
          exclude_pattern - (string) The new glider name exclude regular expression.
        '''
        try:
            if not dataserver_host_name or not dockserver_host_uri:
                # If no dataserver host name or dockserver host uri provided, notify caller.
                raise ProxyError(self, 'No dataserver host name or dockserver host URI provided.  DockChangesPacket to modify dataserver data subscription cancelled.')

            change = packet.DockSubscriptionChange(packet.DockSubscriptionChange.MOD, dataserver_host_name, dockserver_host_uri)
            change.setPatterns(include_pattern, exclude_pattern)
            self.packet_channel.sendPacket(packet.DockChangesPacket(change))
        except:
            # If unable to send packet, notify caller
            raise ConnectionError(self, 'Unable to send DockChangesPacket packet to modify dataserver data subscription.')

    ###########################################################################
    # Packet channel event handler for the channel close event.  Notifies all
    # DataProxy subscribers of the ON_CLOSE event.
    #
    # Threading - packet.PacketChannel.peer_monitor_timer thread and
    # packet.PacketChannel.asyncore.loop thread
    #
    def onClose(self):
        '''
        Handle the onClose event from a PacketChannel instance.  Called when the network channel
        is closed by the peer (or some inbetween router, etc...)
        '''
        #print 'In DataProxy.onClose for ' + self.host + ':' + str(self.port) + '...'
        # Pass this event to subscribers for handling as they wish.
        self.proxy_publisher.FireEvent(DataProxy.ON_CLOSE, self)

    ###########################################################################
    # Event handler for PacketChannel receive packets Event.
    #
    # Threading - packet.PacketChannel.receivePacketThread
    #
    # Arguments:
    #  packet_received - (subclass of Packet) The packet just received from the
    #    dataserver.
    #
    def onPacket(self, packet_received):
        '''
        Handles onPacket events from a PacketChannel instance.  Called each time a packet arrives
        from the dataserver.
        '''
        #print 'In DockProxy.onPacket for ' + self.host + ':' + str(self.port) + '...'
        #print str(packet_received)

        # Handle packets
        if packet_received.getAction() == packet.Packet.DOCK_STATUS:
            # If received packet is a dock status packet, then notify subscribers
            #print 'Received DOCK_STATUS packet.  Fire ON_MESSAGE event.'
            self.proxy_publisher.FireEvent(DataProxy.ON_MESSAGE, DockProxyEvent(self, packet_received.getMessage()))
###############################################################################
# Proxy class for glider information and data that resides on an actual dockserver
# machine.  All glider specific communication between this client and dockserver
# passes through this class.
#
# Publish / Subscribe Interface - This class supports the following events.
#
# onDock(GliderProxy, Boolean) subscribe to event topic GliderProxy.ON_DOCK.
#  Fired when a glider status packet is received from the dockserver.
#
# onAbort(GliderProxy, GliderAbort) subscribe to event topic GliderProxy.ON_ABORT.
#  Fired when a new glider abort occurs and the glider has come to the surface.
#
# onMissionStatus(GliderProxy, GliderMission) subscribe to event topic GliderProxy.ON_MISSION_STATUS.
#  Fired when a glider status packet is received that contains mission status data.
#
# onPosition(GliderProxy, GliderPosition) subscribe to event topic GliderProxy.ON_POSITION.
#  Fired when a glider status packet is received that contains GPS position data.
#
# onClose(GliderProxy) subscribe to event topic GliderProxy.ON_CLOSE.
#  Fired when the network connection to dockserver closes.
#
# onData(GliderProxy, list) subscribe to event topic GliderProxy.ON_DATA
#  Fired when a glider status packet is received that contains glider data filenames most
#  recently transferred to the dockserver from the glider.
#
# onSensors(GliderProxy, GliderSurfaceSensors) subscribe to event topic GliderProxy.ON_SENSORS
#  Fired when a glider status packet is received that contains the values of glider sensors
#  reported in the durface dialog (config.srf).
#
class GliderProxy(object):
    '''
    Attributes:
      dock_proxy - (DockProxy) The proxy of the dockserver that manages this glider.
      name - (string) The glider's name
      packet_channel - (PacketChannel) The network connection to the actual dockserver
        over which packets are sent and received.
      proxy_publisher - (publisher.Publisher) Manages subscribers and event notifications
        for glider proxy events.
      is_docked - (boolean) True if the actual glider is in communication with the actual
        dockserver.  Otherwise, False.
      last_abort - (packet.GliderAbort) The last known abort that occured on this glider.
      last_mission - (packet.GliderMissionStatus) Status of last known mission running on
        the glider.
      last_position - (packet.GliderPosition) Last known GPS position of this glider.
      track - (list of packet.GliderPosition) The current GPS position track of this glider.
      current_mission_sequence - (packet.MissionSequence) The staged mission sequence that
        is currently running on the glider.
      data_filenames - (list of string) filenames of last batch of zmodem data files sent
        from the glider to dockserver.
      surface_sensors - (packet.GliderSurfaceSensors)  Sensors reported in glider surface dialog
        during the last surfacing.  Their names, values, and units.
      persistent_sensor_result - (SurfaceSensorResultNames or SurfaceSensorResultData) Information
        retrieved about glider surface sensors.  Set by recieving a GliderSurfaceSensorResopnse packet from
        the dockserver.
      is_write_track_event - (threading.Event) Signals that a WriteTrackPacket has been received as the
        reponse to the last ReadTrackPacket sent to the dockserver.  Used to block caller to getTrack()
        until the track has been received from dockserver.
      is_write_current_sequence_event - (threading.Event) Signals that a WriteCurrentSequencePacket has been
        received in response to the last ReadCurrentSequencePacket sent to the dockserver.  Used to block
        caller to getCurrentMissionSequence() until the current sequence has been received from dockserver.
      is_initialized_event - (threading.Event) Signals that this instance of GliderProxy has been initialized
        with a GliderStatueResponse packet from the dockserver.  This packet contains the glider's last known
        abort, mission, and position information.
      is_persistent_sensor_response_event - (threading.Event) Signals that a GliderSurfaceSensorResponse packet
        has been received in reply to a sent GliderSurfaceSensorRequest packet.  Used to block the caller to
        getPersistentSensorNames() and getPersistentSensorData() calls until a response packet from the dockserver
        is received.
      glider_link_proxy - (packet.GliderLinkProxy) None if glider is not docked.  Otherwise, it contains the
        serial port and device used to communicate with a docked glider.  Devcie precedence - direct over
        freewave over iridium.
    '''
    # Class constants
    ON_DOCK = 'onDock(GliderProxy, Boolean)'
    ON_ABORT = 'onAbort(GliderProxy, GliderAbort)'
    ON_MISSION_STATUS = 'onMissionStatus(GliderProxy, GliderMission)'
    ON_POSITION = 'onPosition(GliderProxy, GliderPosition)'
    ON_CLOSE = 'onClose(GliderProxy)'
    ON_DATA = 'onData(GliderProxy, list)'
    ON_SENSORS = 'onSensors(GliderProxy, GliderSurfaceSensors)'
    ON_DIALOG = 'onDialog(GliderProxy, GliderLinkProxy, string)'

    ###########################################################################
    # Create a glider proxy from the passed information.
    #
    # Arguments:
    #  dock_proxy - (DockProxy) The dockserver proxy that manages this glider
    #    proxy instance.
    #  name - (string) The glider's name.
    #
    def __init__(self, dock_proxy, name):
        self.dock_proxy = dock_proxy
        self.name = name
        self.packet_channel = None
        self.proxy_publisher = publisher.Publisher()
        self.is_docked = False
        self.last_abort = None
        self.last_mission = None
        self.last_position = None
        self.track = []
        self.current_mission_sequence = None
        self.data_filenames = []
        self.surface_sensors = None
        self.persistent_sensor_result = None
        self.primary_link_proxy = None
        self.secondary_link_proxy = None

        # Event signal that Dock Server has responded with a WriteTrackPacket to the
        # last sent ReadTrackPacket (via self.getTrack()).
        self.is_write_track_event = threading.Event()
        # Event signal that Dock SErver has responded with a WriteCurrentSequencePacket to the
        # last send ReadCurrentSequencePacket (via getCurrentMissionSequence() call).
        self.is_write_current_sequence_event = threading.Event()
        # Event signal that this glider proxy is initialize by the first GliderStatusResponse
        # packet from dockserver
        self.is_initialized_event = threading.Event()
        # Event signal that Dock Server has responded with a GliderSurfaceSensorResponse packet
        # to the last request for persistent sensor names.
        self.is_persistent_sensor_response_event = threading.Event()

    ###########################################################################
    # Release all network resources.
    #
    def __del__(self):
        '''
        Destructor - close this proxy's network connection.
        '''
        self.closeConnection()

    ###########################################################################
    # PUBLIC Interface
    ###########################################################################

    ###########################################################################
    # Establishes a network connection between this proxy and the actual dockserver
    # machine where this glider is managed.  Once open, sends packets to dockserver
    # requesting this glider's current status information.
    #
    # Exceptions:
    #  ConnectionError - thrown if unable to establish a network connection to the dockserver.
    #
    def openConnection(self):
        '''
        Open a persistent network connection to this glider's dockserver and request this glider's
        status.
        '''
        try:
            if self.packet_channel:
                # If a connection is open, close it
                self.closeConnection()
            # open a new connection
            self.packet_channel = packet.PacketChannel(self.dock_proxy.getHost(), self.dock_proxy.getPort(), self.dock_proxy.getClientID(), self.dock_proxy.getClientVersion())
            # Subscribe to packet channel events.
            self.packet_channel.addSubscriber(packet.PacketChannel.ON_PACKET, self.onPacket)
            self.packet_channel.addSubscriber(packet.PacketChannel.ON_CLOSE, self.onClose)
            self.packet_channel.openChannel()
        except:
            # If unable to open a connection, then notify caller.
            raise ConnectionError(self, 'Unable to open network connection to Dock Server.')

        try:
            # Request this glider's status
            self.packet_channel.sendPacket(packet.GliderStatusRequest(self))
            # Request continuous status updates.
            self.packet_channel.sendPacket(packet.GliderConnectRequest(self))
        except:
            # If unable to send packet, notify caller
            raise ConnectionError(self, 'Unable to send GliderStatusRequest packet.')

    ###########################################################################
    # Releases all network resources used to communicate to the dockserver.
    #
    # Exceptions:
    #  ConnectionError - thrown if unable to close the network socket connection.
    #
    def closeConnection(self):
        '''
        Close the packet channel to this dock proxy's dockserver.
        '''
        try:
            if self.packet_channel:
                self.packet_channel.removeSubscriber(packet.PacketChannel.ON_PACKET, self.onPacket)
                self.packet_channel.removeSubscriber(packet.PacketChannel.ON_CLOSE, self.onClose)
                self.packet_channel.closeChannel()
        except:
            raise ConnectionError(self, 'Unable to close network connection to Dock Server.')
        finally:
            self.packet_channel = None

    ###########################################################################
    # Add the passed subscriber (function) to the list of methods called when
    # the event with the passed topic is fired.
    #
    # Arguments:
    #  topic - (string) The identifier of the event.
    #  subscriber - (function) The method to call when the event associated with
    #    the topic is fired.
    #
    # Exceptions:
    #  ProxyError - thrown if unable to add the passed subscriber.
    #
    def addSubscriber(self, topic, subscriber):
        # Add the passed subscriber as an event listener
        try:
            self.proxy_publisher.AddSubscriber(topic, subscriber)
        except:
            raise ProxyError(self, 'Unable to add subscriber to event: ' + topic)

    ###########################################################################
    # Remove the passed subscriber from the list of methods to call when the event
    # associated with the passed topic is fired.
    #
    # Arguments:
    #  topic - (string) The identifier of the event.
    #  subscriber - (function) The method to remove from those called when the event
    #    associated with the passed topic is fired.
    #
    # Exceptions:
    #  ProxyError - thrown if unable to remove the passed subscriber.
    #
    def removeSubscriber(self, topic, subscriber):
        # Remove the passed subscriber from packet event notification.
        try:
            self.proxy_publisher.RemoveSubscriber(topic, subscriber)
        except:
            raise ProxyError(self, 'Unable to remove subscriber from event: ' + topic)

    ###########################################################################
    # Returns this glider's name as a string.
    #
    # Returns:
    #  string - This glider's name.
    #
    def getName(self):
        return self.name

    ###########################################################################
    # Returns this glider's dock proxy
    #
    # Returns:
    #  DockProxy - This glider's dock proxy.
    #
    def getDockProxy(self):
        return self.dock_proxy

    ###########################################################################
    # Returns this glider's last known mission status as a GliderMissionStatus
    # instance.  Will block caller until this glider is initialized with information
    # from dockserver.
    #
    # Returns:
    #  packet.GliderMissionStatus - Last known running mission for this glider.
    #
    def getLastMissionStatus(self):
        self.waitForInitialize()
        return self.last_mission

    ###########################################################################
    # Returns this glider's last known abort as a GliderAbort instance.  Will
    # block caller until this glider is initialized with information from
    # dockserver.
    #
    # Returns:
    #  packet.GliderAbort - Last known abort on this glider.
    #
    def getLastAbort(self):
        self.waitForInitialize()
        return self.last_abort

    ###########################################################################
    # Returns True if this glider is currently in communication with its dockserver.
    # Otherwise, returns False.  Will block the caller until this glider is
    # initialized with information from dockserver.
    #
    # Returns:
    #  bool - True if this actual glider corresponding to this proxy is in
    #    communication with the dockserver.
    #
    def isDocked(self):
        self.waitForInitialize()
        return self.is_docked

    ###########################################################################
    # Returns this glider's last known GPS position as a GliderPosition
    # instance.  Will block the caller until this glider is initialized with
    # information from dockserver.
    #
    # Returns:
    #  packet.GliderPosition - Last known glider GPS position.
    #
    def getLastPosition(self):
        self.waitForInitialize()
        return self.last_position

    ###########################################################################
    # Returns this glider's sensor values displayed during the last known surfacing
    # as a GliderSurfaceSensors instance.  Will block the caller until this glider
    # is initialized with information from dockserver.
    #
    # Returns:
    #  packet.GliderSurfaceSensors - Last known values for glider sensors displayed
    #    during surfacing (config.srf)
    #
    def getSurfaceSensors(self):
        self.waitForInitialize()
        return self.surface_sensors

    ###########################################################################
    # Returns the last known set of glider data filenames as a list of strings.
    # Will block the caller until this glider is initialized with information
    # from dockserver.
    #
    # Returns:
    #  list of strings - The glider data filenames in the most recent transfer
    #    of files to the dockserver from the glider.
    #
    def getDataFilenames(self):
        self.waitForInitialize()
        return self.data_filenames

    ###########################################################################
    # Returns this glider's track as a list of GliderPosition instances where
    # the positions' timestamp occurs between the passed start_date and end_date.
    # If no dates are passed, the entire historical track is returned.  The caller
    # is blocked while this glider's dockserver is queried for the track data.
    #
    # Arguments:
    #  start_date - (datetime.datetime) Restricts returned positions to have a
    #    time stamp that occurs after this time.
    #  end_date - (datetime.datetime) Restricts returned positions to have a
    #    time stamp that occurs before this time.
    #
    # Returns:
    #  A list of GPS positions where each position is an instance of GliderPosition
    #
    # Exceptions:
    #  TimeoutError - thrown if dockserver does not response to the track request
    #    within the timeout period (~ 30 sec).
    #  ProxyError - Thrown if the network connection to dockserver is not open.
    #
    def getTrack(self, start_date=None, end_date=None):
        # Check for open connection
        if not self.packet_channel:
            raise ProxyError(self, 'Connection to Dock Server is not open.  Try calling openConnection()')
        # Clear the thread sync event that represents receiving a WriteTrackPacket for
        # a sent ReadTrackPacket
        self.is_write_track_event.clear()
        # Send a request to Dock Server for this glider's track.
        read_track = packet.ReadTrackPacket(self, start_date, end_date)
        self.packet_channel.sendPacket(read_track)
        # Wait for the Dock Server to respond with a WriteTrackPacket
        self.is_write_track_event.wait(CONNECTION_RESPONSE_TIMEOUT)
        if not self.is_write_track_event.isSet():
            # If timed out waiting for Dock Server response packet, then raise an exception.
            raise TimeoutError(self, 'Timeout waiting to receive a WriteTrackPacket.')
        # return the just received track to the caller.
        return self.track


    def getPrimaryLink(self):
        '''
        Purpose: Return the primary glider link proxy associated with this glider proxy.

        Returns:
          (GliderLinkProxy) - The primary glider link proxy.
        '''
        return self.primary_link_proxy

    def getSecondaryLink(self):
        '''
        Purpose: Return the secondary glider link proxy associated with this glider proxy.

        Returns:
          (GliderLinkProxy) - The secondary glider link proxy.
        '''
        return self.secondary_link_proxy

    ###########################################################################
    # Stage the passed mission sequence by sending it to the dockserver.  When
    # the glider next surfaces, these missions will be transferred to the glider
    # and sequenced for running.
    #
    # Arguments:
    #  mission_sequence - (packet.MissionSequence) The sequence of missions to
    #    to send to the dockserver for staging to the glider.  This sequence
    #    includes all mi, ma, and gbf for each mission in the sequence.
    #
    # Exceptions:
    #  ProxyError - thrown if a packet channel to the dockserver has not been opened.
    #
    def stageMissionSequence(self, mission_sequence):
        # Check for open connection
        if not self.packet_channel:
            raise ProxyError(self, 'Connection to Dock Server is not open.  Try calling openConnection()')
        write_packet = packet.WriteMissionSequencePacket(self, mission_sequence)
        self.packet_channel.sendPacket(write_packet)

    ###########################################################################
    # Returns this glider's current staged mission sequence as retrieved from its
    # dockserver.  Will block the caller while waiting for the dockserver to respond
    # with this glider's current mission sequence or until a timeout occurs.
    #
    # Returns:
    #  A MissionSequence instance representing this glider currently running mission
    #    sequence.
    #
    # Exceptions:
    #  TimeoutError - thrown if dockserver does not repond within the timeout period (~30 sec).
    #  ProxyError - Thrown if the network connection to dockserver is not open.
    #
    def getCurrentMissionSequence(self):
        # Check for open connection
        if not self.packet_channel:
            raise ProxyError(self, 'Connection to Dock Server is not open.  Try calling openConnection()')
        # Clear the thread sync event that represents receiving a WriteCurrentSequencePacket for
        # a sent ReadCurrentSequencePacket
        self.is_write_current_sequence_event.clear()
        # Send a request to Dock Server for this glider's current staged mission sequence.
        read_current_sequence = packet.ReadCurrentSequencePacket(self)
        self.packet_channel.sendPacket(read_current_sequence)
        # Wait for the Dock Server to respond with a WriteCurrentSequencePacket
        self.is_write_current_sequence_event.wait(CONNECTION_RESPONSE_TIMEOUT)
        if not self.is_write_current_sequence_event.isSet():
            # If timed out waiting for Dock Server response packet, then raise an exception.
            raise TimeoutError(self, 'Timeout waiting to receive a WriteCurrentSequencePacket.')
        # return the just received mission sequence to the caller.
        return self.current_mission_sequence

    ###########################################################################
    # Stage the passed mission adjustment by sending it to the dockserver.  This
    # adjustment will be transfered to the glider upon its next surfacing and
    # executed.
    #
    # Arguments:
    #  mission_adjustment - (packet.MissionAdjustment) All the mission files (ma and gbf)
    #    associated with adjusting the currently running mission.
    #
    # Exceptions:
    #  ProxyError - thrown if the connection to the dockserver is not open.
    #
    def stageMissionAdjustment(self, mission_adjustment):
        # Check for open connection
        if not self.packet_channel:
            raise ProxyError(self, 'Connection to Dock Server is not open.  Try calling openConnection()')
        write_packet = packet.WriteMissionAdjustmentPacket(self, mission_adjustment)
        self.packet_channel.sendPacket(write_packet)

    ###########################################################################
    # Returns a list of this glider's sensor names that persists on
    # dockserver.  Retrieves this list from dockserver in real time.  Will block
    # the caller until dockserver responds with the list or a timeout occurs.
    #
    # Returns: A list [] of sensor names as strings
    #
    def getPersistentSensorNames(self):
        # Send a SurfaceSensorResultNames query to dockserver and wait for a response.
        self.getPersistentSensorResult(packet.SurfaceSensorQueryNames())
        if self.persistent_sensor_result:
            return self.persistent_sensor_result.getResult()
        else:
            return []

    ###########################################################################
    # Retrieve all persistent sensor data from dockserver that occurs between
    # the passed start_date and end_date for the sensor names in the passed list.
    # Will block the caller until dockserver responds with the sensor data values
    # or a timeout occurs.
    #
    # Arguments:
    #   sensor_names - ([]) List of sensor names whose values are queried.
    #   start_date - (datetime.datetime) Start date for returned data values in UTC
    #   end_date - (datetime.datetime) End date for returned data values in UTC
    #
    # Returns: A dictionary whose keys are sensor names as strings and values are
    #   a list of two lists - the first list is time stamps (milliseconds since unix epoch);
    #   the second is sensor data values at those time stamps.
    #   Example: { m_battery : [[0, 1, ...], [1.2, 3.4, ...]], ...}
    #
    def getPersistentSensorData(self, sensor_names = None, start_date = None, end_date = None):
        # Send a SurfaceSensorResultData query to dockserver and wait for a response.
        self.getPersistentSensorResult(packet.SurfaceSensorQueryData(sensor_names, start_date, end_date))
        if self.persistent_sensor_result:
            return self.persistent_sensor_result.getResult()
        else:
            return {}

    ###########################################################################
    # Send the passed glider sensor query to the dockserver and wait for a
    # response packet.
    #
    # Arguments:
    #   query - (SurfaceSensorQueryNames or SurfaceSensorQueryData) The query to
    #     send to the dockserver.
    #
    # Returns:  An instance of SurfaceSensorResultNames or SurfaceSensorResultData.
    #
    # Exceptions:
    #  ProxyError - Thrown if the network connection to dockserver is not open.
    #  TimeoutError - thrown if dockserver does not repond within the timeout period (~30 sec).
    #
    def getPersistentSensorResult(self, query):
        # Check for open connection
        if not self.packet_channel:
            raise ProxyError(self, 'Connection to Dock Server is not open.  Try calling openConnection()')
        # Clear the thread sync event that represents receiving a GliderSurfaceSensorResponse packet for
        # a sent GliderSurfaceSensorRequest packet.
        self.is_persistent_sensor_response_event.clear()
        # Send a request to Dock Server for this glider's current list of persisted sensor names.
        surface_sensor_request = packet.GliderSurfaceSensorRequest(self, query)
        self.packet_channel.sendPacket(surface_sensor_request)
        # Wait for the Dock Server to respond with a GliderSurfaceSensorResponse packet.
        self.is_persistent_sensor_response_event.wait(CONNECTION_RESPONSE_TIMEOUT)
        if not self.is_persistent_sensor_response_event.isSet():
            # If timed out waiting for Dock Server response packet, then raise an exception.
            raise TimeoutError(self, 'Timeout waiting to receive a GliderSurfaceSensorResponse packet.')
        # return the just received list of persistent sensor names to the caller.
        return self.persistent_sensor_result

    def writeToLink(self, glider_link_proxy, command):
        '''
        Purpose: Send a packet to the dockserver requesting that the passed command
          be sent to the passed glider link.

        Arguments:
          glider_link_proxy - (packet.GliderLinkProxy) The glider link to send the passed
            command to.
          command - (string) A gliderDOS or picoDOS command to send

        Exceptions:
          ProxyError - Throw if the passed glider_link_proxy is None or not docked.  Or,
            if the passed command does not have a string representation.
        '''
        if glider_link_proxy is None:
            raise ProxyError(self, 'Glider link proxy can not be None.')
        link_proxy = None
        if glider_link_proxy == self.getPrimaryLink():
            link_proxy = self.getPrimaryLink()
        if glider_link_proxy == self.getSecondaryLink():
            link_proxy = self.getSecondaryLink()
        if link_proxy is None:
            raise ProxyError(self, 'Glider link proxy not in use: ' + str(glider_link_proxy))
        if not link_proxy.isDocked():
            raise ProxyError(self, 'Can not write to a glider link that is not docked.')
        try:
            glider_command_request = packet.GliderCommandRequest(glider_link_proxy, str(command))
            self.packet_channel.sendPacket(glider_command_request)
        except BaseException as be:
            raise ProxyError(self, 'Command has no string representation: ' + str(be))

    def write(self, command):
        '''
        Purpose: Write the passed gliderDOS or picoDOS command to the glider using its
          primary glider link.

        Arguments:
          command = (string) The gliderDOS or picoDOS command to send to the glider.
        '''
        self.writeToLink(self.getPrimaryLink(), command)

    ###########################################################################
    # Just a test method.
    #
    def helloWorld(self):
        print ('Hello World from Glider')

    ###########################################################################
    # PRIVATE Interface
    ###########################################################################

    ###########################################################################
    # Blocks the caller until this glider can be initialized with its status
    # from the actual dockserver machine or a timeout occurs.
    #
    # Exceptions:
    #  TimeoutError - thrown if dockserver does not respond with a GliderStatusResponse
    #    packet with the timeout period (~ 30 sec).
    #
    def waitForInitialize(self):
        if self.is_initialized_event.isSet():
            return True
        self.is_initialized_event.wait(CONNECTION_RESPONSE_TIMEOUT)
        if not self.is_initialized_event.isSet():
            # If timed out waiting for GliderProxy initialization, then
            # raise an exception.
            raise TimeoutError(self, 'Timeout waiting for GliderProxy to initialize.')
        return self.is_initialized_event.isSet()

    ###########################################################################
    # Update this glider's status with the information passed in the
    # packet_received argument.  Fire several events related to the arrival
    # of new status information - this notifies subscribers of new information.
    #
    # Arguments:
    #  packet_received - (GliderStatusResponse) Packet from dockserver containing
    #    this glider's latest status information - i.e., current mission, position,
    #    etc...
    #
    def update(self, packet_received):
        # Update this proxy's information from the passed glider status packet
        self.primary_link_proxy = packet_received.getPrimaryGliderLink()
        self.secondary_link_proxy = packet_received.getSecondaryGliderLink()
        self.is_docked = packet_received.isDocked()
        self.last_abort = packet_received.getLastAbort()
        self.last_mission = packet_received.getLastMission()
        self.last_position = packet_received.getLastPosition()
        self.data_filenames = packet_received.getDataFilenames()
        self.surface_sensors = packet_received.getSensors()
        # Update subscribers with info from the newly received glider status packet.
        # print 'In GliderProxy.update for glider ' + self.getName() + ' is docked ' + str(self.is_docked)
        self.proxy_publisher.FireEvent(GliderProxy.ON_DOCK, self, self.is_docked)
        if self.last_abort:
            self.proxy_publisher.FireEvent(GliderProxy.ON_ABORT, self, self.last_abort)
        if self.last_mission:
            self.proxy_publisher.FireEvent(GliderProxy.ON_MISSION_STATUS, self, self.last_mission)
        if self.last_position:
            self.proxy_publisher.FireEvent(GliderProxy.ON_POSITION, self, self.last_position)
        if self.data_filenames:
            self.proxy_publisher.FireEvent(GliderProxy.ON_DATA, self, self.data_filenames)
        if self.surface_sensors:
            self.proxy_publisher.FireEvent(GliderProxy.ON_SENSORS, self, self.surface_sensors)

    ###########################################################################
    # Handles the packet arrived event from the packet channel connection to the
    # dockserver.
    #
    # Threading - packet.PacketChannel.receivePacketThread
    #
    # Arguments:
    #  packet_received - (packet.Packet) The latest packet received from the dockserver.
    #
    def onPacket(self, packet_received):
        #print 'In GliderProxy.onPacket for ' + self.getName() + '...'
        #print str(packet_received)

        # Handle packets
        if packet_received.getAction() == packet.Packet.GLIDER_STATUS:
            # If a glider status packet is received, then update this glider's status
            self.update(packet_received)
            # Signal waiting client threads that this proxy is now initialized
            self.is_initialized_event.set()
        if packet_received.getAction() == packet.Packet.WRITE_TRACK_TAG:
            #print 'Got WriteTrackPacket for glider... ' + self.getName()
            self.track = packet_received.getTrack()
            # Signal any waiting client thread (call to getTrack()) that the
            # glider's track has arrived.
            self.is_write_track_event.set()
        if packet_received.getAction() == packet.Packet.WRITE_CURRENT_SEQUENCE_TAG:
            #print 'Got WriteCurrentSequencePacket for glider... ' + self.getName()
            self.current_mission_sequence = packet_received.getMissionSequence()
            # Signal any waiting client thread (call to getCurrentMissionSequence()) that
            # glider's current mission sequence has arrived.
            self.is_write_current_sequence_event.set()
        if packet_received.getAction() == packet.Packet.SURFACE_SENSOR:
            # If a surface sensor response packet is received, then get the query result.
            self.persistent_sensor_result = packet_received.getResult()
            # Signal any waiting client thread (call to getPersistentSensorQuery()) that
            # dockserver's response has arrived.
            self.is_persistent_sensor_response_event.set()
            #print 'In GliderProxy.onPacket...just processed a surface sensor response packet'
        if packet_received.getAction() == packet.Packet.GLIDER_CONNECT:
            # If glider dialog is received, then signal any clients.
            #print 'In GliderProxy.onPacket: ' + str(packet_received.getGliderLink())
            #print packet_received.getGliderOutput()
            self.proxy_publisher.FireEvent(GliderProxy.ON_DIALOG, self, packet_received.getGliderLink()
                                                                      , packet_received.getGliderOutput())

    ###########################################################################
    # Handles the close event from the packet channel.  Notifies subscribers
    # that the network connection to dockserver has closed.
    #
    # Threading - packet.PacketChannel.peer_monitor_timer thread and
    # packet.PacketChannel.asyncore.loop thread
    #
    def onClose(self):
        #print 'In GliderProxy.onClose for ' + self.getName() + '...'
        self.proxy_publisher.FireEvent(GliderProxy.ON_CLOSE, self)

###############################################################################
# This class represents information about generic exceptions releated to dockserver
# and glider proxy functionality.
#
class ProxyError(Exception):
    '''
    Attributes:
      proxy - (DockProxy or GliderProxy) the source object of the exception.
      message - (string) a description of the exception.
      base_type - (string) exception trace information
      base_value - (string) exception trace information
    '''
    # Class constants

    ###########################################################################
    # Create a new exception.
    #
    # Arguments:
    #  proxy - (DockProxy or GliderProxy) The source of the exception.
    #  message - (string) A description of the exception error.
    #
    def __init__(self, proxy, message):
        self.proxy = proxy
        self.message = message
        self.base_type = str(sys.exc_info()[0])
        self.base_value = str( sys.exc_info()[1])

    ###########################################################################
    # Returns the proxy source object of this exception.
    #
    # Returns:
    #  A DockProxy or GliderProxy instance.
    #
    def getProxy(self):
        return self.proxy

    ###########################################################################
    # Returns a description of this exception as a string
    #
    # Returns:
    #  A description of this exception as a string.
    #
    def getMessage(self):
        return self.message

    ###########################################################################
    # Returns a string representation of this exception.
    #
    # Returns:
    #   string - description of this exception
    #
    def __str__(self):
        return 'Message: ' + self.message + ' Base Type: ' + self.base_type + ' Base Value: ' + self.base_value

###############################################################################
class TimeoutError(ProxyError):
    '''
    Attributes:
    '''

    ###########################################################################
    # Create a TimeoutError instance.
    #
    # Arguments:
    #  proxy - (DockProxy or GliderProxy) The source of the exception.
    #  message - (string) A description of the exception error.
    #
    def __init__(self, proxy, message):
        super(TimeoutError, self).__init__(proxy, message)

###############################################################################
class ConnectionError(ProxyError):
    '''
    Attributes:
    '''

    ###########################################################################
    # Create a ConnectError instance.
    #
    # Arguments:
    #  proxy - (DockProxy or GliderProxy) The source of the exception.
    #  message - (string) A description of the exception error.
    #
    def __init__(self, proxy, message):
        super(ConnectionError, self).__init__(proxy, message)
