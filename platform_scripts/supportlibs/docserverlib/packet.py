'''
Date        Author                Comment
-----------------------------------------
2011-05-11  rtrout@teledyne.com   Created
2011-06-01  rtrout@teledyne.com   Added date math to account for WriteTrackPacket's
                                  date format (the utc offset at the end '-03:20').
                                  Python 2.5 doesn't have the date format codes to
                                  parse this offset syntax directly.
2011-06-03  rtrout@teledyne.com   Fixed start and end dates in Read Track packet.
                                  These date / times should be UTC.
2011-09-16  rtrout@teledyne.com   Changed python method default argument values from []
                                  to None.
2011-09-28  rtrout@teledyne.com   Added mission files' content to xml packets for staging
                                  mission sequences and adjustments.
2011-11-11  rtrout@teledyne.com   Added glider event log packet classes.
2011-11-28  lcooney@teledyne.com  Added getStatustring to Abort, Mission and Position classes
2011-12-15  rtrout@teledyne.com   Added support for glider batch files in missions.
2012-01-30  rtrout@teledyne.com   Added dockserver requests for glider surface sensor names and
                                  data values (defined in config.srf on a glider).
2012-02-08  rtrout@teledyne.com   Ignore glider positions where the date format doesn't match
                                  expected format.
2012-02-09  rtrout@teledyne.com   Received stage adjustment log entries that have a mission adjustment
                                  with no ma files (but may or may not have a mafiles element)
                                  are treated as a cancel staged adjustment log entry.
2012-03-29  rtrout@teledyne.com   Added GliderConnectResponse and GliderCommandRequest packets for
                                  receiving glider dialog and sending glider commands.
2012-10-03  rtrout@teledyne.com   Added GliderChange and DockChangesPacket to support hosting dockserver
                                  administration scripts.
                                  Added DockConnectRequest and DockStatusPacket to support
                                  notification of dockserver change request results.
2012-10-04  rtrout@teledyne.com   Parameterized application information - client_id and client_version
                                  so dockserver could tell which client was making a request.
2012-10-08  rtrout@teledyne.com   Added restore glider command to GliderChange in support of hosting
                                  dockserver administration scripts.
2012-10-10  rtrout@teledyne.com   Refactored encoding server_host and user_id into PacketChannel
                                  and Packet classes.
2012-10-16  rtrout@teledyne.com   Added class HostChange in support of dockserver hosting administration.
2012-10-29  rtrout@teledyne.com   Added class HostChanges in support of dockserver hosting administration.
2012-11-14  rtrout@teledyne.com   Refactored host changes class hierarchy.
2012-11-27  rtrout@teledyne.com   Class DockSubscriptionChange in support of dataserver host administration scripts.
2014-07-31  rtrout@teledyne.com   Moved code from control center project to python-dockproxy project.
'''
import asyncore
import socket
import getpass
import datetime
import thread
import threading
import Queue
import xml.dom.minidom
import publisher
import sys
import os.path

# module constants
FALSE = 'false'
TRUE = 'true'

###############################################################################
# Convert the passed log entry xml element into its class representation
# based on its event attribute
#
# Arguments:
#  log_entry_element - (xml.dom.Element) The XML Element representing a log entry
#
# Returns:
#  EventLogEntry subclass - the class representation of the passed log entry
#
def eventLogEntryFactory(log_entry_element):
    event_type = log_entry_element.getAttribute(EventLogEntry.EVENT_ATTRIBUTE)
    if event_type == EventLogEntry.GLIDER_SURFACED:
        # If the event type is deployed mission entry, then return its class
        # representation.
        return GliderSurfacedEntry(log_entry_element)
    if event_type == EventLogEntry.ANNOTATION:
        # If the event type is user annotation, then return its class representation
        return AnnotationEntry(log_entry_element)
    if event_type == EventLogEntry.STAGED_MISSION:
        # If the event type is staged mission entry, then return its class representation
        return StagedMissionEntry(log_entry_element)
    if event_type == EventLogEntry.STAGED_ADJUSTMENT:
        #print 'In eventLogEntryFactory: processing Staged Adjustment entry'
        return StagedAdjustmentEntry(log_entry_element)
    if event_type == EventLogEntry.FILES_FROM_GLIDER or event_type == EventLogEntry.FILES_TO_GLIDER:
        return FilesTransferredEntry(log_entry_element)

    print 'TODO: packet.eventLogEntryFactory...'
    # No recognized event log entry
    return None

###############################################################################
# Convert the passed packet-as-a-string to the appropriate instance
# of class Packet and return the instance.
#
# Arguments:
#  packet_string - (string) String representation of an XML packet.
#
# Returns:
#  An instance of a Packet subclass that is the XML representation of the passed
#  string packet
#
def packetFactory(packet_string):
    packet_document = xml.dom.minidom.parseString(packet_string)
    packet_node_list = packet_document.getElementsByTagName(Packet.PACKET_TAG)
    if packet_node_list.length > 0:
        # If received packet is a dockserver packet, then process it
        #print 'In packetFactory...Dock Server packet received'
        packet_element = packet_node_list.item(0)
        packet_action = packet_element.getAttribute(Packet.ACTION_ATTRIBUTE)
        if packet_action == Packet.KEEP_ALIVE_PACKET:
            #print 'In packetFactory...KeepAlivePacket'
            return KeepAlivePacket(packet_document = packet_document)
        if packet_action == Packet.CHANNEL_CLOSE:
            #print 'In packetFactory...ChannelControlPacket'
            return ChannelControlPacket(packet_document = packet_document)
        if packet_action == Packet.DOCK_CONFIGURATION:
            #print 'In packetFactory...DockConfigurationResponse'
            return DockConfigurationResponse(packet_document = packet_document)
        if packet_action == Packet.GLIDER_STATUS:
            #print 'In packetFactory...GliderStatusResponse'
            return GliderStatusResponse(packet_document = packet_document)
        if packet_action == Packet.LOG_NOTIFICATION:
            #print 'In packetFactory...EventLogNotificationPacket'
            # If a log notification packet was received, then transform to class representation.
            return EventLogNotificationPacket(packet_document = packet_document)
        if packet_action == Packet.LOG_PAGE:
            # If a log page packet was received, then transform to class representation
            #print 'In packetFactory: log page packet received'
            return EventLogPagePacket(packet_document = packet_document)
        if packet_action == Packet.SURFACE_SENSOR:
            #print 'In packetFactory...GliderSurfaceSensorResponse'
            # If the response to a surface sensor query packet was received, then transform to class representation.
            return GliderSurfaceSensorResponse(packet_document = packet_document)
        if packet_action == Packet.GLIDER_CONNECT:
            #print 'In packetFactory...GliderConnectResponse'
            # If a glider connect packet was received, then transform to class representation.
            return GliderConnectResponse(packet_document = packet_document)
        if packet_action == Packet.DOCK_STATUS:
            #print 'In packetFactory...DockStatusPacket'
            # If a dock status packet was received, then transform to class representation.
            return DockStatusPacket(packet_document = packet_document)
        # Dock Server packet is unrecognized
        #print 'In packetFactory: Packet not recognized - ' + packet_string
        return None
    glmpc_node_list = packet_document.getElementsByTagName(Packet.GLMPC_TAG)
    if glmpc_node_list.length > 0:
        # If received packet is a glmpc packet, then process it.
        #print 'In packetFactory...GLMPC packet received'
        glmpc_element = glmpc_node_list.item(0)
        write_track_node_list = glmpc_element.getElementsByTagName(Packet.WRITE_TRACK_TAG)
        if write_track_node_list.length > 0:
            #print 'In packetFactory...WriteTrackPacket'
            return WriteTrackPacket(packet_document = packet_document)
        write_current_sequence_node_list = glmpc_element.getElementsByTagName(Packet.WRITE_CURRENT_SEQUENCE_TAG)
        if write_current_sequence_node_list.length > 0:
            #print 'In packetFactory...WriteCurrentSequencePacket'
            return WriteCurrentSequencePacket(packet_document = packet_document)
        # GLMPC packet is unrecognized
        return None
    # No recognized packet
    return None

###############################################################################
# Represents a query to dockserver for the names of a glider's surface
# sensors as defined in config.srf.
#
class SurfaceSensorQueryNames(object):
    '''
    Attributes:
    '''
    # Class Constants
    # XML representation
    SENSOR_NAMES = 'SENSOR_NAMES'

    ###########################################################################
    # Create a new query for the surface sensor names of a glider.
    #
    def __init__(self):
        pass

    ###########################################################################
    # Transform this query into its XML representation.
    #
    # Arguments:
    #   packet_document - (xml.dom.minidom.Document) The XML document of the
    #     packet.
    #
    # Returns:
    #  The encompassing XML element that represents this query
    #
    def toXml(self, packet_document):
        select_element = packet_document.createElement(Packet.SELECT_TAG)
        select_element.setAttribute(Packet.TYPE_ATTRIBUTE, self.SENSOR_NAMES)
        return select_element

###############################################################################
# Representat a query for dockserver surface sensor data values of a particular
# glider.  These sensors are persisted on the dockserver.
#
class SurfaceSensorQueryData(object):
    '''
    Attributes:
      sensor_names - ([]) List of sensor names to query.
      start_date - (datetime.datetime) Start date/time for retrieved sensor data
      end_date - (datetime.datetime) End date/time for retrieved sensor data.
    '''

    # Class Constants
    # XML representation
    SENSOR_DATA = 'SENSOR_DATA'
    START_DATE_ATTRIBUTE = 'startDate'
    END_DATE_ATTRIBUTE = 'endDate'
    # Date/time format for packet query
    SURFACE_SENSOR_DATETIME_FORMAT = '%Y%m%dT%H%M%S' # 20120131T013110

    ###########################################################################
    # Create a new query for data values associated with surface sensors.
    # (those defined on a glider in config.srf).
    #
    # Arguments:
    #   sensor_names - ([]) list of sensor names whose data is queried.
    #   start_date - (datetime.datetime) Start date/time of queried data in UTC
    #   end_date - (datetime.datetime) End date/time of queried data in UTC
    #
    def __init__(self, sensor_names = None, start_date = None, end_date = None):
        self.sensor_names = sensor_names
        if self.sensor_names == None:
            self.sensor_names = []
        self.start_date = start_date
        self.end_date = end_date

    ###########################################################################
    # Transform this query into its XML representation.
    #
    # Arguments:
    #   packet_document - (xml.dom.minidom.Document) The XML document of the
    #     packet.
    #
    # Returns:
    #  xml.dom.Element - The encompassing XML element that represents this query
    #
    def toXml(self, packet_document):
        # Encode the query's type, start and end dates.
        select_element = packet_document.createElement(Packet.SELECT_TAG)
        select_element.setAttribute(Packet.TYPE_ATTRIBUTE, self.SENSOR_DATA)
        if self.start_date:
            start_date_string = self.start_date.strftime(self.SURFACE_SENSOR_DATETIME_FORMAT)
            select_element.setAttribute(self.START_DATE_ATTRIBUTE, start_date_string)
        if self.end_date:
            end_date_string = self.end_date.strftime(self.SURFACE_SENSOR_DATETIME_FORMAT)
            select_element.setAttribute(self.END_DATE_ATTRIBUTE, end_date_string)
        # Encode sensor names if any
        for name in self.sensor_names:
            sensor_element = packet_document.createElement(Packet.SENSOR_TAG)
            sensor_element.setAttribute(Packet.NAME_ATTRIBUTE, name)
            select_element.appendChild(sensor_element)
        return select_element

###############################################################################
# Represents the result of a surface sensor names query.
#
class SurfaceSensorResultNames(object):
    '''
    Attributes:
      result - ([]) A list of sensor names from the dockserver.  These are the
        sensors whose values are persisted by the dockserver (they appear in
        a glider's config.srf file).
    '''
    ###########################################################################
    # Create an instance from the passed sensor history element.  Extract all
    # sensor names from the passed xml and add them to the 'result' list.
    #
    # Arguments:
    #  history_element - (xml.dom.Element) The encompassing XML element of
    #    a list of sensor names from the dockserver in response to a
    #    surface sensor names query.
    #
    '''
    Sample XML Representation:
    <surfaceSensorHistory type="SENSOR_NAMES">
      <sensor name="m_battery"/>
      <sensor name="m_vacuum"/>
      ...
    </surfaceSensorHistory>
    '''
    def __init__(self, history_element = None):
        self.result = []
        if history_element:
            # If a history element is passed, then decoded the sensor names.
            sensor_element_nodelist = history_element.getElementsByTagName(Packet.SENSOR_TAG)
            for sensor_element in sensor_element_nodelist:
                self.result.append(sensor_element.getAttribute(Packet.NAME_ATTRIBUTE))

    ###########################################################################
    # Returns the list of surface sensor names last received from dockserver
    # for a specific glider.
    #
    # Returns:
    #  A list of sensor names.
    #
    def getResult(self):
        return self.result

###############################################################################
# Represents the result of a surface sensor data query.
#
class SurfaceSensorResultData(object):
    '''
    Attributes:
      result - ({}) A dictionary representation of the query results.  Each
        dictionary item's key is a sensor name as string; each value is a list
        of two elements - first is a list of date/time values as Unix time and the second
        is a list of corresponding sensor data values.
    '''
    # Class Constants
    DT_ATTRIBUTE = 'dT'
    VAL_ATTRIBUTE = 'val'
    OID_ATTRIBUTE = 'OID'
    SEN_ATTRIBUTE = 'sen'

    ###########################################################################
    # Transform the passed XML representation of a data query result into its
    # class representation.
    #
    # Arguments:
    #   history_element - (xml.dom.Element) The surface sensor history element
    #     of the query result.
    #
    '''
    Sample XML Representation:
    <surfaceSensorHistory type="SENSOR_DATA">
      <surface OID="1234" dT="<unix time in milliseconds>" lat="4136.981" lon="-7032.128" mi="gy10v001.mi"/>
      <surface ...
      <sensor OID="5678" name="m_battery" unit="volts"/>
      <sensor ...
      <data surf="1234" sen="5678" dT="<unix time in milliseconds>" val="1.0" />
      <data ...
    </surfaceSensorHistory>
    '''
    def __init__(self, history_element = None):
        self.result = {}
        sensor_names = {}
        if history_element:
            # If a history element is passed, then decoded the sensor names.
            sensor_element_nodelist = history_element.getElementsByTagName(Packet.SENSOR_TAG)
            for sensor_element in sensor_element_nodelist:
                sensor_name = sensor_element.getAttribute(Packet.NAME_ATTRIBUTE)
                sensor_oid = sensor_element.getAttribute(self.OID_ATTRIBUTE)
                sensor_names[sensor_oid] = sensor_name
                self.result[sensor_name] = [[], []]
            # Decode the sensor values
            data_element_nodelist = history_element.getElementsByTagName(Packet.DATA_TAG)
            for data_element in data_element_nodelist:
                time = long(data_element.getAttribute(self.DT_ATTRIBUTE))
                value = float(data_element.getAttribute(self.VAL_ATTRIBUTE))
                sen = data_element.getAttribute(self.SEN_ATTRIBUTE)
                if sen in sensor_names:
                    # If this data's sensor is in the list, then record it.
                    self.result[sensor_names[sen]][0].append(time)
                    self.result[sensor_names[sen]][1].append(value)

    ###########################################################################
    # Return the result of the last surface sensor data query for a particular
    # gliders.
    #
    # Returns:
    #  A dictionary representation of the query results.  Each
    #  dictionary item's key is a sensor name as string; each value is a list
    #  of two elements - first is a list of date/time values as Unix time and the second
    #  is a list of corresponding sensor data values.
    #
    def getResult(self):
        return self.result

###############################################################################
# Handles a TCP/IP socket that connects a dockserver client to it's dockserver
# and the parsing of the IO stream into dockserver packets.  This class sends
# keepalive packets every 60 seconds to keep the connection open.
#
# Publish / Subscribe Interface - This class supports the following events.
#
# onPacket(packet) - subscribe to topic PacketChannel.ON_PACKET
#   Fired each time a packet is received that is NOT a keepalive packet.
#
# onClose() - subscribe to topic PacketChannel.ON_CLOSE
#   Fired when this channel is close due to error or peer closing connection.
#   Does NOT fire if closeChannel is called.
#
class PacketChannel(asyncore.dispatcher):
    '''
    Attributes:
      out_buffer - (string) Output buffer for packets sent to the dockserver (over the socket).
      in_buffer - (string) Input buffer for reading characters received from the dockserver (over the socket).
      send_queue - (Queue.Queue) queue of packets-as-strings to be sent to the dockserver.
        Used to communicate packets across threads involved in sending packets.
      receive_queue - (Queue.Queue) queue of packets-as-Packet-subclass received from dockserver.
        Used to communicate packets across threads involved in receiving packets.
      channel_publisher - (publisher.Publisher) Manages subscribers to PacketChannel events.
      isChannelOpen - (boolean) True if a TCP/IP network socket is open to the dockserver.
        Otherwise, False.
      destination_host - (string) Dockserver domain name or dotted IP address.
      destination_port - (int) Dockserver listening socket for client connection requests.
      dispatcher_map - (dict) contains this channel's socket.  Used by asyncore.loop
        for non-blocking socket I/O.
      keep_alive_timer - (threading.Timer) Countdown timer for sending keepalive packets to dockserver.
      peer_monitor_timer - (threading.Timer)  Countdown timer for receiving keepalive packets
        from a dockserver.  Timeout means the dockserver connection is no longer good.
      receive_packet_thread - (threading.Thread) Continually checks the receive_queue for packets
        to send to event subscribers.
      sender_id - (string) Encoding of user application's client_id and client_version used by
        dockserver as a client identification string.
      sender_version - (string) Encoding of user application's client_id and client_version used by
        dockserver to determine feature support.
      server_host - (string) For hosting dockserver use only.  The host dockserver identifier that
        this channel connects to.
      user_id - (string) The user name used for authentication on the hosting dockserver side of this channel.
        This is the logged in user.
    '''
    # Class Constants
    DEFAULT_SERVER_HOST = 'default'
    # Keepalive packet frequency in seconds.
    KEEPALIVE_INTERVAL = 60
    ASYNCORE_LOOP_INTERVAL = 5.0
    IN_BUFFER_SIZE = 8192
    END_OF_PACKET_TAGS = ['</packet>', '</glmpc>']
    # identifier for subscribing to a "packet arrived" event.
    # signature for method is onPacket(self, packet)
    ON_PACKET = 'onPacket'
    # identifier for subscribing to a socket close event.
    # signature for method is onClose(self)
    ON_CLOSE = 'onClose'

    # Packet Channel directives - channel control directives that are queued along
    # with packets to control this end of the channel.
    CLOSE_CHANNEL_DIRECTIVE = 'close'

    ###########################################################################
    # Class PacketChannel constructor.  Create and initialize (but not open) a
    # packet channel to the dockserver at the passed host and port IP address.
    #
    # Arguments:
    #  host - (string) Dockserver domainname or dotted IP address.
    #  port - (int) TCP/IP socket number that dockserver listens for client requests.
    #  client_id - (string) The application's client ID as known to dockserver.
    #    Used by dockserver to determine feature support.  Sent in every packet.
    #  client_version - (string) The application's client version as understood by
    #    dockserver.  Used by dockserver to determine feature support.  Sent in every
    #    packet.
    #
    def __init__(self, host, port, client_id, client_version):
        '''
        Constructor
        '''
        # Packet outbound to dockserver buffer
        self.out_buffer = ''
        self.send_queue = Queue.Queue(0)
        self.receive_queue = Queue.Queue(0)
        self.channel_publisher = publisher.Publisher()
        self.isChannelOpen = False
        self.destination_host = host
        self.destination_port = port
        self.sender_id = getpass.getuser() + ';' + socket.getfqdn() + \
            ';' + client_id + ';' + str(datetime.datetime.now())
        self.sender_version = client_id + ';' + client_version
        self.server_host = PacketChannel.DEFAULT_SERVER_HOST
        # Get the logged in user name
        self.user_id = getpass.getuser()
        self.keep_alive_timer = threading.Timer(self.KEEPALIVE_INTERVAL, self.sendKeepAlive)
        self.peer_monitor_timer = threading.Timer(self.KEEPALIVE_INTERVAL * 3, self.shutdownChannel)
        self.receivePacketThread = threading.Thread(target = self.receivedPacketMonitor)
        self.in_buffer = ''
        # Set up custom map for asyncore loop - don't want to mix with
        # other asyncore loop maps and want thread safety.  Just want this socket checked.
        # Note that each dockproxy will have its own socket and asyncore
        # loop map as each map runs in its own thread.
        self.dispatcher_map = {}
        asyncore.dispatcher.__init__(self, map=self.dispatcher_map)

    ###########################################################################
    # PUBLIC Interface
    ###########################################################################

    ###########################################################################
    # Opens a socket to the dockserver passed in the constructor.  Starts the
    # thread that handles receiving data over the socket.
    #
    # Threading - main "caller" thread.
    #
    def openChannel(self):
        if not self.isChannelOpen:
            self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
            # Connect to dockserver client port.
            self.connect( (self.destination_host, self.destination_port) )
            # Start a thread to effect async, non-blocking socket I/O
            thread.start_new_thread(asyncore.loop, (PacketChannel.ASYNCORE_LOOP_INTERVAL, \
                                                    False, self.dispatcher_map, None))
            self.isChannelOpen = True

    ###########################################################################
    # Sends the passed packet to the connected dockserver by placing it on the
    # send queue.  Caller is never blocked by this method.
    #
    # Arguments:
    #  packet - (Packet subclass) Class representation of the packet to send to
    #    the dockserver.
    #
    # Threading - main "caller" thread
    def sendPacket(self, packet):
        #print 'In sendPacket...'
        # Encode the sending application's client identification information
        packet.setSender(self.sender_id, self.sender_version)
        # Encode hosting dockserver information - ignored by non-hosting dockservers
        packet.setServerHost(self.server_host, self.user_id)
        # Queue the passed packet as a string for sending to the dockserver
        self.send_queue.put_nowait(str(packet))

    ###########################################################################
    # Closes the network socket to the dockserver.  First sends a close packet
    # request to notify dockserver of channel closing.
    #
    # Threading - main "caller" thread
    #
    def closeChannel(self):
        #print 'In closeChannel...'
        if self.isChannelOpen:
            # Close this channel nicely - i.e., send close packet to dockserver
            self.sendClosePacket()

    ###########################################################################
    # Add the passed subscriber (function) as a listener for the pass event topic.
    #
    # Arguments:
    #  topic - (string) Identifier for the event.
    #  subscriber - (function) function to call when the event identified by the
    #    passed topic is fired.
    #
    # Threading - main "caller" thread
    #
    def addSubscriber(self, topic, subscriber):
        # Add the passed subscriber as an event listener
        self.channel_publisher.AddSubscriber(topic, subscriber)

    ###########################################################################
    # Removes the passed subscriber (function) from those notified when the
    # event identified by the passed topic is fired.
    #
    # Arguments:
    #  topic - (string) Identifier for the event
    #  subscriber - (function) function to remove from the list of event listeners.
    #
    # Threading - main "caller" thread
    #
    def removeSubscriber(self, topic, subscriber):
        # Remove the passed subscriber from packet event notification.
        self.channel_publisher.RemoveSubscriber(topic, subscriber)

    ###########################################################################
    # Sets the hosting dockserver and the user to send with each packet to the
    # hosting dockserver.  Used for authentication.  Only applies to hosting
    # dockserver administration scripts.  This information ignored by non-hosting
    # dockservers.
    #
    def setServerHost(self, server_host):
        self.server_host = server_host

    ###########################################################################
    # Just a  test method.
    #
    def helloWorld(self):
        print 'Hello World from PacketChannel'

    ###########################################################################
    # PRIVATE Interface
    ###########################################################################

    ###########################################################################
    # Socket callback - called when socket has successfully connected to the
    # destination machine.
    #
    # Threading - asyncore.loop thread
    #
    def handle_connect(self):
        #print 'In handle_connect...starting all threads'
        self.isChannelOpen = True
        # As soon as a connection is open, start sending keepalive packets.
        self.keep_alive_timer.start()
        # Channel monitor timer - is peer still connected.
        self.peer_monitor_timer.start()
        # Start a thread to notify subscribers that packets have arrived
        #thread.start_new_thread(self.receivedPacketMonitor, ())
        self.receivePacketThread.start()

    ###########################################################################
    # Socket callback - called when the socket closes.
    #
    # Threading - asyncore.loop thread
    #
    def handle_close(self):
        #print 'In handle_close...'
        self.shutdownChannel()

    ###########################################################################
    # Socket callback - called when the socket has data available to read.
    # This method reads the data and checks for receiving a complete packet.
    # When a packet other than keepalive is received, it is enqueued on the
    # receive_queue.  This method does not block the caller.
    #
    # Threading - asyncore.loop thread
    #
    def handle_read(self):
        #print 'In handle_read...'
        self.in_buffer = self.in_buffer + self.recv(PacketChannel.IN_BUFFER_SIZE)
        packet_end = self.findPacketEnd()
        while packet_end > 0:
            #print 'In handle_read...found packet...'
            # If an end of packet is found, then make packet and check for keepalive
            packet_string = self.in_buffer[:packet_end]
            #print 'PacketChannel.handle_read...' + packet_string
            # Remove packet from input buffer
            self.in_buffer = self.in_buffer[packet_end:]
            #print 'In handle_read...in_buffer is: ' + self.in_buffer
            packet = packetFactory(packet_string)
            #print 'In PacketChannel.handle_read...back from packetFactory'
            if packet:
                # If a recognized packet is found, then process it
                if packet.isKeepAlive():
                    #print 'In handle_read...received keepalive packet...'
                    # If packet is a keepalive, then reset keepalive received countdown
                    self.peer_monitor_timer.cancel()
                    self.peer_monitor_timer = threading.Timer(self.KEEPALIVE_INTERVAL * 3, self.shutdownChannel)
                    self.peer_monitor_timer.start()
                elif packet.isCloseRequest():
                    if packet.getControl() == ChannelControlPacket.CLOSE_REQUEST:
                        # If a close request is received from dockserver, then send a close acknowledge
                        # and close this channel
                        #print 'In handle_read...received close request packet.'
                        self.sendPacket(ChannelControlPacket(ChannelControlPacket.CLOSE_ACKNOWLEDGE))
                        self.send_queue.put_nowait(PacketChannel.CLOSE_CHANNEL_DIRECTIVE)
                else:
                    # If not a keepalive packet, then add packet to receive queue
                    self.receive_queue.put_nowait(packet)
                    #print 'In handle_read...just queued received packet for subscribers. ' + str(self.receive_queue.qsize()) + str(packet)
            else:
                pass
                #print 'Received unrecognized packet: ' + packet_string
            # Check for another received packet
            packet_end = self.findPacketEnd()

    ###########################################################################
    # Socket callback - checks the output buffer for more characters to send.
    #
    # Returns:
    #  (boolean) True is more characters are available to send.  Otherwise, False.
    #
    # Threading - asyncore.loop thread
    #
    def writable(self):
        #print 'In writable...'
        if len(self.out_buffer) > 0:
            # If a dequeued packet has not been completely sent, then send more of it.
            return True
        else:
            # If the dequeued packet has been completely sent, then dequeue the next packet if one.
            try:
                self.out_buffer = self.send_queue.get_nowait()
                return True
            except Queue.Empty:
                return False

    ###########################################################################
    # Socket callback - called when the socket is ready to receive characters
    # to send to the peer socket.
    #
    # Threading - asyncore.loop thread
    #
    def handle_write(self):
        #print 'In handle_write...'
        if self.out_buffer == PacketChannel.CLOSE_CHANNEL_DIRECTIVE:
            # If the output buffer contains a close channel directive, then close it down.
            self.shutdownChannel()
        else:
            # Send the dequeued packet to the dockserver
            sent = self.send(self.out_buffer)
            #print self.out_buffer[:sent]
            # Remove the sent portion.
            self.out_buffer = self.out_buffer[sent:]

    ###########################################################################
    # Socket callback - called when an error is detected with the socket.
    # Outputs the error and closes the socket.
    #
    # Threading - asyncore.loop thread
    #
    def handle_error(self):
        #print 'In handle_error...'
        print sys.exc_info()[0]
        print sys.exc_info()[1]
        print sys.exc_info()[2]
        self.shutdownChannel()

    # Threading - asyncore.loop thread
    def handle_expt(self):
        #print 'In handle_expt...'
        pass

    ###########################################################################
    # Searches the input character buffer for end-of-packet markers.
    #
    # Returns:
    #  (int) The buffer position just after the first found end-of-packet tag.
    #  Returns -l if no end-of-packet tag found.
    #
    def findPacketEnd(self):
        end_tag_positions = []
        for end_tag in PacketChannel.END_OF_PACKET_TAGS:
            end_tag_position = self.in_buffer.find(end_tag)
            if end_tag_position > 0:
                end_tag_position = end_tag_position + len(end_tag)
            end_tag_positions.append(end_tag_position)

        found_tag_positions = [position for position in end_tag_positions if position > 0]
        if found_tag_positions:
            return min(found_tag_positions)
        else:
            return -1

    ###########################################################################
    # Sends a close request packet to the dockserver and then queues a close
    # channel directive in the send-queue.  The caller is not blocked.
    #
    def sendClosePacket(self):
        #print 'In sendClosePacket...'
        if self.isChannelOpen:
            # If this channel is open, then send a channel close packet.
            self.sendPacket(ChannelControlPacket(ChannelControlPacket.CLOSE_REQUEST))
            # Queue close channel directive
            self.send_queue.put_nowait(PacketChannel.CLOSE_CHANNEL_DIRECTIVE)

    ###########################################################################
    # Shutsdown the network connection to the dockserver.  Stops the thread that
    # sends keepalive packets and the peer thread for checking that the peer is
    # alive.  Blocks the calling thread until the receive packet thread has
    # terminated.
    #
    # Threading - peer_monitor_timer thread and asyncore.loop thread
    #
    def shutdownChannel(self):
        #print 'In shutdownChannel...'
        if self.isChannelOpen:
            # Shutdown this channel.
            self.keep_alive_timer.cancel()
            #print '... keep alive time canceled.'
            self.peer_monitor_timer.cancel()
            #print '... peer monitor timer canceled.'
            self.close()
            #print '... socket closed.'
            self.isChannelOpen = False
            self.receivePacketThread.join()
            #print '...receivePacket thread joined.'
            # Notify subscribers that channel has closed
            self.channel_publisher.FireEvent(PacketChannel.ON_CLOSE)
            #print '...onClose event fired.'

    ###########################################################################
    # Sends a keepalive packet to the dockserver each time the keepalive timer
    # times out.
    #
    # Threading - keep_alive_timer thread.
    #
    def sendKeepAlive(self):
        #print 'In sendKeepAlive...'
        if self.isChannelOpen:
            # If this channel is open, send a keepalive packet to the dockserver.
            self.sendPacket(KeepAlivePacket())
            # Schedule the next keepalive packet.
            self.keep_alive_timer = threading.Timer(self.KEEPALIVE_INTERVAL, self.sendKeepAlive)
            self.keep_alive_timer.start()

    ###########################################################################
    # While the network connection is open, this method continuously checks for
    # received packets and notifies subscribers when packets are received.  It
    # runs in its own thread.
    #
    # Threading - receive packet monitor thread
    #
    def receivedPacketMonitor(self):
        #print 'In receivedPacketMonitor...'
        while self.isChannelOpen:
            try:
                # Block until a packet arrives
                #print 'PacketChannel.receivedPacketMonitor...checking receive queue'
                packet = self.receive_queue.get(True, 5)
            except Queue.Empty:
                #print 'PacketChannel.receivedPacketMonitor...receive queue empty'
                packet = None
            if packet:
                # Notify subscribers that a packet has arrived.  Subscriber can use
                # this thread to process the packet if they wish.
                try:
                    #print 'PacketChannel:receivedPacketMonitor... ' + str(packet)
                    self.channel_publisher.FireEvent(PacketChannel.ON_PACKET, packet)
                except:
                    print 'PacketChannel...OnPacket event threw an exception:'
                    print sys.exc_info()[0]
                    print sys.exc_info()[1]
                    print sys.exc_info()[2]
                    print '... PacketChannel ignores packet and continues to process incoming packets.'
        #print '...Out receivePacketMonitor'

###############################################################################
# Encapsulates methods and variables that are common to all dockserver packets.
# All dockserver packets should inherit from this class.
#
class Packet(object):
    '''
    Attributes:
      packet_document - (xml.dom.minidom.Document) DOM tree representation of this packet
      packet_element - (xml.dom.Element) Top-level element of the packet XML Document
      sender_id - (string) This client's ID string expected by dockserver in received packets.
      sender_version - (string) Running version of the Control Center client.

    Sample XML Packet Header:
      <packet
        senderID="rtrout;magellan.webbresearch.com;WRC Mission Planner;Tue March 29, 2005"
        senderVersion="WRC Mission Planner;1.7"
      >
      </packet>
    '''
    # XML Attributes and tags
    PACKET_XML_TEMPLATE = '<?xml version="1.0" encoding="UTF-8"?><packet> </packet>'
    GLMPC_XML_TEMPLATE = '<?xml version="1.0" encoding="UTF-8"?><glmpc> </glmpc>'

    PACKET_TAG = 'packet'
    GLMPC_TAG = 'glmpc'
    GLIDER_TAG = 'glider'
    LAST_ABORT_TAG = 'lastAbort'
    LAST_MISSION_TAG = 'lastMission'
    POSITION_TAG = 'position'
    WRITE_TRACK_TAG = 'writeTrack'
    REPORT_TAG = 'report'
    LAT_TAG = 'lat'
    LON_TAG = 'lon'
    TIME_TAG = 'time'
    WRITE_MISSION_SEQUENCE_TAG = 'writeMissionSequence'
    MISSION_TAG = 'mission'
    MA_FILE_TAG = 'maFile'
    IDENTIFIER_TAG = 'identifier'
    WRITE_CURRENT_SEQUENCE_TAG = 'writeCurrentSequence'
    WRITE_MISSION_ADJUSTMENT_TAG = 'writeMissionAdjustment'
    DATA_FILE_TAG = 'dataFile'
    FILE_TAG = 'file'
    REQUEST_TAG = 'request'
    SENSORS_TAG = 'sensors'
    SELECT_TAG = 'select'
    SENSOR_TAG = 'sensor'
    SUFRACE_SENSOR_HISTORY_TAG = 'surfaceSensorHistory'
    DATA_TAG = 'data'
    GLIDER_LINK_TAG = 'gliderLink'
    TEXT_TAG = 'text'

    TYPE_ATTRIBUTE = 'type'
    ACTION_ATTRIBUTE = 'action'
    SENDER_ID_ATTRIBUTE = 'senderID'
    SENDER_VERSION_ATTRIBUTE = 'senderVersion'
    CONTROL_ATTRIBUTE = 'control'
    NAME_ATTRIBUTE = 'name'
    RESET_ATTRIBUTE = 'reset'
    CAUSE_ATTRIBUTE = 'cause'
    DATE_TIME_ATTRIBUTE = 'dateTime'
    SEGMENT_ATTRIBUTE = 'segment'
    MISSION_ATTRIBUTE = 'mission'
    MISSION_NAME_ATTRIBUTE = 'missionName'
    MISSION_NUMBER_ATTRIBUTE = 'missionNumber'
    BECAUSE_ATTRIBUTE = 'because'
    DATE_TIME_ATTRIBUTE = 'dateTime'
    MISSION_TIME_ATTRIBUTE = 'missionTime'
    STATUS_ATTRIBUTE = 'status'
    LATITUDE_ATTRIBUTE = 'latitude'
    LONGITUDE_ATTRIBUTE = 'longitude'
    GLIDER_NAME_ATTRIBUTE = 'gliderName'
    ID_ATTRIBUTE = 'id'
    FILENAME_ATTRIBUTE = 'filename'
    REPETITIONS_ATTRIBUTE = 'repetitions'
    GLIDERS_HOME_ATTRIBUTE = 'glidersHome'
    IS_RUNNING_ATTRIBUTE = 'isRunning'
    PAGE_ATTRIBUTE = 'page'
    GBF_ATTRIBUTE = 'gbf'
    COMMAND_ATTRIBUTE = 'command'
    HOST_ATTRIBUTE = 'host'
    USER_ID_ATTRIBUTE = 'userID'

    PACKET = 'packet'
    REQUEST = 'request'
    RESPONSE = 'response'
    KEEP_ALIVE_PACKET = 'keepalivePacket'
    CHANNEL_CLOSE = 'channelClose'
    DOCK_CONFIGURATION = 'dockConfiguration'
    GLIDER_STATUS = 'gliderStatus'
    GLIDER_CONNECT = 'gliderConnect'
    LOG_REQUEST = 'logRequest'
    LOG_NOTIFICATION = 'logNotification'
    LOG_PAGE = 'logPage'
    SURFACE_SENSOR = 'surfaceSensor'
    GLIDER_COMMAND = 'gliderCommand'
    DOCK_CHANGES = 'dockChanges'
    DOCK_CONNECT = 'dockConnect'
    DOCK_STATUS = 'dockStatus'

    ###########################################################################
    # Create a base packet instance and initialize items common to all packets.
    # All packet subclasses must handle two 'constructor' cases:
    # (1) Creating a packet to send to a dockserver.
    # (2) Creating a packet to encapsulate a packet string received from dockserver
    # The argument, packet_document, determines which case is performed.
    #
    # Arguments:
    #  packet_template - (string) Determine the 'style' of packet to create - '<packet>' or '<glmpc>'
    #  packet_document - (xml.dom.minidom.Document) DOM tree representation of packet to decode.
    #    If this parameter is None, then build a packet for sending.  Used by clients.
    #    Otherwise, decode the passed packet_document into its class representation.  Used to
    #    receive packets from dock server.
    #
    def __init__(self, packet_template, packet_document):
        '''
        Constructor
        '''
        if not packet_document:
            # If sending a packet, create the general packet structure.
            self.packet_document = xml.dom.minidom.parseString(packet_template)
            self.packet_element = self.packet_document.documentElement
        else:
            # If receiving a packet, extract and initialize common packet elements.
            self.packet_document = packet_document
            self.packet_element = packet_document.documentElement
            self.sender_id = self.packet_element.getAttribute(Packet.SENDER_ID_ATTRIBUTE)
            self.sender_version = self.packet_element.getAttribute(Packet.SENDER_VERSION_ATTRIBUTE)

    ###########################################################################
    # Returns a string representing the content of the xml text nodes in the
    # passed list.
    #
    # Arguments:
    #  node_list - (list) List of XML elements that may contain text.
    #
    # Returns:
    #  (string) The concatination of the content of the text nodes passed in the list.
    #
    def getText(self, node_list):
        test_list = []
        for node in node_list:
            if node.nodeType == node.TEXT_NODE:
                test_list.append(node.data)
        return ''.join(test_list)

    ###########################################################################
    # Returns a formated string representation of this XML DOM.
    #
    # Returns:
    #  (string) A string presentation of this packet
    #
    def __str__(self):
        '''
        Returns formatted xml string
        '''
        return self.packet_document.toxml('utf-8')

    ###########################################################################
    # Returns True (boolean) if this packet is a keepalive packet.  Oterwise
    # returns False.
    #
    # Returns:
    #  (boolean)
    #
    def isKeepAlive(self):
        # If this packet is a keepalive packet, then return true
        return self.action == Packet.KEEP_ALIVE_PACKET

    ###########################################################################
    # Returns True (boolean) if this packet is a close channel packet.
    # Otherwise, returns False.
    #
    # Returns:
    #  (boolean)
    #
    def isCloseRequest(self):
        return self.action == Packet.CHANNEL_CLOSE

    ###########################################################################
    # Returns the value of the XML 'action' attribute.  This value helps the
    # packet receiver determine the packet type.
    #
    # Returns:
    #  (string) A string representing this packet's action.
    #
    def getAction(self):
        return self.action

    ###########################################################################
    # Encode the passed sender information in the packet.
    #
    # Arguments:
    #  sender_id - (string)
    #  sender_version - (string)
    #
    def setSender(self, sender_id, sender_version):
        self.packet_element.setAttribute(Packet.SENDER_ID_ATTRIBUTE, sender_id)
        self.packet_element.setAttribute(Packet.SENDER_VERSION_ATTRIBUTE, sender_version)

    ###########################################################################
    # Encode the passed hosting dockserver information in the packet.
    #
    # Arguments:
    #  server_host - (string)
    #  user_id - (string)
    #
    def setServerHost(self, server_host, user_id):
        self.packet_element.setAttribute(Packet.HOST_ATTRIBUTE, server_host)
        self.packet_element.setAttribute(Packet.USER_ID_ATTRIBUTE, user_id)

###############################################################################
# Represents keepalive packet sent between the control center and dockserver.
# These packets attempt to keep a network connection from being shutdown by an
# intermediate network element (e.g., router) due to inactivity.
#
# These packets are sent every 60 seconds.
#
class KeepAlivePacket(Packet):
    '''
    Attributes:
      type - (string) The general 'class' or type of packet.
      action - (string) The specific packet within a 'class' or type.
    '''
    # Class Constants

    ###########################################################################
    # Creates a keepalive packet.
    #
    # Arguments:
    #  packet_document - (xml.dom.Document) If not None, then decode this passed
    #    document element of the packet just received.  If None, then create a
    #    new document that represents a packet being prepared for sending.
    #
    def __init__(self, packet_document = None):
        '''
        Constructor
        '''
        #print 'In KeepAlivePacket constructor...'
        super(KeepAlivePacket, self).__init__(Packet.PACKET_XML_TEMPLATE, packet_document)

        self.type = Packet.PACKET
        self.action = Packet.KEEP_ALIVE_PACKET
        if not packet_document:
            # If encoding a keepalive packet for sending, do it
            # Add keep alive packet specific attributes and elements.
            # Setup packet type and action attributes
            self.packet_element.setAttribute(Packet.TYPE_ATTRIBUTE, self.type)
            self.packet_element.setAttribute(Packet.ACTION_ATTRIBUTE, self.action)
        # print self.packet_document.toxml('utf-8')

###############################################################################
# These packets control the connection to a dockserver by requesting and
# acknowledging that the connection be closed.
#
class ChannelControlPacket(Packet):
    '''
    Attributes:
      type - (string) The general 'class' or type of packet.
      action - (string) The specific packet within a 'class' or type.
      control - (string) Specifies packet close request or packet close acknownledge.
    '''
    # Class Constants
    CLOSE_REQUEST = '1'
    CLOSE_ACKNOWLEDGE = '2'

    ###########################################################################
    # Create a channel control packet for from the passed XML document or
    # the passed control operation (request close or acknowledge close request)
    #
    # Arguments:
    #  control - (string) Specifies the kind of control packet to create -
    #    (a) close request, or (b) close request acknowledge.
    #  packet_document - (xml.dom.Document) If not None, then decode this passed
    #    document element of the packet just received.  If None, then create a
    #    new document that represents a packet being prepared for sending.
    #
    def __init__(self, control = CLOSE_REQUEST, packet_document = None):
        '''
        Constructor
        '''
        #print 'In ChannelControlPacket constructor...'
        super(ChannelControlPacket, self).__init__(Packet.PACKET_XML_TEMPLATE, packet_document)

        self.type = Packet.PACKET
        self.action = Packet.CHANNEL_CLOSE
        if not packet_document:
            # Add channel control packet specific attributes and elements.
            self.control = control
            self.packet_element.setAttribute(Packet.CONTROL_ATTRIBUTE, control)
            # Setup packet type and action attributes
            self.packet_element.setAttribute(Packet.TYPE_ATTRIBUTE, self.type)
            self.packet_element.setAttribute(Packet.ACTION_ATTRIBUTE, self.action)
        else:
            # If decoding a packet, then extract attributes
            self.type = self.packet_element.getAttribute(Packet.TYPE_ATTRIBUTE)
            self.action = self.packet_element.getAttribute(Packet.ACTION_ATTRIBUTE)
            self.control = self.packet_element.getAttribute(Packet.CONTROL_ATTRIBUTE)
        # print self.packet_document.toxml('utf-8')

    ###########################################################################
    # Returns the control type of this packet.
    #
    # Returns:
    #  string - this packet's control type.
    #
    def getControl(self):
        return self.control

###############################################################################
# This packet is sent to dockservers to request their configuration.  That is,
# what gliders and serial ports do they manage.
#
class DockConfigurationRequest(Packet):
    '''
    Attributes:
      type - (string) The general 'class' or type of packet.
      action - (string) The specific packet within a 'class' or type.
    '''
    # Class Constants

    ###########################################################################
    # Creates a packet to request a dockserver's configuration.
    #
    # Arguments:
    #  packet_document - (xml.dom.Document) Ignored for now.  Control Center
    #    never receives dockserver configuration request packets.
    #
    def __init__(self, packet_document = None):
        '''
        Constructor
        '''
        super(DockConfigurationRequest, self).__init__(Packet.PACKET_XML_TEMPLATE, packet_document)
        # Setup packet type and action attributes
        self.type = Packet.REQUEST
        self.action = Packet.DOCK_CONFIGURATION
        self.packet_element.setAttribute(Packet.TYPE_ATTRIBUTE, self.type)
        self.packet_element.setAttribute(Packet.ACTION_ATTRIBUTE, self.action)

        # print self.packet_document.toxml('utf-8')

###############################################################################
# This packet is received from a dockserver and details its configuration -
# what gliders and serial ports it manages.
#
class DockConfigurationResponse(Packet):
    '''
    Attributes:
      type - (string) The general 'class' or type of packet.
      action - (string) The specific packet within a 'class' or type.
      glider_names - (Set) Set of strings representing glider names.
      gliders_home - (String) The dockserver's gliders home directory's full path.
    '''
    # Class Constants

    ###########################################################################
    # Transforms the passed XML Document into a class representation of a
    # dockserver's configuration.
    #
    # Arguments:
    #  packet_dockument - (xml.dom.Document) An XML representation of a dock
    #    configuration packet received from a dockserver.
    #
    def __init__(self, packet_document = None):
        '''
        Constructor
        '''
        super(DockConfigurationResponse, self).__init__(Packet.PACKET_XML_TEMPLATE, packet_document)
        # print self.packet_document.toxml('utf-8')
        self.type = Packet.RESPONSE
        self.action = Packet.DOCK_CONFIGURATION
        self.glider_names = set()
        self.gliders_home = None
        if packet_document:
            #print 'In DockConfigurationResponse constructor...'
            #print packet_document.toxml()
            # If a packet document was passed, then decode received packet.
            self.decode()
        else:
            # Setup packet type and action attributes
            self.packet_element.setAttribute(Packet.TYPE_ATTRIBUTE, self.type)
            self.packet_element.setAttribute(Packet.ACTION_ATTRIBUTE, self.action)

    ###########################################################################
    # Return a Set of strings representing glider names
    #
    # Returns:
    #  Set - s set of strings corresponding to the names of gliders managed by
    #    the dockserver that sent this packet.
    #
    def getGliderNames(self):
        return self.glider_names

    ###########################################################################
    # Return the dockserver's gliders home directory.
    #
    # Returns:
    #  string - Path to the glider home directory on the dockserver that sent
    #    this packet.
    #
    def getGlidersHome(self):
        return self.gliders_home

    ###########################################################################
    # PRIVATE Interface
    ###########################################################################

    ###########################################################################
    # Decode this packet's XML representation into its class representation.
    #
    def decode(self):
        # Get the docksever's gliders home directory
        if self.packet_element.hasAttribute(Packet.GLIDERS_HOME_ATTRIBUTE):
            self.gliders_home = self.packet_element.getAttribute(Packet.GLIDERS_HOME_ATTRIBUTE)
        # Create a set of glider names managed by the dockserver.
        glider_nodes = self.packet_element.getElementsByTagName(Packet.GLIDER_TAG)
        for glider_node in glider_nodes:
            #print glider_node.toxml()
            self.glider_names.add(glider_node.getAttribute(Packet.NAME_ATTRIBUTE))

###############################################################################
# Packet to dockserver for requesting a list of surface sensors for a specific
# glider (configured in config.srf).
#
class GliderSurfaceSensorRequest(Packet):
    '''
    Attributes:
      type - (string) The general 'class' or type of packet.
      action - (string) The specific packet within a 'class' or type.
    '''

    # Class Constants
    # XML attribute values

    ###########################################################################
    # Create a surface sensor request packet from the passed glider with the
    # passed query.
    #
    # Arguments:
    #   glider_proxy - (GliderProxy) The passed query refers to this glider's surface
    #     sensors.
    #   query - (SurfaceSensorQuery subclass) - Details of the query - what exactly
    #     is requested about the surface sensors - names or data.
    #   packet_document -(xml.dom.Document) If receiving and decoding a packet from
    #     dockserver, this represents the packet's XML document tree.  If sending a
    #     packet, this argument is None; it is initialized to the template packet document.
    #
    def __init__(self, glider_proxy, query, packet_document = None):
        super(GliderSurfaceSensorRequest, self).__init__(Packet.PACKET_XML_TEMPLATE, packet_document)

        # Initialize the packet specifics.
        self.type = Packet.REQUEST
        self.action = Packet.SURFACE_SENSOR
        self.packet_element.setAttribute(Packet.TYPE_ATTRIBUTE, self.type)
        self.packet_element.setAttribute(Packet.ACTION_ATTRIBUTE, self.action)
        self.packet_element.setAttribute(Packet.GLIDER_NAME_ATTRIBUTE, glider_proxy.getName())
        # Transform the passed query to XML and add to the document
        self.packet_element.appendChild(query.toXml(self.packet_document))

###############################################################################
# Dockserver's response to a request for sensor names or data (recorded from glider surface
# dialog - config.srf).  A response may contain only glider sensor names or data.
#
class GliderSurfaceSensorResponse(Packet):
    '''
    Attributes:
      type - (string) The general 'class' or type of packet.
      action - (string) The specific packet within a 'class' or type.
      result - (SurfaceSensorResultNames or SurfaceSensorResultData)
    '''

    ###########################################################################
    # Transform the passed XML Document representing a packet into its class
    # representation.
    #
    # Arguments:
    #  packet_document - (xml.dom.Document) XML Document representing the received
    #    surface sensor response packet.
    #
    def __init__(self, packet_document = None):
        super(GliderSurfaceSensorResponse, self).__init__(Packet.PACKET_XML_TEMPLATE, packet_document)

        self.type = Packet.RESPONSE
        self.action = Packet.SURFACE_SENSOR
        self.result = None
        if packet_document:
            # If a packet document was passed, then decode it.
            self.decode()

    ###########################################################################
    # Transform this packet's XML representation into its class representation.
    #
    def decode(self):
        surface_sensor_history_element = self.packet_element.getElementsByTagName(Packet.SUFRACE_SENSOR_HISTORY_TAG).item(0)
        history_type = surface_sensor_history_element.getAttribute(Packet.TYPE_ATTRIBUTE)
        if history_type == SurfaceSensorQueryNames.SENSOR_NAMES:
            self.result = SurfaceSensorResultNames(surface_sensor_history_element)
        if history_type == SurfaceSensorQueryData.SENSOR_DATA:
            self.result = SurfaceSensorResultData(surface_sensor_history_element)

    ###########################################################################
    # Returns the requested surface sensor results - sensor names or sensor data.
    #
    # Returns:
    #  SurfaceSensorResultNames or SurfaceSensorResultData - the results contained
    #    in this surface sensor response packet.
    #
    def getResult(self):
        return self.result

###############################################################################
# Packet to request the status of a particular glider from the dockserver.
#
class GliderStatusRequest(Packet):
    '''
    Attributes:
      type - (string) The general 'class' or type of packet.
      action - (string) The specific packet within a 'class' or type.
    '''
    # Class Constants

    ###########################################################################
    # Create a packet to send to the dockserver that requests the status of
    # the passed glider.
    #
    # Arguments:
    #  glider_proxy - (GliderProxy) Request this glider's status.
    #  packet_document - (xml.dom.Document) Not used for now.  Always None.
    #
    def __init__(self, glider_proxy, packet_document = None):
        '''
        Constructor
        '''
        super(GliderStatusRequest, self).__init__(Packet.PACKET_XML_TEMPLATE, packet_document)
        # Set the glider's name for status request.
        # Setup packet type and action attributes
        self.type = Packet.REQUEST
        self.action = Packet.GLIDER_STATUS
        self.packet_element.setAttribute(Packet.TYPE_ATTRIBUTE, self.type)
        self.packet_element.setAttribute(Packet.ACTION_ATTRIBUTE, self.action)
        self.packet_element.setAttribute(Packet.GLIDER_NAME_ATTRIBUTE, glider_proxy.getName())
        # print self.packet_document.toxml('utf-8')

###############################################################################
# Packet received by dockserver that contains the status of a particular glider.
# Status includes the last known abort, mission, GPS position, and data filenames.
#
class GliderStatusResponse(Packet):
    '''
    Attributes:
      type - (string) The general 'class' or type of packet.
      action - (string) The specific packet within a 'class' or type.
      is_docked - (bool) True if glider is in communication with the actual dockserver.
        Otherwise, False.
      last_abort - (packet.GliderAbort) Last known glider abort.
      last_mission - (packet.GliderMissionStatus) Last known running glider mission information.
      last_position - (packet.GliderPosition) Last known GPS position of glider.
      data_filenames - (list of string) List of data filenames most recently received by the
        dockserver from the glider.
      sensors - (packet.GliderSurfaceSensors) Glider sensor values from last known surfacing
        (config.srf).
    '''
    # Class Constants
    DOCKED = '1'

    ###########################################################################
    # Transforms the passed XML Document representing a glider's status into
    # its class representation.
    #
    # Arguments:
    #  packet_document - (xml.dom.Document) The XML representation of a glider
    #    status packet.
    #
    def __init__(self, packet_document = None):
        '''
        Constructor
        '''
        super(GliderStatusResponse, self).__init__(Packet.PACKET_XML_TEMPLATE, packet_document)

        # Setup packet type and action attributes
        self.type = Packet.RESPONSE
        self.action = Packet.GLIDER_STATUS
        self.data_filenames = []
        self.sensors = None
        self.primary_glider_link = None
        self.secondary_glider_link = None
        self.is_docked = False
        # print self.packet_document.toxml('utf-8')
        if packet_document:
            #print 'In GliderStatusResponse constructor...'
            #print packet_document.toprettyxml()
            # If a packet document was passed, then decode received packet.
            self.decode()
        else:
            self.packet_element.setAttribute(Packet.TYPE_ATTRIBUTE, self.type)
            self.packet_element.setAttribute(Packet.ACTION_ATTRIBUTE, self.action)

    ###########################################################################
    # Returns a bool indicating whether or not a glider is in communication with
    # dockserver.
    #
    # Returns:
    #  bool - True if this packet's glider is in comunication with dockserver.
    #    Otherwise, False.
    #
    def isDocked(self):
        return self.is_docked

    ###########################################################################
    # Returns the glider abort contained in this packet, if one.
    #
    # Returns:
    #  GliderAbort - This packet's last known glider abort.
    #
    def getLastAbort(self):
        return self.last_abort

    ###########################################################################
    # Returns the last known running glider mission, if one.
    #
    # Returns:
    #  GliderMissionStatus - This packet's last known running mission.
    #
    def getLastMission(self):
        return self.last_mission

    ###########################################################################
    # Returns the last known GPS position of this packet's glider, if one.
    #
    # Returns:
    #  GliderPosition - Last known GPS position of this packet's glider.
    #
    def getLastPosition(self):
        return self.last_position

    ###########################################################################
    # Returns a list of glider data filesnames just transferred from the glider
    # to dockserver.
    #
    # Returns:
    #  list of string - Glider data filenames just transferred from the glider.
    #
    def getDataFilenames(self):
        return self.data_filenames

    ###########################################################################
    # Returns the glider sensors just displayed in the last glider surface dialog.
    #
    # Returns:
    #  GliderSurfaceSensors - The sensor names, values, and units scanned from the
    #    latest glider surface dialog (config.srf).
    #
    def getSensors(self):
        return self.sensors

    def getPrimaryGliderLink(self):
        '''
        Purpose: Return the primary link proxy contained in this packet.

        Returns:
          (GliderLinkProxy) - The primary glider link proxy in this packet
        '''
        return self.primary_glider_link

    def getSecondaryGliderLink(self):
        '''
        Purpose: Return the secondary link proxy contained in this packet

        Returns:
          (gliderLinkPorxy) - The secondary glider link proxy in this packet
        '''
        return self.secondary_glider_link

    ###########################################################################
    # PRIVATE Interface
    ###########################################################################

    ###########################################################################
    # Transform this packet's content from its XML representation to its class
    # representation.
    #
    def decode(self):
        # Set docked status
        status = self.packet_element.getAttribute(Packet.STATUS_ATTRIBUTE)
        self.is_docked = (status == self.DOCKED)
        # Decode last abort
        last_abort_element = self.packet_element.getElementsByTagName(Packet.LAST_ABORT_TAG).item(0)
        if last_abort_element:
            # If an abort appears in the packet, then transform it to its class representation
            self.last_abort = GliderAbort(last_abort_element)
        else:
            # if no abort, set to none
            self.last_abort = None
        # Decode last mission status
        last_mission_element = self.packet_element.getElementsByTagName(Packet.LAST_MISSION_TAG).item(0)
        if last_mission_element:
            # If a last mission element appears in the packet, then transform it to its class representation
            missionName = last_mission_element.getAttribute(Packet.MISSION_NAME_ATTRIBUTE)
            if missionName != 'unavailable':
                # If the last mission is known, then transform it to its class representation.
                self.last_mission = GliderMissionStatus(last_mission_element)
            else:
                self.last_mission = None
        else:
            self.last_mission = None

        # Decode glider position.
        position_element = self.packet_element.getElementsByTagName(Packet.POSITION_TAG).item(0)
        if position_element:
            # If a position element appears in the packet, then check for lat and lon
            if position_element.hasAttributes():
                self.last_position = GliderPosition(position_element)
            else:
                self.last_position = None
        else:
            self.last_position = None

        # Decode glider's data filenames if any
        data_file_list_node = self.packet_element.getElementsByTagName(Packet.DATA_FILE_TAG)
        for data_file_element in data_file_list_node:
            self.data_filenames.append(data_file_element.getAttribute(Packet.NAME_ATTRIBUTE))

        # Decode sensor information.
        sensors_element = self.packet_element.getElementsByTagName(Packet.SENSORS_TAG).item(0)
        if sensors_element:
            self.sensors = GliderSurfaceSensors(sensors_element)
        else:
            self.sensors = None

        # Decode any glider links
        glider_link_node_list = self.packet_element.getElementsByTagName(Packet.GLIDER_LINK_TAG)
        if len(glider_link_node_list) >= 1:
            self.primary_glider_link = GliderLinkProxy(glider_link_node_list.item(0))
            #print 'In GliderStatusResponse.decode: primary link is ' + str(self.primary_glider_link)
        if len(glider_link_node_list) >= 2:
            self.secondary_glider_link = GliderLinkProxy(glider_link_node_list.item(1))
            # print 'In GliderStatusResponse.decode: secondary link is ' + str(self.secondary_glider_link)
            if self.primary_glider_link < self.secondary_glider_link:
                temp_link = self.secondary_glider_link
                self.secondary_glider_link = self.primary_glider_link
                self.primary_glider_link = temp_link

###############################################################################
# Dock Server clients send this packet to request a glider's position history
# (or track) between given dates.
#
class ReadTrackPacket(object):
    '''
    Attributes:
      document_string - (string) Read track packet XML as a string.
      packet_document - (xml.dom.minidom.Document) DOM tree representation of the Read Track packet.
      glider_element - (xml.dom.Element) The glider xml element of the Read Track packet.
    '''
    # Class Constants
    READ_TRACK_HEADER_XML_CLAUSE = '<?xml version="1.0" encoding="UTF-8"?><glmpc><readTrack><identifier><glider type="WRC"/></identifier>'
    READ_TRACK_FOOTER_XML_CLAUSE = '</readTrack></glmpc>'
    UTC_ZERO_OFFSET = '+00:00'

    ###########################################################################
    # Create a packet request to dockserver for the passed glider's position
    # track.
    #
    # Arguments:
    #  glider_proxy - (GliderProxy) The glider whose track is requested.
    #  start_date - (datetime) start time of track in UTC (or a local time with non-zero utcoffset).
    #  end_date - (datetime) end time of track in UTC (or a local time with non-zero utcoffset).
    #
    def __init__(self, glider_proxy, start_date=None, end_date=None):
        '''
        Constructor
        '''
        # Build a string representation of the Read Track packet XML.
        self.document_string = ReadTrackPacket.READ_TRACK_HEADER_XML_CLAUSE
        if start_date:
            # If a start date is provided, encode it.
            try:
                start_date_string = start_date.isoformat()
                if len(start_date_string) <= len('YYYY-MM-DDTHH:MM:SS'):
                    # If start_date.utcoffset() returns None, then append '+00:00'
                    # Dockserver expects an explicit UTC offset.
                    start_date_string = start_date_string + ReadTrackPacket.UTC_ZERO_OFFSET
                self.document_string = self.document_string + '<from>' + start_date_string + '</from>'
            except:
                start_date = None

        if end_date:
            # If an end date is provided, encode it.
            try:
                end_date_string = end_date.isoformat()
                if len(end_date_string) <= len('YYYY-MM-DDTHH:MM:SS'):
                    # If start_date.utcoffset() returns None, then append '+00:00'
                    # Dockserver expects an explicit UTC offset.
                    end_date_string = end_date_string + ReadTrackPacket.UTC_ZERO_OFFSET
                self.document_string = self.document_string + '<to>' + end_date_string + '</to>'
            except:
                end_date = None

        self.document_string = self.document_string + ReadTrackPacket.READ_TRACK_FOOTER_XML_CLAUSE

        self.packet_document = xml.dom.minidom.parseString(self.document_string)
        #self.sender_id = getpass.getuser() + ';' + socket.getfqdn() + \
        #    ';' + Packet.CLIENT_ID + ';' + str(datetime.datetime.now())
        #self.sender_version = Packet.CLIENT_ID + ';' + Packet.CLIENT_VERSION
        self.glider_element = self.packet_document.getElementsByTagName(Packet.GLIDER_TAG).item(0)
        #self.packet_element.setAttribute(Packet.SENDER_ID_ATTRIBUTE, self.sender_id)
        #self.packet_element.setAttribute(Packet.SENDER_VERSION_ATTRIBUTE, self.sender_version)
        self.glider_element.setAttribute(Packet.ID_ATTRIBUTE, glider_proxy.getName())

        # print self.packet_document.toxml('utf-8')

    ###########################################################################
    # Return the string representation of this packet.
    #
    # Returns:
    #  string - a string representation of this packet's XML document.
    #
    def __str__(self):
        '''
        Returns formatted xml string
        '''
        return self.packet_document.toxml('utf-8')

###############################################################################
# A glider's position track received from a dockserver in response to a client
# request.
#
class WriteTrackPacket(object):
    '''
    Dock Server sends this packet in response to receiving a ReadTrackPacket.

    Attributes:
      positions - (list of GliderPosition) The glider's track contained in this packet.

    Sample Packet:
    <?xml version="1.0" encoding="UTF-8"?>
    <glmpc>
      <writeTrack xmlns="">
        <identifier>
          <glider id="darwin" type="WRC"/>
        </identifier>
        <report>
          <location>
            <lat>2121.248</lat>
            <lon>-15756.557</lon>
          </location>
          <time>2006-06-21T23:18:37+00:00</time>
        </report>
        <report>
          <location>
            <lat>2121.842</lat>
            <lon>-15756.613</lon>
          </location>
          <time>2006-06-23T20:40:15+00:00</time>
        </report>
        <dataParameters>
          <dataParameter xsi:type="ns1:DataParameter"
                     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <name>param1</name>
            <value>value1</value>
          </dataParameter>
        </dataParameters>
      </writeTrack>
    </glmpc>
    '''
    # Class Constants

    ###########################################################################
    # Transforms the passed XML Document representation of this packet into
    # its class representation.
    #
    # Arguments:
    #  packet_document - (xml.dom.Document) This packet as an XML Document.
    #
    def __init__(self, packet_document = None):
        '''
        Constructor
        '''

        self.positions = []
        if packet_document:
            self.decode(packet_document)

    ###########################################################################
    # Transform the XML Document representation to its class representation.
    #
    # Arguments:
    #  packet_document - (xml.dom.Document) XML Document representing this
    #    packet's content.
    #
    def decode(self, packet_document):
        # Decode the <report> elements
        report_node_list = packet_document.getElementsByTagName(Packet.REPORT_TAG)
        for report_element in report_node_list:
            #print report_element.toxml()
            self.positions.append(GliderPosition(report_element))

    ###########################################################################
    # Returns the glider track contained in this packet.
    #
    # Returns:
    #  list of GliderPosition - A list of GPS positions representing this packet's
    #    glider track.
    #
    def getTrack(self):
        return self.positions

    ###########################################################################
    # Returns False - this packet is not a keepalive packet.
    #
    # Returns:
    #  bool - False always.
    #
    def isKeepAlive(self):
        # If this packet is a keep alive packet, then return true
        return False

    ###########################################################################
    # Returns False - this packet is never a close packet request.
    #
    # Returns:
    #  bool - False always.
    #
    def isCloseRequest(self):
        return False

    ###########################################################################
    # Returns this packet's action - always a write track action.
    #
    # Returns:
    #  string - write track action.
    #
    def getAction(self):
        return Packet.WRITE_TRACK_TAG

###############################################################################
# Dock Server clients send this packet to stage a mission (or mission sequence)
# to a glider.  The next time this glider surfaces, these staged mission files
# are transferred to the glider and the missions are sequenced.
#
class WriteMissionSequencePacket(Packet):
    '''
    Attributes:

    Sample Packet:
    <glmpc>
      <writeMissionSequence>
        <identifier>
          <glider id="bensim" />
        </identifier>
        <missionSequence>
          <mission filename="foo.mi" repetitions=1>
            <maFile>surfac01.ma</maFile>
            <maFile>goto_l01.ma</maFile>
            <maFile>yo01.ma</maFile>
            <maFile>sample01.ma</maFile>
            <file name="foo.mi">File content appears here</file>
            <file name="surfac01.ma">File content appears here</file>
            <file name="goto_l01.ma">File content appears here</file>
            <file name="yo01.ma">File content appears here</file>
            <file name="sample01.ma">File content appears here</file>
          </mission>
          <mission filename="bar.mi" repetitions=3>
            <maFile>surfac02.ma</maFile>
            <maFile>goto_l02.ma</maFile>
            <maFile>yo02.ma</maFile>
            <maFile>sample02.ma</maFile>
            <file name="bar.mi">File content appears here</file>
            <file name="surfac02.ma">File content appears here</file>
            <file name="goto_l02.ma">File content appears here</file>
            <file name="yo02.ma">File content appears here</file>
            <file name="sample02.ma">File content appears here</file>
          </mission>
          <mission filename="third.mi">
            <maFile>surfac03.ma</maFile>
            <maFile>goto_l03.ma</maFile>
            <maFile>yo03.ma</maFile>
            <maFile>sample03.ma</maFile>
            <file name="third.mi">File content appears here</file>
            <file name="surfac03.ma">File content appears here</file>
            <file name="goto_l03.ma">File content appears here</file>
            <file name="yo03.ma">File content appears here</file>
            <file name="sample03.ma">File content appears here</file>
          </mission>
        </missionSequence>
      </writeMissionSequence>
    </glmpc>
    '''
    # Class Constants

    ###########################################################################
    # Create a packet to send to dockserver that contains a mission sequence
    # to transfer to the glider for execution.  This packet contains the file
    # contents of all mission related files (mi, ma, gbf).
    #
    # Arguments:
    #  glider_proxy - (proxy.GliderProxy) Transfer the mission sequence to this
    #    glider.
    #  mission_sequence - (MissionSequence) The mission sequence that contains
    #    all mission files and the sequence command for transfer to the passed
    #    glider.
    #  packet_document - (xml.dom.Document)  For now, always None.  Clients
    #    do not recieve this packet type.
    #
    def __init__(self, glider_proxy, mission_sequence, packet_document = None):

        super(WriteMissionSequencePacket, self).__init__(Packet.GLMPC_XML_TEMPLATE, packet_document)
        # Add WriteMissionSequencePacket specific elements and attributes here
        # Add glider identification information
        glider_element = self.packet_document.createElement(Packet.GLIDER_TAG)
        glider_element.setAttribute(Packet.ID_ATTRIBUTE, glider_proxy.getName())
        identifier_element = self.packet_document.createElement(Packet.IDENTIFIER_TAG)
        identifier_element.appendChild(glider_element)

        # Transform passed mission sequence to xml representation
        mission_sequence_element = mission_sequence.toXml(self.packet_document)

        # Put it all together
        write_mission_sequence_element = self.packet_document.createElement(Packet.WRITE_MISSION_SEQUENCE_TAG)
        write_mission_sequence_element.appendChild(identifier_element)
        write_mission_sequence_element.appendChild(mission_sequence_element)
        self.packet_element.appendChild(write_mission_sequence_element)

###############################################################################
# Dock Server clients send this packet to adjust the currently running mission
# (or mission sequence) on a glider.  The next time this glider surfaces, these
# mission files are transferred to the glider and Ctrl-F is sent to re-read ma
# files.
#
class WriteMissionAdjustmentPacket(Packet):
    '''
    Attributes:

    Sample Packet:
    <glmpc>
      <writeMissionAdjustment>
        <identifier>
          <glider id="bensim" />
        </identifier>
        <maFiles>
          <maFile>surfac03.ma</maFile>
          <maFile>goto_l03.ma</maFile>
          <maFile>yo03.ma</maFile>
          <maFile>sample03.ma</maFile>
          <file name="surfac03">File content appears here</file>
          <file name="goto_l03.ma">File content appears here</file>
          <file name="yo03.ma">File content appears here</file>
          <file name="sample03.ma">File content appears here</file>
        </maFiles>
      </writeMissionAdjustment>
    </glmpc>
    '''
    # Class Constants

    ###########################################################################
    # Create a packet to send to dockserver that contains a mission adjustment
    # to transfer to the glider for execution.  This packet contains the file
    # contents of all mission adjustment files (ma and gbf).
    #
    # Arguments:
    #  glider_proxy - (proxy.GliderProxy) Transfer the mission adjustment to this
    #    glider.
    #  mission_adjustment - (MissionAdjustment) The mission adjustment that contains
    #    all adjustment files for transfer to the passed glider.
    #  packet_document - (xml.dom.Document)  For now, always None.  Clients
    #    do not recieve this packet type.
    #
    def __init__(self, glider_proxy, mission_adjustment, packet_document = None):

        super(WriteMissionAdjustmentPacket, self).__init__(Packet.GLMPC_XML_TEMPLATE, packet_document)
        # Add WriteMissionSequencePacket specific elements and attributes here
        # Add glider identification information
        glider_element = self.packet_document.createElement(Packet.GLIDER_TAG)
        glider_element.setAttribute(Packet.ID_ATTRIBUTE, glider_proxy.getName())
        identifier_element = self.packet_document.createElement(Packet.IDENTIFIER_TAG)
        identifier_element.appendChild(glider_element)

        # Create the packet's xml representation
        write_mission_adjustment_element = self.packet_document.createElement(Packet.WRITE_MISSION_ADJUSTMENT_TAG)
        write_mission_adjustment_element.appendChild(identifier_element)
        if mission_adjustment is not None:
            # If a mission adjustment is passed, then transform it to xml representation
            # Note: mission_adjustment is None means cancel the staged mission adjustment
            ma_files_element = mission_adjustment.toXml(self.packet_document)
            write_mission_adjustment_element.appendChild(ma_files_element)

        self.packet_element.appendChild(write_mission_adjustment_element)

###############################################################################
# Dock Server clients send this packet to request the current running mission.
#
class ReadCurrentSequencePacket(Packet):
    '''
    Attributes:

    Sample Packet:
    <glmpc>
      <readCurrentSequence>
        <identifier>
          <glider id="glider01"/>
        </identifier>
      </readCurrentSequence>
    </glmpc>
    '''
    # Class Constants
    READ_CURRENT_SEQUENCE_XML_TEMPLATE = '<glmpc><readCurrentSequence><identifier><glider id=""/></identifier></readCurrentSequence></glmpc>'

    ###########################################################################
    # Create a packet to send to the dockserver that requests the details of
    # the current running mission.  Note that this mission must have been staged
    # for this request to work (i.e., not manually run).
    #
    # Arguments:
    #  glider_proxy - (GliderProxy) Request the running mission of this glider.
    #  packet_document - (xml.dom.Document) For now, always None.  This packet type
    #    is never received by clients.
    #
    def __init__(self, glider_proxy, packet_document = None):

        super(ReadCurrentSequencePacket, self).__init__(self.READ_CURRENT_SEQUENCE_XML_TEMPLATE, packet_document)
        # Insert the name of the passed glider proxy
        glider_element = self.packet_element.getElementsByTagName(Packet.GLIDER_TAG).item(0)
        glider_element.setAttribute(Packet.ID_ATTRIBUTE, glider_proxy.getName())

###############################################################################
# Dockserver clients receive this packet in response to sending dockserver a ReadCurrentSequencePacket.
# This packet contains the current running mission sequence and all its files.
# The returned mission sequence must have been staged before it can be returned
# as the current running mission (that is, if a mission is manually run, it will
# never show up in this packet).
#
class WriteCurrentSequencePacket(Packet):
    '''
    Attributes:
      action - (string) The specific packet within a 'class' or type.
      mission_sequence - (MissionSequence) Class representation of this packet's mission sequence.
        Includes all mission file contents and sequence command.
      glider_name - (string) The name of the glider running this mission sequence.

    Sample Packet:
    <glmpc>
      <writeCurrentSequence>
        <identifier>
          <glider id="glider01">
        </identifier>
        <missionSequence>
          <mission filename="foo.mi" repetitions=1>
            <maFile>surfac01.ma</maFile>
            <maFile>goto_l01.ma</maFile>
            <maFile>yo01.ma</maFile>
            <maFile>sample01.ma</maFile>
            <file name="foo.mi">File content appears here</file>
            <file name="surfac01.ma">File content appears here</file>
            <file name="goto_l01.ma">File content appears here</file>
            <file name="yo01.ma">File content appears here</file>
            <file name="sample01.ma">File content appears here</file>
          </mission>
          <mission filename="bar.mi" repetitions=3>
            <maFile>surfac02.ma</maFile>
            <maFile>goto_l02.ma</maFile>
            <maFile>yo02.ma</maFile>
            <maFile>sample02.ma</maFile>
            <file name="bar.mi">File content appears here</file>
            <file name="surfac02.ma">File content appears here</file>
            <file name="goto_l02.ma">File content appears here</file>
            <file name="yo02.ma">File content appears here</file>
            <file name="sample02.ma">File content appears here</file>
          </mission>
          <mission filename="third.mi">
            <maFile>surfac03.ma</maFile>
            <maFile>goto_l03.ma</maFile>
            <maFile>yo03.ma</maFile>
            <maFile>sample03.ma</maFile>
            <file name="third.mi">File content appears here</file>
            <file name="surfac03.ma">File content appears here</file>
            <file name="goto_l03.ma">File content appears here</file>
            <file name="yo03.ma">File content appears here</file>
            <file name="sample03.ma">File content appears here</file>
          </mission>
        </missionSequence>
      </writeCurrentSequence>
    </glmpc>
    '''
    # Class Constants

    ###########################################################################
    # Transform the passed XML document representation of this packet into its
    # class representation.
    #
    # Arguments:
    #  packet_document - (xml.dom.Document) The XML document representation of
    #    this packet.
    #
    def __init__(self, packet_document = None):

        super(WriteCurrentSequencePacket, self).__init__(None, packet_document)
        self.action = Packet.WRITE_CURRENT_SEQUENCE_TAG
        self.mission_sequence = None
        # Decode the packet specific portions
        # Decode the glider's name
        glider_element = self.packet_element.getElementsByTagName(Packet.GLIDER_TAG).item(0)
        self.glider_name = glider_element.getAttribute(Packet.ID_ATTRIBUTE)
        # Decode the mission sequence
        mission_sequence_node_list = self.packet_element.getElementsByTagName(MissionSequence.MISSION_SEQUENCE_TAG)
        if mission_sequence_node_list.length > 0:
            self.mission_sequence = MissionSequence(mission_sequence_element = mission_sequence_node_list.item(0))

    ###########################################################################
    # Returns this packet's mission sequence.
    #
    # Returns:
    #  MissionSequence - Class representationn of this packet's mission sequence.
    #
    def getMissionSequence(self):
        return self.mission_sequence

    ###########################################################################
    # Returns the name of this packet's glider; the glider running the returned
    # mission sequence.
    #
    # Returns:
    #  string - Name of the glider running the returned mission sequence.
    #
    def getGliderName(self):
        return self.glider_name

class GliderCommandRequest(Packet):
    '''
    Attributes:
      type - (string) The general 'class' or type of packet.
      action - (string) The specific packet within a 'class' or type.
    '''
    # Class Constants

    def __init__(self, glider_link_proxy, command, packet_document = None):
        '''
        Purpose: Create a packet to send to dockserver that requests the passed command
          be sent to the passed glider link.

        Arguments:
          glider_link_proxy - (GliderLinkProxy) The glider link proxy to send the passed
            command to.
          command - (string) The gliderDOS or picoDOS command to send to the passed
            glider link.
          packet_document - (xml.dom.Document) If receiving a packet, the xml document to
            decode.
        '''
        super(GliderCommandRequest, self).__init__(Packet.PACKET_XML_TEMPLATE, packet_document)
        # Setup packet type and action attributes
        self.type = Packet.REQUEST
        self.action = Packet.GLIDER_COMMAND
        self.packet_element.setAttribute(Packet.TYPE_ATTRIBUTE, self.type)
        self.packet_element.setAttribute(Packet.ACTION_ATTRIBUTE, self.action)
        self.packet_element.setAttribute(Packet.COMMAND_ATTRIBUTE, command)
        self.packet_element.appendChild(glider_link_proxy.toXml(self.packet_document))
        # print self.packet_document.toxml('utf-8')


###############################################################################
# Clients send this packet to dockserver to subscribe to glider status events
# (received as GliderStatusResponse packets).  Only need to send one per glider.
# Must close network connection to stop receiving status packets.
#
class GliderConnectRequest(Packet):
    '''
    Attributes:
      type - (string) The general 'class' or type of packet.
      action - (string) The specific packet within a 'class' or type.
    '''
    # Class Constants

    ###########################################################################
    # Create a packet to send to dockserver that requests a subscription to
    # status events for the passed glider.
    #
    # Arguments:
    #  glider_proxy - (proxy.GliderProxy) Subscribe to this glider's status
    #    events.
    #  packet_document - (xml.dom.Document) Always None.  Document created by
    #    superclass.
    #
    def __init__(self, glider_proxy, packet_document = None):
        '''
        Constructor
        '''
        super(GliderConnectRequest, self).__init__(Packet.PACKET_XML_TEMPLATE, packet_document)
        # Set the glider's name for status request.
        # Setup packet type and action attributes
        self.type = Packet.REQUEST
        self.action = Packet.GLIDER_CONNECT
        self.packet_element.setAttribute(Packet.TYPE_ATTRIBUTE, self.type)
        self.packet_element.setAttribute(Packet.ACTION_ATTRIBUTE, self.action)
        self.packet_element.setAttribute(Packet.GLIDER_NAME_ATTRIBUTE, glider_proxy.getName())
        # print self.packet_document.toxml('utf-8')

###############################################################################
# This packet is received from a dockserver each time glider dialog is received
# by that dockserver.
#
class GliderConnectResponse(Packet):
    '''
    Attributes:
      type - (string) The general 'class' or type of packet.
      action - (string) The specific packet within a 'class' or type.
      glider_link - (GliderLinkProxy) The dockserver glider link that produced this packet's glider dialog.
      glider_output - (string) The glider dialog contained in this packet.
    '''
    # Class Constants

    ###########################################################################
    # Transforms the passed XML Document into a class representation of
    # dialog received from the glider.
    #
    # Arguments:
    #  packet_dockument - (xml.dom.Document) An XML representation of a glider
    #     connect response packet received from a dockserver.
    #
    def __init__(self, packet_document = None):
        '''
        Constructor
        '''
        super(GliderConnectResponse, self).__init__(Packet.PACKET_XML_TEMPLATE, packet_document)
        # print self.packet_document.toxml('utf-8')
        self.type = Packet.RESPONSE
        self.action = Packet.GLIDER_CONNECT
        self.glider_link = None
        self.glider_output = None
        if packet_document:
            #print 'In DockConfigurationResponse constructor...'
            #print packet_document.toxml()
            # If a packet document was passed, then decode received packet.
            self.decode()
        else:
            # Setup packet type and action attributes
            self.packet_element.setAttribute(Packet.TYPE_ATTRIBUTE, self.type)
            self.packet_element.setAttribute(Packet.ACTION_ATTRIBUTE, self.action)

    ###########################################################################
    # Return the glider link that produced this packet's glider dialog.
    #
    # Returns:
    #  (GliderLinkProxy) - proxy to the glider link that produced this packet's
    #  dialog.
    #
    def getGliderLink(self):
        return self.glider_link

    ###########################################################################
    # Return the glider dialog contained in this packet.
    #
    # Returns:
    #  (string) -
    #
    def getGliderOutput(self):
        return self.glider_output

    ###########################################################################
    # PRIVATE Interface
    ###########################################################################

    ###########################################################################
    # Decode this packet's XML representation into its class representation.
    #
    def decode(self):
        # Get the glider link information
        glider_link_node_list = self.packet_element.getElementsByTagName(Packet.GLIDER_LINK_TAG)
        self.glider_link = GliderLinkProxy(glider_link_node_list.item(0))

        # Get the glider dialog in the packet.
        text_node_list = self.packet_element.getElementsByTagName(Packet.TEXT_TAG)
        text = []
        for text_node in text_node_list:
            text.append(self.getText(text_node.childNodes))
        self.glider_output = ''.join(text)

###############################################################################
# Packet to request an event log action such as return next / previous log page
# or add a new log entry.  Sent from client to dockserver.
#
class EventLogRequestPacket(Packet):
    '''
    Attributes:
      type - (string) The general 'class' or type of packet.
      action - (string) The specific packet within a 'class' or type.
    '''
    # Class Constants
    # Types of event log requests that can be made to the dockserver.
    NEXT_PAGE_REQUEST = '1'
    PREVIOUS_PAGE_REQUEST = '2'
    SUBSCRIBE_TO_EVENT_LOG = '3'
    UNSUBSCRIBE_TO_EVENT_LOG = '4'
    ADD_EVENT = '5'
    REPLACE_EVENT = '6' # Not implemented as of dockserver release 7.9
    DELETE_EVENT = '7'  # Not implemented as of dockserver release 7.9
    PAGE_REQUEST = '8'

    ###########################################################################
    # Create a packet encompassing the passed event log request to be sent to
    # dockserver.
    #
    # Arguments:
    #  request_type - (string)
    #  log_page - (string) Date format of YYYYMMDD
    #  log_entry - (EventLogEntry)
    #  packet_document - (xml.dom.Document)
    #
    def __init__(self, request_type, log_page = None, log_entry = None, packet_document = None):
        '''
        Constructor
        '''
        super(EventLogRequestPacket, self).__init__(Packet.PACKET_XML_TEMPLATE, packet_document)
        # Setup packet type and action attributes
        self.type = Packet.PACKET
        self.action = Packet.LOG_REQUEST
        self.packet_element.setAttribute(Packet.TYPE_ATTRIBUTE, self.type)
        self.packet_element.setAttribute(Packet.ACTION_ATTRIBUTE, self.action)
        # Add packet specifics
        request_element = self.packet_document.createElement(Packet.REQUEST_TAG)
        request_element.setAttribute(Packet.TYPE_ATTRIBUTE, request_type)
        if log_page:
            request_element.setAttribute(Packet.PAGE_ATTRIBUTE, log_page)
        if log_entry:
            # If a log entry is passed, add it to the xml packet
            log_entry_element = log_entry.toXml(self.packet_document)
            request_element.appendChild(log_entry_element)
        # Put the packet together
        self.packet_element.appendChild(request_element)
        # print self.packet_document.toxml('utf-8')

###############################################################################
# Sent by dockserver upon request from a client.  Contains all event log entries
# for a particular day (i.e., page).
# Can request a specific log page (page == YYYYMMDD) or recieve today's page
# upon request to subscribe to glider event log notifications.
#
class EventLogPagePacket(Packet):
    '''
    Attributes:
      type - (string) The general 'class' or type of packet.
      action - (string) The specific packet within a 'class' or type.
      log_page - (EventLogPage) Class representation of this packet's log page.
    '''
    # Class Constants

    ###########################################################################
    # Transform the received event log packet into its class representation.
    #
    # Arguments:
    #  packet_document - (xml.dom.Document) The XML document that represents
    #    an event log page packet received from dockserver.
    #
    def __init__(self, packet_document = None):
        '''
        Constructor
        '''
        super(EventLogPagePacket, self).__init__(Packet.PACKET_XML_TEMPLATE, packet_document)

        self.type = Packet.PACKET
        self.action = Packet.LOG_PAGE
        if packet_document:
            #print self.packet_document.toxml('utf-8')
            # If a packet document was passed, then decode received packet.
            log_page_element = self.packet_element.getElementsByTagName(EventLogPage.LOG_PAGE_TAG).item(0)
            self.log_page = EventLogPage(log_page_element)

    ###########################################################################
    # Return the event log page received in this packet from dockserver.
    #
    # Returns:
    #  EventLogPage - The event log page received in this packet.
    #
    def getLogPage(self):
        return self.log_page

###############################################################################
# Represent event log notifications sent by dockserver each time a log event occurs.
# These packets are sent to client that have subscribed to the event log.
#
class EventLogNotificationPacket(Packet):
    '''
    Attributes:
      type - (string) The general 'class' or type of packet.
      action - (string) The specific packet within a 'class' or type.
      log_entry - (EventLogEntry) Class representation of this packet's event
        log entry - usually a new entry to add to the current page.
    '''
    # Class Constants

    ###########################################################################
    # Transforms the passed XML document representing this packet into its
    # class representation.
    #
    # Arguments:
    #  packet_document - (xml.dom.Document) XML document representing the received
    #    notification packet.
    #
    def __init__(self, packet_document = None):
        '''
        Constructor
        '''
        super(EventLogNotificationPacket, self).__init__(Packet.PACKET_XML_TEMPLATE, packet_document)

        self.type = Packet.PACKET
        self.action = Packet.LOG_NOTIFICATION
        self.log_entry = None
        if packet_document:
            # If a packet document was passed, then decode received packet.
            log_entry_nodelist = self.packet_element.getElementsByTagName(EventLogEntry.LOG_ENTRY_TAG)
            self.log_entry = eventLogEntryFactory(log_entry_nodelist.item(0))
            # print self.packet_document.toxml('utf-8')

    ###########################################################################
    # Returns this packet's event log entry.
    #
    # Returns:
    #  EventLogEntry - This packet's event log entry.
    #
    def getLogEntry(self):
        return self.log_entry

###############################################################################
# Packet to request a glider related change on the dockserver.  Only used for
# hosting dockserver administration.  Moves a glider between two host dockservers
# or adds a new glider to a hosting dockserver.
#
class DockChangesPacket(Packet):
    '''
    Attributes:

    Sample XML representation (only transfer and add glider are implemented):
<packet
    type="packet"
    action="dockChanges"
    host="default"
    userID="phpbb-userID"
    senderBuildVersion="glider 6.37 UNRELEASED DEVELOPMENT NONAUTOMATED BUILD"
    senderVersion="Glider Terminal;1.7"
    senderOptions="option1=value1;option2=value2">
  <!-- The changes element appears in this packet and the dock status packet (as a
       response to this packet being sent to the dockserver).
    -->
  <changes>
    <!-- Backup and remove glider ann -->
    <change gliderName="ann" remove="true" backup="true"/>
    <!-- Backup glider bob -->
    <change gliderName="bob" remove="false" backup="true"/>
    <!-- Remove glider tom (no backup) -->
    <change gliderName="tom" remove="true" backup="false"/>
    <!-- Only available to hosting dockservers.  Transfer glider from this dockserver host to another -->
    <change gliderName="darwin" destinationHost="twr" />
    <change gliderName="nemesis" destinationHost="ooi" />
    <!-- Only available to hosting dockservers.  Add a new glider this this dockserver's managed gliders -->
    <change gliderName="ann" add="true" />
  </changes>
</packet>
    '''

    # Class constants
    # XML representation
    CHANGES_TAG = 'changes'

    ###########################################################################
    # Create a packet encompassing the passed dockserver host change request.
    #
    # Arguments:
    #  changes - (GliderChange or HostChanges) The details of the requested dockserver host changes.
    #  packet_document - (xml.dom.Document)
    #
    def __init__(self, changes, packet_document = None):
        '''
        Constructor
        '''
        super(DockChangesPacket, self).__init__(Packet.PACKET_XML_TEMPLATE, packet_document)
        # Setup packet type and action attributes
        self.type = Packet.PACKET
        self.action = Packet.DOCK_CHANGES
        self.packet_element.setAttribute(Packet.TYPE_ATTRIBUTE, self.type)
        self.packet_element.setAttribute(Packet.ACTION_ATTRIBUTE, self.action)
        # Add packet specifics
        changes_element = self.packet_document.createElement(DockChangesPacket.CHANGES_TAG)
        change_element = changes.toXml(self.packet_document)
        self.packet_element.appendChild(changes_element)
        changes_element.appendChild(change_element)
        #print self.packet_document.toxml('utf-8')

###############################################################################
# This packet is sent to dockservers to request a connection - acts like a
# subscription request to dock status packets.
#
class DockConnectRequest(Packet):
    '''
    Attributes:
      type - (string) The general 'class' or type of packet.
      action - (string) The specific packet within a 'class' or type.
    '''
    # Class Constants

    ###########################################################################
    # Creates a packet to subscribe to dock status packets
    #
    # Arguments:
    #  packet_document - (xml.dom.Document) Ignored for now.
    #
    def __init__(self, packet_document = None):
        '''
        Constructor
        '''
        super(DockConnectRequest, self).__init__(Packet.PACKET_XML_TEMPLATE, packet_document)
        # Setup packet type and action attributes
        self.type = Packet.REQUEST
        self.action = Packet.DOCK_CONNECT
        self.packet_element.setAttribute(Packet.TYPE_ATTRIBUTE, self.type)
        self.packet_element.setAttribute(Packet.ACTION_ATTRIBUTE, self.action)

        # print self.packet_document.toxml('utf-8')

###############################################################################
# Sent by dockserver to proxy's that have subscribed via DockConnectRequest
# packets.  Contain a text message to display detailing dockserver status and
# results of dockserver change requests.
#
class DockStatusPacket(Packet):
    '''
    Attributes:
      type - (string) The general 'class' or type of packet.
      action - (string) The specific packet within a 'class' or type.
      message - (string) Content of the dock status packet message
    '''
    # Class Constants
    MESSAGE_TAG = 'message'

    ###########################################################################
    # Transform the received dock status packet into its class representation.
    #
    # Arguments:
    #  packet_document - (xml.dom.Document) The XML document that represents
    #    a dock status packet received from dockserver.
    #
    def __init__(self, packet_document = None):
        '''
        Constructor
        '''
        super(DockStatusPacket, self).__init__(Packet.PACKET_XML_TEMPLATE, packet_document)

        self.type = Packet.PACKET
        self.action = Packet.DOCK_STATUS
        if packet_document:
            #print self.packet_document.toxml('utf-8')
            # If a packet document was passed, then decode received packet.
            message_element = self.packet_element.getElementsByTagName(DockStatusPacket.MESSAGE_TAG).item(0)
            self.message = self.getText(message_element.childNodes)

    ###########################################################################
    # Return the message received in this packet from dockserver.
    #
    # Returns:
    #  message - (string) The dock status packet's text message
    #
    def getMessage(self):
        return self.message

###############################################################################
# Represents all the event log entries for a particular log page.  A log page
# corresponds to all log events that occur on the same date.  Log pages are
# labeled using their date - YYYYMMDD - an 8-digit ID.
#
class EventLogPage(object):
    '''
    Attributes:
      page_id - (string) Page ID - has the format YYYYMMDD.
      has_next - (boolean) True if this page has a next page.
      has_previous - (boolean) True if this page has a previous page.
      log_entries - (list of EventLogEntry) List of event log entries.
      glider_names - (list of string) List of glider names associated with this
        page's log entries.

    XML Representation:
    <logPage
      page="20111103"
      previous="false"
      next="true"
    >
      <logEntry>...</logEnry>
      <logEntry>...</logEnry>
      <logEntry>...</logEnry>
    </logPage>

    '''
    # Class Constants
    # XML tags and attribute names
    PAGE_ATTRIBUTE = 'page'
    PREVIOUS_ATTRIBUTE = 'previous'
    NEXT_ATTRIBUTE = 'next'
    LOG_PAGE_TAG = 'logPage'

    ###########################################################################
    # Returna new log page that contains a class presentation of the passed
    # XML element.
    #
    # Arguments:
    #  log_page_element - (xml.dom.Element) XML representation of an event log
    #    page received from a dockserver.
    #
    def __init__(self, log_page_element = None):

        self.log_entries = []
        self.glider_names = []
        if log_page_element:
            # If an xml element is passed, then decode it.
            self.decode(log_page_element)

    ###########################################################################
    # Transform the passed XML representation of a log page into its class
    # representation.
    #
    # Arguments:
    #  log_page_element - (xml.dom.Element) XML representation of an event log
    #    page received from a dockserver.
    #
    def decode(self, log_page_element):
        # Decode page attributes
        #print 'In EventLogPage.decode: ' + log_page_element.toprettyxml()
        self.page_id = log_page_element.getAttribute(self.PAGE_ATTRIBUTE)
        self.has_next = (log_page_element.getAttribute(self.NEXT_ATTRIBUTE) == TRUE)
        self.has_previous = (log_page_element.getAttribute(self.PREVIOUS_ATTRIBUTE) == TRUE)
        # Decode this page's log entries
        log_entry_nodelist = log_page_element.getElementsByTagName(EventLogEntry.LOG_ENTRY_TAG)
        for log_entry_element in log_entry_nodelist:
            #print 'EventLogPage.decode: decoding log entry - ' + log_entry_element.toprettyxml()
            log_entry = eventLogEntryFactory(log_entry_element)
            if log_entry:
                # If log entry recognized, then add it to the list.
                self.log_entries.append(log_entry)
                glider_name = log_entry.getGliderName()
                if glider_name not in self.glider_names and glider_name != "" and glider_name:
                    self.glider_names.append(glider_name)
            else:
                # If not recognized, then notify user.
                print 'Unknown log entry from XML:'
                print log_entry_element
        #print 'EventLogPage.decode: Done decoding page xml'

    ###########################################################################
    # Add the passed log entry to this log page.
    #
    # Arguments:
    #  entry - (EventLogEntry) The glider event log entry to add to this page.
    #
    def addEntry(self, entry):
        self.log_entries.append(entry)

    ###########################################################################
    # Return the list of this log page's entries.
    #
    # Returns:
    #  list of EventLogEntry - A list of this page's log entries.
    #
    def getLogEntries(self):
        return self.log_entries

    ###########################################################################
    # Returns indication of an existing previous log page.
    #
    # Returns:
    #  bool - True if a log page for a date older than this page exists.  False
    #    otherwise.
    #
    def hasPrevious(self):
        return self.has_previous

    ###########################################################################
    # Returns indication of a log page for a newer date than this page.
    #
    # Returns:
    #  bool - True if a page with a newer date than this page exists.
    #
    def hasNext(self):
        return self.has_next

    ###########################################################################
    # Returns this page's ID.  A page's ID is the date of all its entries formatted
    # as YYYYMMDD.
    #
    # Returns:
    #  string - this page's ID formatted as YYYYMMDD
    #
    def getID(self):
        return self.page_id

    ###########################################################################
    # Return the glider names associated with event log entries of this page.
    #
    # Returns:
    #  list of string - A list of glider names associated with this log page.
    #
    def getGliderNames(self):
        return self.glider_names

###############################################################################
# A glider mission sequence.  Contains the actual sequence command and the
# content of all the file (mi, ma, and gbf) associated with each mission in
# the sequence.
#
class MissionSequence(object):
    '''
    Attributes:
      missions - (list of Mission) The list of glider missions that make up
        this sequence.
    '''
    # Class Constants
    # XML tags and attribute names
    MISSION_SEQUENCE_TAG = 'missionSequence'

    ###########################################################################
    # Creates a sequence of glider missions from one of the passed list of missions
    # or XML element representing a mission sequence.
    #
    # Arguments:
    #  missions - (list of Mission) If not None, make a mission sequence from this
    #    list of missions.
    #  mission_sequence_element (xml.dom.Element) If not None, make a mission sequence
    #    from this passed XML representation of a mission sequence.
    #
    def __init__(self, missions = None, mission_sequence_element = None):

        if missions == None:
            missions = []
        self.missions = missions
        if mission_sequence_element:
            # If an xml element is passed, then decode it.
            self.decode(mission_sequence_element)

    ###########################################################################
    # Add the passed mission to the end of this mission sequence.
    #
    def addMission(self, mission):
        self.missions.append(mission)

    ###########################################################################
    # Replace this sequence's missions with the passed list of missions.
    #
    def setMissions(self, missions):
        self.missions = missions

    ###########################################################################
    # Returns this sequence's list of missions.
    #
    # Returns:
    #  list of Mission - A list of this sequence's missions.
    #
    def getMissions(self):
        return self.missions

    ###########################################################################
    # Return the mission currently running on the glider from this sequence.
    #
    # Returns:
    #  Mission - The mission currently running on a glider from this sequence.
    #    Returns None if no mission is running from this sequence.
    #
    def getRunningMission(self):
        for mission in self.missions:
            if mission.isRunning():
                return mission
        return None

    ###########################################################################
    # Transform the passed XML mission sequence representation into its class
    # form.
    #
    # Arguments:
    #  mission_sequence_element - (xml.dom.Element) A mission sequence represented
    #    in XML.
    #
    def decode(self, mission_sequence_element):
        # Decode the missions in the passed sequence element
        mission_element_nodelist = mission_sequence_element.getElementsByTagName(Packet.MISSION_TAG)
        for mission_element in mission_element_nodelist:
            self.missions.append(Mission(mission_element = mission_element))

    ###########################################################################
    # Transform this mission sequence into its XML representation.
    #
    # Arguments:
    #  packet_document - (xml.dom.Document) The XML document to use in creating
    #    the XML representation of this mission sequence.
    #
    # Returns:
    #  xml.dom.Element - The XML representation of this mission sequence for
    #    sending over the network to other GMC components (like dockserver).
    #
    def toXml(self, packet_document):
        # Build xml representation and return it
        mission_sequence_element = packet_document.createElement(self.MISSION_SEQUENCE_TAG)

        # Append each mission in the sequence.
        for mission in self.missions:
            mission_sequence_element.appendChild(mission.toXml(packet_document))

        return mission_sequence_element

    ###########################################################################
    # Returns the event log note for this mission sequence displayed in the
    # Log Book tab.
    #
    # Returns:
    #  string - The note for this mission sequence - contains mission names
    #    number of repetitions for each ala a sequence command.
    #
    def getNote(self):
        note = ''
        for mission in self.missions:
            note = note + mission.getNote() + ' '
        return note

    ###########################################################################
    # Returns the string representation of this mission sequence.
    #
    # Returns:
    #  string - A string containing the mission filenames and repetition factor for
    #    each mission in this sequence.
    #
    def __str__(self):
        missions_string = ''
        for mission in self.missions:
            missions_string = missions_string + str(mission) + '\n'
        return missions_string

###############################################################################
# Represents a glider mission for the purposes of transfer between dockservers
# and their clients.
#
class Mission(object):
    '''
    Attributes:
      filename - (string) Glider mission filename.
      gbf_filename - (string) Glider batch filename associated with this mission.
      repetitions - (int) sequence gliderDOS command repetitions
      ma_filenames - (list of string) List of ma filenames referenced by this mission.
      mission_files - (dict) Keys are filenames and values are file content.  Includes
        all files that are part of this mission (mi, ma, and gbf).
      running - (bool) Is this mission currently running indicator.

    Sample XML representation:

    <mission filename="third.mi" gbf="abc.gbf" repetitions=3 isRunning="true">
      <maFile>surfac03.ma</maFile>
      <maFile>goto_l03.ma</maFile>
      <maFile>yo03.ma</maFile>
      <maFile>sample03.ma</maFile>
      <file name="third.mi">File content appears here</file>
      <file name="surfac03.ma">File content appears here</file>
      <file name="goto_l03.ma">File content appears here</file>
      <file name="yo03.ma">File content appears here</file>
      <file name="sample03.ma">File content appears here</file>
      <file name="abc.gbf">File content appears here</file>
    </mission>
    '''
    # Class Constants
    MISSION_FILE_EXTENSIONS = ['.mi', '.ma', '.gbf']

    ###########################################################################
    # Create and initialize a glider mission from the passed arguments.
    #
    # Arguments:
    #  filename - (string) Glider mission filename (.mi)
    #  gbf_filename - (string) Glider batch filename (.gbf)
    #  repetitions - (int) Number of times this mission should be sequenced.
    #  ma_filenames - (list of string) The ma filenames referenced by this filename.
    #  mission_element - (xml.dom.Element) XML represenation of an entire mission.
    #    Contains all mission filenames and their content as well as repetitions.
    #
    def __init__(self, filename = None, gbf_filename = None, repetitions = 1, ma_filenames = None, mission_element = None):

        self.filename = filename
        self.gbf_filename = gbf_filename
        self.repetitions = repetitions
        if ma_filenames == None:
            ma_filenames = []
        self.ma_filenames = ma_filenames
        # Dictionary with mission filenames as keys and their content as values (strings).
        self.mission_files = {}
        self.running = False
        if mission_element:
            self.decode(mission_element)

    ###########################################################################
    # Returns this mission's name.
    #
    # Returns:
    #  string - This mission's name.  Taken as the filename minus its extension.
    #
    def getName(self):
        filename_parts = self.filename.split('.')
        return filename_parts[0]

    ###########################################################################
    # Sets the passed filename as the missin filename.
    #
    # Arguments:
    #  mission_filename - (string) This mission's mi filename.
    #
    def setFileName(self, mission_filename):
        self.filename = mission_filename

    ###########################################################################
    # Returns this missions mi filename - including the extension.
    #
    # Returns:
    #  string - The filename for this mission's mi file.
    #
    def getFileName(self):
        return self.filename

    ###########################################################################
    # Sets this mission's glider batch filename to the pass filename.
    #
    # Arguments:
    #  gbf_filename - (string) This mission's new glider batch filename.
    #
    def setGBFFileName(self, gbf_filename):
        self.gbf_filename = gbf_filename

    ###########################################################################
    # Returns this misison's glider batch filesname.
    #
    # Returns:
    #  string - This misison's glider batch filename.
    #
    def getGBFFileName(self):
        return self.gbf_filename

    ###########################################################################
    # Set this mission's sequence repetition to the passed number.
    #
    # Arguments:
    #  repetitions - (int) This mission's new sequence repetition number.
    #
    def setRepetitions(self, repetitions):
        self.repetitions = repetitions

    ###########################################################################
    # Returns this misison's sequence repetition number.
    #
    # Returns:
    #  int - This mission's sequence repetition number.
    #
    def getRepetitions(self):
        return self.repetitions

    ###########################################################################
    # Add the passed ma filename to this mission's list of referenced ma files.
    #
    # Arguments:
    #  ma_filename - (string) An ma filename to add to this mission's list of
    #    referenced ma filenames.
    #
    def addMaFilename(self, ma_filename):
        self.ma_filenames.append(ma_filename)

    ###########################################################################
    # Set this mission's list of referenced ma filenames to the passed list.
    # Replaces old list of ma filenames.
    #
    # Arguments:
    #  ma_filenames - (list of string) New list of referenced ma filenames for
    #    this mission.
    #
    def setMaFilenames(self, ma_filenames):
        self.ma_filesnames = ma_filenames

    ###########################################################################
    #
    #
    def getMaFilenames(self):
        return self.ma_filenames

    def addFile(self, name, content):
        self.mission_files[name] = content

    def isRunning(self):
        return self.running

    # Return a dictionary where keys are mission filesnames and values are file
    # contents.
    def getFiles(self):
        return self.mission_files

    def decode(self, mission_element):
        self.filename = mission_element.getAttribute(Packet.FILENAME_ATTRIBUTE)
        if mission_element.hasAttribute(Packet.GBF_ATTRIBUTE):
            self.gbf_filename = mission_element.getAttribute(Packet.GBF_ATTRIBUTE)
        if mission_element.hasAttribute(Packet.REPETITIONS_ATTRIBUTE):
            self.repetitions = mission_element.getAttribute(Packet.REPETITIONS_ATTRIBUTE)
        if mission_element.hasAttribute(Packet.IS_RUNNING_ATTRIBUTE):
            self.running = mission_element.getAttribute(Packet.IS_RUNNING_ATTRIBUTE)
        # Decode any ma files
        ma_file_element_nodelist = mission_element.getElementsByTagName(Packet.MA_FILE_TAG)
        for ma_file_element in ma_file_element_nodelist:
            self.ma_filenames.append(ma_file_element.firstChild.data)
        # Decode any mission file content
        file_element_nodelist = mission_element.getElementsByTagName(Packet.FILE_TAG)
        for file_element in file_element_nodelist:
            file_name = file_element.getAttribute(Packet.NAME_ATTRIBUTE)
            self.mission_files[file_name] = file_element.firstChild.data

    def toXml(self, packet_document):
        mission_element = packet_document.createElement(Packet.MISSION_TAG)
        mission_element.setAttribute(Packet.FILENAME_ATTRIBUTE, self.filename)
        if self.gbf_filename is not None:
            mission_element.setAttribute(Packet.GBF_ATTRIBUTE, self.gbf_filename)
        mission_element.setAttribute(Packet.REPETITIONS_ATTRIBUTE, str(self.repetitions))

        for ma_filename in self.ma_filenames:
            ma_filename_textnode = packet_document.createTextNode(ma_filename)
            ma_file_element = packet_document.createElement(Packet.MA_FILE_TAG)
            ma_file_element.appendChild(ma_filename_textnode)
            mission_element.appendChild(ma_file_element)

        # Encode mission files and their content - if any are provided.
        for file_name, file_content in self.mission_files.iteritems():
            file_content_textnode = packet_document.createTextNode(file_content)
            file_element = packet_document.createElement(Packet.FILE_TAG)
            file_element.setAttribute(Packet.NAME_ATTRIBUTE, file_name)
            file_element.appendChild(file_content_textnode)
            mission_element.appendChild(file_element)

        return mission_element

    def exportAsFiles(self, path):
        # Write out all mission files to the passed directory
        for file_name, file_content in self.mission_files.iteritems():
            mission_file = open(os.path.join(path, file_name), 'w')
            mission_file.write(file_content)
            mission_file.close()

    def removeAllFiles(self, path):
        # Remove all mission files (not just this mission's files) from the passed directory.
        # All *.mi, *.ma, and *.gbf are removed.
        files = os.listdir(path)
        for file in files:
            if not os.path.isdir(file):
                filename, file_extension = os.path.splitext(file)
                if file_extension.lower() in self.MISSION_FILE_EXTENSIONS:
                    full_path = os.path.join(path, file)
                    # print 'In Mission.removeAllFiles...' + ' removing ' + full_path
                    os.remove(full_path)

    def getNote(self):
        return self.filename + '(' + self.repetitions + ')'

    def __str__(self):
        mission_string = 'Filename: ' + self.filename + ' GBF: ' + self.gbf_filename + ' Repetitions: ' + self.repetitions + ' MA files: '

        for ma_filename in self.ma_filenames:
            mission_string = mission_string + ' ' + ma_filename
        return mission_string

###############################################################################
class GliderChange(object):
    '''
    Attributes:
      change_type - (int) Type of requested dockserver change.
      glider_name - (String) Name of glider involved in the change.
      destination_host - (String) In a glider move request, the destination hosting dockserver
      glider_backup_file - (String) In a restore request, the glider backup .tar.gz filename.

    Sample XML representation (only transfer and add glider are implemented):
    <!-- Backup and remove glider ann -->
    <change gliderName="ann" remove="true" backup="true"/>
    <!-- Backup glider bob -->
    <change gliderName="bob" remove="false" backup="true"/>
    <!-- Remove glider tom (no backup) -->
    <change gliderName="tom" remove="true" backup="false"/>
    <!-- Only available to hosting dockservers.  Transfer glider from this dockserver host to another -->
    <change gliderName="darwin" destinationHost="twr" />
    <change gliderName="nemesis" destinationHost="ooi" />
    <!-- Only available to hosting dockservers.  Add a new glider this this dockserver's managed gliders -->
    <change gliderName="ann" add="true" />
    <!-- Only available to hosting dockservers.  Restore a glider backup file to this dockserver's managed gliders -->
    <change gliderName="mycroft" restore="true" file="20121008T135421_mycroft.tar.gz" />
    '''

    # Class constants
    # XML representation
    CHANGE_TAG = 'change'
    GLIDER_NAME_ATTRIBUTE = 'gliderName'
    DESTINATION_HOST_ATTRIBUTE = 'destinationHost'
    ADD_ATTRIBUTE = 'add'
    REMOVE_ATTRIBUTE = 'remove'
    BACKUP_ATTRIBUTE = 'backup'
    RESTORE_ATTRIBUTE = 'restore'
    FILE_ATTRIBUTE = 'file'

    # Poasible change type
    ADD_GLIDER = 1
    MOVE_GLIDER = 2
    REMOVE_GLIDER = 3
    BACKUP_GLIDER = 4
    RESTORE_GLIDER = 5

    ###########################################################################
    # Create a hosting dockserver change request to move or add a glider.
    #
    # Arguments:
    #  change_type - (int)
    #  glider_name - (string)
    #  change_param - (string) This is destination host for move type change and
    #    glider backup filename for restore type change.
    #
    def __init__(self, change_type, glider_name, change_param = None):

        self.change_type = change_type
        self.glider_name = glider_name
        if self.change_type == GliderChange.MOVE_GLIDER:
            self.destination_host = change_param
        elif self.change_type == GliderChange.RESTORE_GLIDER:
            self.glider_backup_file = change_param

    ###########################################################################
    # Encode this dock change in XML as part of the passed XML document.
    # Arguments:
    #  packet_document - (xml.dom.Document) The XML document to use in creating
    #    the XML representation of this dock change.
    #
    # Returns:
    #  xml.dom.Element - The XML representation of this dock change for
    #    sending over the network to other GMC components (like dockserver).
    #
    def toXml(self, packet_document):
        change_element = packet_document.createElement(GliderChange.CHANGE_TAG)
        change_element.setAttribute(GliderChange.GLIDER_NAME_ATTRIBUTE, self.glider_name)
        if self.change_type == GliderChange.MOVE_GLIDER:
            change_element.setAttribute(GliderChange.DESTINATION_HOST_ATTRIBUTE, self.destination_host)
        elif self.change_type == GliderChange.ADD_GLIDER:
            change_element.setAttribute(GliderChange.ADD_ATTRIBUTE, TRUE)
        elif self.change_type == GliderChange.REMOVE_GLIDER:
            change_element.setAttribute(GliderChange.REMOVE_ATTRIBUTE, TRUE)
        elif self.change_type == GliderChange.BACKUP_GLIDER:
            change_element.setAttribute(GliderChange.BACKUP_ATTRIBUTE, TRUE)
        elif self.change_type == GliderChange.RESTORE_GLIDER:
            change_element.setAttribute(GliderChange.RESTORE_ATTRIBUTE, TRUE)
            change_element.setAttribute(GliderChange.FILE_ATTRIBUTE, self.glider_backup_file)

        return change_element

###############################################################################
class HostChanges(object):
    '''
    Attributes:
      host_changes_tag - (string) 'dockHostChanges' or 'dataHostChanges'
    '''
    # Class constants
    DOCK_HOST_CHANGES_TAG = 'dockHostChanges'
    DATA_HOST_CHANGES_TAG = 'dataHostChanges'

    ###########################################################################
    # Create a list of hosting dockserver or dataserver change requests.
    #
    # Arguments:
    #  host_changes_tag - (string) 'dockHostChanges' or 'dataHostChanges'
    #  change_list - (list) List of host changes - glider changes, or dockserver
    #    changes, or dataserver changes
    #
    def __init__(self, host_changes_tag):

        self.host_changes_tag = host_changes_tag
        self.change_list = []

    ###########################################################################
    def addChange(self, change):
        self.change_list.append(change)

    ###########################################################################
    # Transform this list of host changes to its XML representation.
    #
    # Returns:
    #  xml.dom.Element - The XML representation of this host changes list for
    #    sending over the network to other GMC components (like dockserver).
    #
    def toXml(self, xml_document):
        host_changes_element = xml_document.createElement(self.host_changes_tag)
        for change in self.change_list:
            change_element = change.toXml(xml_document)
            host_changes_element.appendChild(change_element)
        return host_changes_element

###############################################################################
class HostChange(object):
    '''
    Attributes:
      change_type - (string) Type of record to change in hostsState.xml

    Sample XML representation:
      <hostChange type='host' command='add' name='ooi' configFilename='ooi.xml' />
    '''
    # Class constants
    # XML representation
    HOST_CHANGE_TAG = 'hostChange'
    TYPE_ATTRIBUTE = 'type'
    COMMAND_ATTRIBUTE = 'command'
    NAME_ATTRIBUTE = 'name'

    # Commands
    ADD = 'add'
    DELETE = 'delete'

    # Types of dock host changes
    HOST_TYPE = 'host'
    CUSTOMER_TYPE = 'customer'
    USER_TYPE = 'user'
    MEMBER_TYPE = 'member'
    PRIVILEGE_TYPE = 'privilege'

    ###########################################################################
    # Create a hosting dockserver change request to add a new host or delete an
    # existing one.
    #
    # Arguments:
    #  change_type - (string) 'host', 'user', 'customer'
    #
    def __init__(self, change_type):

        self.change_type = change_type

    ###########################################################################
    # Encode this dock change in XML as part of the passed XML document.
    # Arguments:
    #  packet_document - (xml.dom.Document) The XML document to use in creating
    #    the XML representation of this dock change.
    #
    # Returns:
    #  xml.dom.Element - The XML representation of this dock change for
    #    sending over the network to other GMC components (like dockserver).
    #
    def toXml(self, packet_document):
        # Encode common portions of host changes
        host_change_element = packet_document.createElement(HostChange.HOST_CHANGE_TAG)
        host_change_element.setAttribute(HostChange.TYPE_ATTRIBUTE, self.change_type)
        return host_change_element

###############################################################################
class HostCustomerChange(HostChange):
    '''
    Purpose: Represent a host server customer change.  Add a new customer, delete
      an existing customer.

    Attributes:
      command - (string) function to perform - 'add' or 'delete'
      name - (string) perform function on this customer name

    Sample XML representation:
      <hostChange type='customer' command='add' name='ooi' />
    '''
    # Class constants
    # XML representation

    ###########################################################################
    # Create a hosting dockserver change request to add a new host or delete an
    # existing one.
    #
    # Arguments:
    #  change_type - (string) 'host', 'user', 'customer'
    #
    def __init__(self, command, name):

        super(HostCustomerChange, self).__init__(HostChange.CUSTOMER_TYPE)

        self.command = command
        self.name = name

    ###########################################################################
    # Encode this dock change in XML as part of the passed XML document.
    # Arguments:
    #  packet_document - (xml.dom.Document) The XML document to use in creating
    #    the XML representation of this dock change.
    #
    # Returns:
    #  xml.dom.Element - The XML representation of this dock change for
    #    sending over the network to other GMC components (like dockserver).
    #
    def toXml(self, packet_document):
        # Encode common portions of host changes
        host_change_element = super(HostCustomerChange, self).toXml(packet_document)

        host_change_element.setAttribute(HostChange.COMMAND_ATTRIBUTE, self.command)
        host_change_element.setAttribute(HostChange.NAME_ATTRIBUTE, self.name)

        return host_change_element

###############################################################################
class HostHostChange(HostChange):
    '''
    Purpose: Represent a host server host change.  Add a new host, delete
      an existing host.

    Attributes:
      command - (string) function to perform - 'add' or 'delete'
      name - (string) perform function on this host name
      filename - (string) For add new host, configuration filename

    Sample XML representation:
      <hostChange type='host' command='add' name='ooi' configFilename='ooi.xml' />
    '''
    # Class constants
    # XML representation
    CONFIG_FILENAME_ATTRIBUTE = 'configFilename'

    def __init__(self, command, name, filename = None):
        '''
        Create a hosting dockserver change request to add a new host or delete an
        existing one.

        Arguments:
          command - (string) 'add' or 'delete' the passed host
          name - (string) the host name to add or delete
          filename - (string) For add a host, use this name as the configuration filename.
        '''

        super(HostHostChange, self).__init__(HostChange.HOST_TYPE)

        self.command = command
        self.name = name
        self.filename = filename

    def toXml(self, packet_document):
        '''
        Encode this dock change in XML as part of the passed XML document.

        Arguments:
          packet_document - (xml.dom.Document) The XML document to use in creating
            the XML representation of this host change.

        Returns:
          xml.dom.Element - The XML representation of this dock change for
            sending over the network to other GMC components (like dockserver).
        '''
        # Encode common portions of host changes
        host_change_element = super(HostHostChange, self).toXml(packet_document)

        host_change_element.setAttribute(HostChange.COMMAND_ATTRIBUTE, self.command)
        host_change_element.setAttribute(HostChange.NAME_ATTRIBUTE, self.name)
        if self.command == HostChange.ADD:
            # If adding a new host, then check the filename
            if self.filename is None:
                # If adding a new host and no configuration filename passed, then create one from the host name.
                self.filename = self.name + '.xml'
            host_change_element.setAttribute(HostHostChange.CONFIG_FILENAME_ATTRIBUTE, self.filename)

        return host_change_element

###############################################################################
class HostUserChange(HostChange):
    '''
    Purpose: Represent a host server user change.  Add a new user, delete
      an existing user.

    Attributes:
      command - (string) function to perform - 'add' or 'delete'
      name - (string) perform function on this user name
      external_ID - (string) the passed user name as known outside the hostState.xml
        files.  For example, the phpbb ID corresponding to the passed user name.

    Sample XML representation:
      <hostChange type='user' command='add' name='rtrout' externalID='rtrout' />
    '''
    # Class constants
    # XML representation
    EXTERNAL_ID_ATTRIBUTE = 'externalID'

    def __init__(self, command, name, external_ID = None):
        '''
        Create a hosting dockserver change request to add a new user or delete an
        existing one.

        Arguments:
          command - (string) function to perform - 'add' or 'delete'
          name - (string) perform function on this user name
          external_ID - (string) the passed user name as known outside the hostState.xml
            files.  For example, the phpbb ID corresponding to the passed user name.
        '''
        super(HostUserChange, self).__init__(HostChange.USER_TYPE)

        self.command = command
        self.name = name
        self.external_ID = external_ID

    def toXml(self, packet_document):
        '''
        Encode this dock change in XML as part of the passed XML document.

        Arguments:
          packet_document - (xml.dom.Document) The XML document to use in creating
            the XML representation of this host change.

        Returns:
          xml.dom.Element - The XML representation of this dock change for
            sending over the network to other GMC components (like dockserver).
        '''
        # Encode common portions of host changes
        host_change_element = super(HostUserChange, self).toXml(packet_document)

        host_change_element.setAttribute(HostChange.COMMAND_ATTRIBUTE, self.command)
        host_change_element.setAttribute(HostChange.NAME_ATTRIBUTE, self.name)
        if self.command == HostChange.ADD:
            # If adding a new user, then check for passed external ID
            if self.external_ID is None:
                # If no external ID is passed, then use the user name as one
                self.external_ID = self.name
            host_change_element.setAttribute(HostUserChange.EXTERNAL_ID_ATTRIBUTE, self.external_ID)

        return host_change_element

###############################################################################
class HostMemberChange(HostChange):
    '''
    Purpose: Represent a host server member change.  Add a new member to a
      customer, delete an existing customer member.

    Attributes:
      command - (string) function to perform - 'add' or 'delete'
      name - (string) perform function on this user name
      customer_name - (string) Add / delete the passed user as a member of this
        customer.

    Sample XML representation:
      <hostChange type='member' command='add' name='rtrout' customerName='ooi' />
    '''
    # Class constants
    # XML representation
    CUSTOMER_NAME_ATTRIBUTE = 'customerName'

    def __init__(self, command, name, customer_name = None):
        '''
        Create a hosting dockserver change request to add a new user or delete an
        existing one.

        Arguments:
          command - (string) function to perform - 'add' or 'delete'
          name - (string) perform function on this user name
          customer_name - (string) Add / delete the passed user as a member of this
            customer.
        '''
        super(HostMemberChange, self).__init__(HostChange.MEMBER_TYPE)

        self.command = command
        self.name = name
        self.customer_name = customer_name

    def toXml(self, packet_document):
        '''
        Encode this dock change in XML as part of the passed XML document.

        Arguments:
          packet_document - (xml.dom.Document) The XML document to use in creating
            the XML representation of this host change.

        Returns:
          xml.dom.Element - The XML representation of this dock change for
            sending over the network to other GMC components (like dockserver).

        Exceptions:
          PacketError - if customer name or user name is missing.
        '''
        if self.customer_name is None or self.name is None:
            # If no customer or user name is passed, then error exit.
            raise PacketError(self, 'Missing customer or user name in host member change.')
        # Encode common portions of host changes
        host_change_element = super(HostMemberChange, self).toXml(packet_document)

        host_change_element.setAttribute(HostChange.COMMAND_ATTRIBUTE, self.command)
        host_change_element.setAttribute(HostChange.NAME_ATTRIBUTE, self.name)
        host_change_element.setAttribute(HostMemberChange.CUSTOMER_NAME_ATTRIBUTE, self.customer_name)

        return host_change_element

###############################################################################
class DockHostPrivilegeChange(HostChange):
    '''
    Purpose:  Represents a customer's privileges on a host dockserver.

    Attributes:
      customer_name - (string) customer whose privileges are represented
      host_name - (string) privileges apply to this host dockserver
      view_host - (True, False, None) grant / revoke / no change to view host.
      view_gliders - (True, False, None) grant / revoke / no change to view gliders of host dockserver.
      view_serial_ports - (True, False, None) grant / revoke / no change to view serial ports of host dockserver.

    Sample XML representation:
      <hostChange type='privilege' name='ooi' hostName='twr'
        viewHost='true' viewGliders='true' viewSerialPorts='false' />
    '''
    # Class constants
    # XML representation
    HOST_NAME_ATTRIBUTE = 'hostName'
    VIEW_HOST_ATTRIBUTE = 'viewHost'
    VIEW_GLIDERS_ATTRIBUTE = 'viewGliders'
    VIEW_SERIAL_PORTS_ATTRIBUTE = 'viewSerialPorts'

    def __init__(self, customer_name, host_name, view_host, view_gliders, view_serial_ports):
        '''
        Create a hosting dockserver change request to add a new host or delete an
        existing one.

        Arguments:
          customer_name - (string) customer whose privileges are represented
          host_name - (string) privileges apply to this host dockserver
          view_host - (True, False, None) grant / revoke / no change to view host.
          view_gliders - (True, False, None) grant / revoke / no change to view gliders of host dockserver.
          view_serial_ports - (True, False, None) grant / revoke / no change to view serial ports of host dockserver.
        '''
        super(DockHostPrivilegeChange, self).__init__(HostChange.PRIVILEGE_TYPE)

        self.customer_name = customer_name
        self.host_name = host_name
        self.view_host = view_host
        self.view_gliders = view_gliders
        self.view_serial_ports = view_serial_ports

    def toXml(self, packet_document):
        '''
        Encode this dock change in XML as part of the passed XML document.
        Arguments:
          packet_document - (xml.dom.Document) The XML document to use in creating
            the XML representation of this dock change.

        Returns:
          xml.dom.Element - The XML representation of this dock change for
            sending over the network to other GMC components (like dockserver).
        '''
        # Encode common portions of host change
        host_change_element = super(DockHostPrivilegeChange, self).toXml(packet_document)

        host_change_element.setAttribute(HostChange.NAME_ATTRIBUTE, self.customer_name)
        host_change_element.setAttribute(DockHostPrivilegeChange.HOST_NAME_ATTRIBUTE, self.host_name)
        if self.view_host is not None:
            host_change_element.setAttribute(DockHostPrivilegeChange.VIEW_HOST_ATTRIBUTE, str(self.view_host))
        if self.view_gliders is not None:
            host_change_element.setAttribute(DockHostPrivilegeChange.VIEW_GLIDERS_ATTRIBUTE, str(self.view_gliders))
        if self.view_serial_ports is not None:
            host_change_element.setAttribute(DockHostPrivilegeChange.VIEW_SERIAL_PORTS_ATTRIBUTE, str(self.view_serial_ports))

        return host_change_element

###############################################################################
class DataHostPrivilegeChange(HostChange):
    '''
    Purpose:  Represents a customer's privileges on a host dataserver.

    Attributes:
      customer_name - (string) customer whose privileges are represented
      host_name - (string) privileges apply to this host dataserver
      view_data - (True, False, None) grant / revoke / no change to view host data.

    Sample XML representation:
      <hostChange type='privilege' name='ooi' hostName='twr' viewData='true' />
    '''
    # Class constants
    # XML representation
    HOST_NAME_ATTRIBUTE = 'hostName'
    VIEW_DATA_ATTRIBUTE = 'viewData'

    def __init__(self, customer_name, host_name, view_data):
        '''
        Create a customer privilege change to grant / revoke viewing data on the passed host dataserver.

        Arguments:
          customer_name - (string) customer whose privileges are represented
          host_name - (string) privileges apply to this host dockserver
          view_data - (True, False, None) grant / revoke / no change to view host data.
        '''
        super(DataHostPrivilegeChange, self).__init__(HostChange.PRIVILEGE_TYPE)

        self.customer_name = customer_name
        self.host_name = host_name
        self.view_data = view_data

    def toXml(self, packet_document):
        '''
        Encode this dock change in XML as part of the passed XML document.
        Arguments:
          packet_document - (xml.dom.Document) The XML document to use in creating
            the XML representation of this dock change.

        Returns:
          xml.dom.Element - The XML representation of this dock change for
            sending over the network to other GMC components (like dockserver).
        '''
        # Encode common portions of host change
        host_change_element = super(DataHostPrivilegeChange, self).toXml(packet_document)

        host_change_element.setAttribute(HostChange.NAME_ATTRIBUTE, self.customer_name)
        host_change_element.setAttribute(DataHostPrivilegeChange.HOST_NAME_ATTRIBUTE, self.host_name)
        if self.view_data is not None:
            host_change_element.setAttribute(DataHostPrivilegeChange.VIEW_DATA_ATTRIBUTE, str(self.view_data))

        return host_change_element

class DockSubscriptionChange(object):
    '''
    Purpose:  Represents a user change request for dataserver host subscription to dockserver host data.

    Attributes:
      command - (string) Type of change request - ADD, DELETE, or MOD
      dataserver_host_name - (string) The dataserver host name of the local host whose dockserver host
        data subscription will be changed.
      dockserver_host_uri - (string) Identifies the dockserver host subscription to change on the passed
        dataserver host name.


    Sample XML representation:
      <dockSubscription command='add' hostName='twr'
        uri='//localhost:6564/twr'
        dataProxyUri='//datahost.webbresearch.com/default'
        include_pattern='darwin|nemsis'
        exclude_pattern='bensim' />
    '''
    # Class constants
    # Change command types
    ADD = 'add'
    DELETE = 'delete'
    MOD = 'mod'

    # XML representation
    DOCK_SUBSCRIPTION_TAG = 'dockSubscription'
    COMMAND_ATTRIBUTE = 'command'
    HOST_NAME_ATTRIBUTE = 'hostName'
    URI_ATTRIBUTE = 'uri'
    DATA_PROXY_URI_ATTRIBUTE = 'dataProxyUri'
    INCLUDE_PATTERN_ATTRIBUTE = 'includePattern'
    EXCLUDE_PATTERN_ATTRIBUTE = 'excludePattern'

    def __init__(self, command, dataserver_host_name, dockserver_host_uri):
        '''
        Create a dockserver subscription change to the passed, local dataserver host name.

        Arguments:
          command - (string) Type of change request - ADD, DELETE, or MOD
          dataserver_host_name - (string) The dataserver host name of the local host whose dockserver host
            data subscription will be changed.
          dockserver_host_uri - (string) Identifies the dockserver host subscription to change on the passed
            dataserver host name.
        '''

        self.command = command
        self.dataserver_host_name = dataserver_host_name
        self.dockserver_host_uri = dockserver_host_uri
        self.dataserver_proxy_uri = None
        self.include_pattern = None
        self.exclude_pattern = None

    def setDataHostProxyUri(self, dataserver_proxy_uri):
        '''
        Set the dataserver proxy URI to the passed value.

        Arguments:
          dataserver_proxy_uri - (string) The proxy dockserver will notify when new glider data is available.
            If empty string or None, dockserver will use the socket's source address for notification purposes.
        '''
        self.dataserver_proxy_uri = dataserver_proxy_uri

    def setPatterns(self, include_pattern, exclude_pattern):
        '''
        Set this subscription change's glider name include and exclude patterns.

        If the include_pattern is empty string or None, a pattern matching ALL glider names is used.

        If the exclude_pattern is empty string or None, a pattern matching NO glider names is used.

        A glider name that DOES NOT MATCH the exclude_pattern and DOES MATCH the include_pattern is added
        to the data subscirption.

        Arguments:
          include_pattern - (string) Regular expression that a subscription's glider name must MATCH to be included
            in the subscription.
          exclude_pattern = (string) Regular expression that a subscription's glider name must NOT MATCH to be included
            in the subscripton.
        '''

        self.include_pattern = include_pattern
        self.exclude_pattern = exclude_pattern

    def toXml(self, packet_document):
        '''
        Encode this dock change in XML as part of the passed XML document.
        Arguments:
          packet_document - (xml.dom.Document) The XML document to use in creating
            the XML representation of this dock change.

        Returns:
          xml.dom.Element - The XML representation of this dock change for
            sending over the network to other GMC components (like dockserver).

        Sample XML representation:
          <dockSubscription command='add' hostName='twr'
            uri='//localhost:6564/twr'
            dataProxyUri='//datahost.webbresearch.com/default'
            include_pattern='darwin|nemsis'
            exclude_pattern='bensim' />
        '''
        dock_subscription_element = packet_document.createElement(DockSubscriptionChange.DOCK_SUBSCRIPTION_TAG)
        dock_subscription_element.setAttribute(DockSubscriptionChange.COMMAND_ATTRIBUTE, self.command)
        dock_subscription_element.setAttribute(DockSubscriptionChange.HOST_NAME_ATTRIBUTE, self.dataserver_host_name)
        dock_subscription_element.setAttribute(DockSubscriptionChange.URI_ATTRIBUTE, self.dockserver_host_uri)

        if self.dataserver_proxy_uri is not None and not self.dataserver_proxy_uri == "":
            # If a dataserver proxy uri is provided, then encode it.
            dock_subscription_element.setAttribute(DockSubscriptionChange.DATA_PROXY_URI_ATTRIBUTE, self.dataserver_proxy_uri)
        if self.include_pattern is not None:
            # If an include pattern is provided, then encode it
            dock_subscription_element.setAttribute(DockSubscriptionChange.INCLUDE_PATTERN_ATTRIBUTE, self.include_pattern)
        if self.exclude_pattern is not None:
            # If an exclude pattern is provided, then encode it
            dock_subscription_element.setAttribute(DockSubscriptionChange.EXCLUDE_PATTERN_ATTRIBUTE, self.exclude_pattern)

        return dock_subscription_element

###############################################################################
class MissionAdjustment(object):
    '''
    Attributes:

    Sample XML representation:

    <maFiles gbf="abc.gbf">
      <maFile>surfac03.ma</maFile>
      <maFile>goto_l03.ma</maFile>
      <maFile>yo03.ma</maFile>
      <maFile>sample03.ma</maFile>
      <file name="surfac03">File content appears here</file>
      <file name="goto_l03.ma">File content appears here</file>
      <file name="yo03.ma">File content appears here</file>
      <file name="sample03.ma">File content appears here</file>
      <file name="abc.gbf">File content appears here</file>
    </maFiles>
    '''
    # Class Constants
    MA_FILES_TAG = 'maFiles'

    # Class MissionAdjustment
    def __init__(self, gbf_filename = None, ma_filenames = None, ma_files_element = None):

        self.gbf_filename = gbf_filename
        if ma_filenames == None:
            ma_filenames = []
        self.ma_filenames = ma_filenames
        self.mission_files = {}
        if ma_files_element:
            self.decode(ma_files_element)

    def isEmpty(self):
        return len(self.ma_filenames) == 0

    def setGBFFilename(self, gbf_filename):
        self.gbf_filename = gbf_filename

    def getGBFFilename(self):
        return self.gbf_filename

    def addMaFilename(self, ma_filename):
        self.ma_filenames.append(ma_filename)

    def setMaFilenames(self, ma_filenames):
        self.ma_filesnames = ma_filenames

    def getMaFilenames(self):
        return self.ma_filenames

    def addFile(self, name, content):
        self.mission_files[name] = content

    # Return a dictionary where keys are mission filesnames and values are file
    # contents.
    def getFiles(self):
        return self.mission_files

    def decode(self, ma_files_element):
        # Decode any ma files
        if ma_files_element.hasAttribute(Packet.GBF_ATTRIBUTE):
            self.gbf_filename = ma_files_element.getAttribute(Packet.GBF_ATTRIBUTE)
        ma_file_element_nodelist = ma_files_element.getElementsByTagName(Packet.MA_FILE_TAG)
        for ma_file_element in ma_file_element_nodelist:
            self.ma_filenames.append(ma_file_element.firstChild.data)

        # Decode any mission file content
        file_element_nodelist = ma_files_element.getElementsByTagName(Packet.FILE_TAG)
        for file_element in file_element_nodelist:
            file_name = file_element.getAttribute(Packet.NAME_ATTRIBUTE)
            self.mission_files[file_name] = file_element.firstChild.data

    def toXml(self, packet_document):
        ma_files_element = packet_document.createElement(MissionAdjustment.MA_FILES_TAG)
        if self.gbf_filename is not None:
            ma_files_element.setAttribute(Packet.GBF_ATTRIBUTE, self.gbf_filename)
        for ma_filename in self.ma_filenames:
            ma_filename_textnode = packet_document.createTextNode(ma_filename)
            ma_file_element = packet_document.createElement(Packet.MA_FILE_TAG)
            ma_file_element.appendChild(ma_filename_textnode)
            ma_files_element.appendChild(ma_file_element)

        # Encode mission files and their content - if any are provided.
        for file_name, file_content in self.mission_files.iteritems():
            file_content_textnode = packet_document.createTextNode(file_content)
            file_element = packet_document.createElement(Packet.FILE_TAG)
            file_element.setAttribute(Packet.NAME_ATTRIBUTE, file_name)
            file_element.appendChild(file_content_textnode)
            ma_files_element.appendChild(file_element)

        return ma_files_element

    def __str__(self):
        mission_string = 'GBF: '
        if self.gbf_filename:
            mission_string = mission_string + self.gbf_filename
        else:
            mission_string = mission_string + 'None'
        mission_string = mission_string + ' MA files: '
        for ma_filename in self.ma_filenames:
            mission_string = mission_string + ' ' + ma_filename
        return mission_string

###############################################################################
class GliderAbort(object):
    '''
    Attributes:

    '''
    # Class Constants
    DATE_TIME_FORMAT = '%Y-%m-%dT%H:%M:%S' #  "2008-05-08T14:40:10 +0000"
    IN_ABORT_DATE_TIME_FORMAT = '%a %b %d %H:%M:%S %Y' #"Tue May 17 02:30:21 2011"

    # Class GliderAbort
    def __init__(self, last_abort_element):
        #print last_abort_element.toxml()
        # Convert the pass xml element to its class representation
        self.reset = last_abort_element.getAttribute(Packet.RESET_ATTRIBUTE)
        self.cause = last_abort_element.getAttribute(Packet.CAUSE_ATTRIBUTE)
        date_time_string = last_abort_element.getAttribute(Packet.DATE_TIME_ATTRIBUTE)[0:-6]
        #print 'In GliderAbort.init...' + date_time_string
        self.date_time = datetime.datetime.strptime(date_time_string, GliderAbort.DATE_TIME_FORMAT)
        self.segment = last_abort_element.getAttribute(Packet.SEGMENT_ATTRIBUTE)
        self.missionName = last_abort_element.getAttribute(Packet.MISSION_ATTRIBUTE)

    def __str__(self):
        return 'Reset: ' + self.reset + ' Cause: ' + self.cause + ' DateTime: ' + self.date_time.strftime(GliderAbort.DATE_TIME_FORMAT) + \
            ' Segment: ' + self.segment + ' Mission Name: ' + self.missionName

    def getStatusString(self):
        return 'Name: ' + self.missionName + '  Number: ' + self.segment  + '\n'+ \
             'Cause: ' + self.cause + '  Time: ' + self.date_time.strftime(GliderAbort.IN_ABORT_DATE_TIME_FORMAT) + '  Reset: ' + self.reset

    def getReset(self):
        return self.reset

    def getCause(self):
        return self.cause

    def getDateTime(self):
        return self.date_time

    def getSegment(self):
        return self.segment

    def getMissionName(self):
        return self.missionName

###############################################################################
class GliderMissionStatus(object):
    '''
    Attributes:

    '''
    # Class Constants
    START_MISSION_DATE_TIME_FORMAT = '%d %b %Y %H:%M:%S Z' # "16 May 2011 19:23:01 Z"
    IN_MISSION_DATE_TIME_FORMAT = '%a %b %d %H:%M:%S %Y' #"Tue May 17 02:30:21 2011"
    # Class GliderMissionStatus
    def __init__(self, last_mission_element):
        #print last_mission_element.toprettyxml()
        self.mission_name = last_mission_element.getAttribute(Packet.MISSION_NAME_ATTRIBUTE)
        self.mission_number = last_mission_element.getAttribute(Packet.MISSION_NUMBER_ATTRIBUTE)
        self.because = last_mission_element.getAttribute(Packet.BECAUSE_ATTRIBUTE)
        date_time_string = last_mission_element.getAttribute(Packet.DATE_TIME_ATTRIBUTE)
        #print 'In GliderMissionStatus.init...' + date_time_string
        if date_time_string[-1] == 'Z':
            # Must be start of mission time
            self.date_time = datetime.datetime.strptime(date_time_string, GliderMissionStatus.START_MISSION_DATE_TIME_FORMAT)
        else:
            # Must be in mission time
            self.date_time = datetime.datetime.strptime(date_time_string, GliderMissionStatus.IN_MISSION_DATE_TIME_FORMAT)
        self.mission_time = last_mission_element.getAttribute(Packet.MISSION_TIME_ATTRIBUTE)

    def __str__(self):
        return 'Name: ' + self.mission_name + ' Number: ' + self.mission_number + ' Because: ' + \
            self.because + ' DateTime: ' + self.date_time.strftime(GliderMissionStatus.IN_MISSION_DATE_TIME_FORMAT) + ' Mission Time: ' + self.mission_time

    def getStatusString(self):
        return 'Name: ' + self.mission_name + '  Number: ' + self.mission_number  + '\n'+ \
        'Cause: ' + self.because + '\n' + \
        'Time: ' + self.date_time.strftime(GliderMissionStatus.IN_MISSION_DATE_TIME_FORMAT) +  '  Mission Time: ' + self.mission_time + ' sec'

    def getMissionName(self):
        return self.mission_name

    def getMissionNumber(self):
        return self.mission_number

    def getBecause(self):
        return self.because

    def getDateTime(self):
        return self.date_time

    def getMissionTime(self):
        return self.mission_time

###############################################################################
class GliderPosition(object):
    '''
    Attributes:
    <report>
      <location>
        <lat>2121.248</lat>
        <lon>-15756.557</lon>
      </location>
      <time>2006-06-21T23:18:37-04:30</time>
    </report>

    '''
    # Class Constants
    STATUS_PACKET_DATE_TIME_FORMAT = '%a %b %d %H:%M:%S %Y' # "Fri Feb 18 19:33:48 2011"
    WRITE_PACKET_DATE_TIME_FORMAT = '%Y-%m-%dT%H:%M:%S' # "2006-06-21T23:18:37-04:00"
    # Class GliderPosition
    def __init__(self, position_element):
        #print 'In packet.GliderPosition... '
        #print position_element.toprettyxml()
        if position_element.tagName == Packet.POSITION_TAG:
            # This position element came from a glider status packet.
            self.latitude = position_element.getAttribute(Packet.LATITUDE_ATTRIBUTE)
            self.longitude= position_element.getAttribute(Packet.LONGITUDE_ATTRIBUTE)
            date_time_string = position_element.getAttribute(Packet.DATE_TIME_ATTRIBUTE)
            #print 'In GliderPosition.init...Glider status packet: ' + date_time_string
            self.date_time = datetime.datetime.strptime(date_time_string, GliderPosition.STATUS_PACKET_DATE_TIME_FORMAT)
            return
        if position_element.tagName == Packet.REPORT_TAG:
            # This position element came from a WriteTrack packet
            lat_element = position_element.getElementsByTagName(Packet.LAT_TAG).item(0)
            self.latitude = lat_element.firstChild.data
            lon_element = position_element.getElementsByTagName(Packet.LON_TAG).item(0)
            self.longitude = lon_element.firstChild.data
            time_element = position_element.getElementsByTagName(Packet.TIME_TAG).item(0)
            date_time_string = time_element.firstChild.data
            #print 'In GliderPosition.init...WriteTrack packet: ' + date_time_string
            # python 2.5 doesn't have a date format code to handle '-04:00' UTC offset.
            # So we do a little date math to accomidate.
            # Parse the 'local' portion of the date.
            date_local = datetime.datetime.strptime(date_time_string[0:-6], GliderPosition.WRITE_PACKET_DATE_TIME_FORMAT)
            # Parse the UTC offset portion of the date
            utc_delta_hours = int(date_time_string[-5:-3])
            utc_delta_minutes = int(date_time_string[-2:len(date_time_string)])
            # Do a little date math.
            utc_delta = datetime.timedelta(hours=utc_delta_hours, minutes=utc_delta_minutes)
            if date_time_string[-6] == '+':
                self.date_time = date_local - utc_delta
            else:
                self.date_time = date_local + utc_delta

    def __str__(self):
        return self.latitude + ' N ' + self.longitude + ' E  ' + \
            self.date_time.strftime(GliderPosition.STATUS_PACKET_DATE_TIME_FORMAT)

    def getStatusString(self):
        return 'Time: ' + self.date_time.strftime(GliderPosition.STATUS_PACKET_DATE_TIME_FORMAT) + \
            '  Lat: '+ self.latitude + ' N' + '  Lon: ' + self.longitude + ' E'

    def getLatitude(self):
        return self.latitude

    def getLongitude(self):
        return self.longitude

    def getDateTime(self):
        return self.date_time

###############################################################################
class EventLogEntry(object):
    '''
    Attributes:
        date_time - (string)
        glider_name - (string)
        event - (string)
        authority - (string)
        note - (string)
        log_entry_element - (Element)

    XML Representation:
    <logEntry
      id="1234"
      dateTime="2008-05-08T14:40:10 +0000"
      nextInThreadPage="20111113"
      nextInThreadID="12345"
      previousInThreadPage=""
      previousInThreadID=""
      gliderName="ann"
      event="Glider Surfaced | Staged Mission | Staged Adjustment | Files to Glider | Files from Glider | Annotation"
      authority= "dockserver | <USERNAME>"
    >
      <!-- Subclass inserts event specific information here.
        -->
    </logEntry>
    '''
    # Class Constants
    # XML tags and attributes
    EVENT_ATTRIBUTE = 'event'
    DATE_TIME_ATTRIBUTE = 'dateTime'
    AUTHORITY_ATTRIBUTE = 'authority'
    GLIDER_NAME_ATTRIBUTE = 'gliderName'

    # Event attribute values
    GLIDER_SURFACED = 'Glider Surfaced'
    ANNOTATION = 'Annotation'
    STAGED_MISSION = 'Staged Mission'
    STAGED_ADJUSTMENT = 'Staged Adjustment'
    FILES_TO_GLIDER = 'Files to Glider'
    FILES_FROM_GLIDER = 'Files from Glider'

    # XML tags and attribute names for log entries
    LOG_ENTRY_TAG = 'logEntry'

    EVENT_LOG_ENTRY_DATE_TIME_FORMAT = '%Y-%m-%dT%H:%M:%S +0000' # "2006-06-21T23:18:37"
    EVENT_LOG_PAGE_ID_FORMAT = '%Y%m%d'

    # Class EventLogEntry
    def __init__(self, log_entry_element):

        self.note = None
        self.log_entry_element = log_entry_element
        if log_entry_element:
            # If "receiving" an event log entry, then decode data common to all event log entires
            self.date_time = log_entry_element.getAttribute(self.DATE_TIME_ATTRIBUTE)
            self.glider_name = log_entry_element.getAttribute(self.GLIDER_NAME_ATTRIBUTE)
            self.event = log_entry_element.getAttribute(self.EVENT_ATTRIBUTE)
            self.authority = log_entry_element.getAttribute(self.AUTHORITY_ATTRIBUTE)
        else:
            # If "sending" or just initializing an event log entry, then initialize
            # with default values
            self.date_time = datetime.datetime.utcnow().strftime(self.EVENT_LOG_ENTRY_DATE_TIME_FORMAT)
            self.glider_name = None
            self.event = None
            self.authority = None

    def getPageID(self):
        return self.date_time[0:4] + self.date_time[5:7] + self.date_time[8:10]

    def getDateTime(self):
        return self.date_time

    def getEvent(self):
        return self.event

    def getEventDescription(self):
        return self.getEvent()

    def getAuthority(self):
        return self.authority

    def getGliderName(self):
        return self.glider_name

    def getNote(self):
        return self.note

    def toXml(self, document):
        # Create the xml common to all event log entries
        self.log_entry_element = document.createElement(self.LOG_ENTRY_TAG)
        self.log_entry_element.setAttribute(self.DATE_TIME_ATTRIBUTE, self.date_time)
        self.log_entry_element.setAttribute(self.EVENT_ATTRIBUTE, self.event)
        self.log_entry_element.setAttribute(self.AUTHORITY_ATTRIBUTE, self.authority)
        if self.glider_name:
            self.log_entry_element.setAttribute(self.GLIDER_NAME_ATTRIBUTE, self.glider_name)

###############################################################################
class GliderSurfacedEntry(EventLogEntry):
    '''
    Attributes:
        mission - (GliderMissionStatus)
        position - (GliderPosition)

    XML Representation:
    <logEntry
      id="1234"
      dateTime="2008-05-08T14:40:10 +0000"
      nextInThreadPage="20111113"
      nextInThreadID="12345"
      previousInThreadPage=""
      previousInThreadID=""
      gliderName="ann"
      event="Glider Surfaced"
      authority= "dockserver"
    >
      <!-- The following content is used to create the text Note for the event.
        -->
      <lastMission
        missionName="GY10V001.MI"
        missionNumber="sim-041-2008-044-0-79 (0000.0079)"
        because="Hit a waypoint [behavior surface_5 start_when = 8.0]"
        dateTime="Fri Feb 18 19:33:48 2011"
        missionTime="20496">
      </lastMission>
      <position
        latitude="4136.981"
        longitude="-7032.128"
        dateTime="Fri Feb 18 19:33:48 2011">
      </position>
    </logEntry>
    '''
    # Class Constants
    EVENT_DESCRIPTION = 'Glider at Surface'

    # Class GliderPosition
    def __init__(self, log_entry_element):
        super(GliderSurfacedEntry, self).__init__(log_entry_element)

        if log_entry_element:
            # If "receiving" an event log entry, then decode it
            self.decode(log_entry_element)

    def decode(self, log_entry_element):
        #print log_entry_element.toprettyxml()
        # Decode last known mission
        last_mission_element = log_entry_element.getElementsByTagName(Packet.LAST_MISSION_TAG).item(0)
        if last_mission_element:
            # If a last mission element appears in the packet, then transform it to its class representation
            mission_name = last_mission_element.getAttribute(Packet.MISSION_NAME_ATTRIBUTE)
            if mission_name != 'unavailable':
                # If the last mission is known, then transform it to its class representation.
                self.mission = GliderMissionStatus(last_mission_element)
            else:
                self.mission = None
        else:
            self.mission = None

        # Decode glider position.
        position_element = log_entry_element.getElementsByTagName(Packet.POSITION_TAG).item(0)
        if position_element:
            # If a position element appears in the packet, then check for lat and lon
            if position_element.hasAttributes():
                try:
                    self.position = GliderPosition(position_element)
                except ValueError:
                    # If error decoding sent position, then ignore it and move on.  We'll get the next one.
                    self.position = None
            else:
                self.position = None
        else:
            self.position = None

        # Initialize note
        self.note = self.mission.getMissionName() + " : " + self.mission.getMissionNumber() \
            + "\n" + self.mission.getBecause() \
            + "\n" + str(self.position)

    def getEventDescription(self):
        return self.EVENT_DESCRIPTION

    def getMission(self):
        return self.mission

    def getPosition(self):
        return self.position

###############################################################################
class AnnotationEntry(EventLogEntry):
    '''
    Attributes:

    XML Representation:
    <logEntry
      id="1234"
      dateTime="2008-05-08T14:40:10 +0000"
      nextInThreadPage="20111113"
      nextInThreadID="12345"
      previousInThreadPage=""
      previousInThreadID=""
      gliderName="ann"
      event="Annotation"
      authority= "<USERNAME>"
    >
      <!-- The following content is used to create the text Note for the event.
        -->
      <note>MANUALLY ENTERED TEXT GOES HERE.
        AND HERE....
        EVEN MORE HERE....
        DOESN'T THIS GUY EVER SHUT UP?!
      </note>
    </logEntry>
    '''
    # Class Constants
    NOTE_TAG = 'note'

    # Class AnnotationEntry
    def __init__(self, log_entry_element = None):
        super(AnnotationEntry, self).__init__(log_entry_element)

        if log_entry_element:
            # If "receiving" an log entry element, then decode it
            self.decode(log_entry_element)
        else:
            # If no xml element passed, then initialize with defaults
            self.note = ""
            self.event = self.ANNOTATION
            self.authority = getpass.getuser()

    def decode(self, log_entry_element):
        note_element = log_entry_element.getElementsByTagName(self.NOTE_TAG).item(0)
        self.note = note_element.firstChild.data

    def setNote(self, note):
        self.note = note

    def setGliderName(self, name):
        self.glider_name = name

    def toXml(self, document):
        # Create the xml common to all event log entries
        EventLogEntry.toXml(self, document)
        # Add the xml unique to this event log entry
        note_element = document.createElement(self.NOTE_TAG)
        note_text_node = document.createTextNode(self.note)
        note_element.appendChild(note_text_node)
        self.log_entry_element.appendChild(note_element)
        return self.log_entry_element

###############################################################################
# A glider log book entry that represents a staged mission on a dockserver
# for a specific glider.
#
class StagedMissionEntry(EventLogEntry):
    '''
    Attributes:
        mission_sequence - (MissionSequence) A sequence of missions staged
          on a docksever to be run on a specific glider.

    XML Representation:
    <logEntry
      id="1234"
      dateTime="2008-05-08T14:40:10 +0000"
      nextInThreadPage="20111113"
      nextInThreadID="12345"
      previousInThreadPage=""
      previousInThreadID=""
      gliderName="ann"
      event="Staged Mission"
      authority= "dockserver"
    >
      <!-- The following content is used to create the text Note for the event.
        -->
      <missionSequence>
        <mission filename="foo.mi" repetitions=1>
          <maFile>surfac01.ma</maFile>
          <maFile>goto_l01.ma</maFile>
          <maFile>yo01.ma</maFile>
          <maFile>sample01.ma</maFile>
        </mission>
        <mission filename="bar.mi" repetitions=3>
          <maFile>surfac02.ma</maFile>
          <maFile>goto_l02.ma</maFile>
          <maFile>yo02.ma</maFile>
          <maFile>sample02.ma</maFile>
        </mission>
        <mission filename="third.mi">
          <maFile>surfac03.ma</maFile>
          <maFile>goto_l03.ma</maFile>
          <maFile>yo03.ma</maFile>
          <maFile>sample03.ma</maFile>
        </mission>
      </missionSequence>
    </logEntry>
    '''
    # Class Constants

    ###########################################################################
    # Create a staged mission log book entry and initial is from the passed
    # XML element representing a staged mission log entry.
    #
    # Arguments:
    #  log_entry_element - (xml.dom.Element) XML representation of a staged
    #    mission sequence log entry.  Received from dockserver.
    #
    def __init__(self, log_entry_element):
        super(StagedMissionEntry, self).__init__(log_entry_element)

        self.mission_sequence = None

        if log_entry_element:
            # If "receiving" an event log entry, then decode it
            self.decode(log_entry_element)

    ###########################################################################
    # Transform the passed staged mission log entry XML into its class representation.
    #
    # Arguments:
    #  log_entry_element - (xml.dom.Element) XML entry to transform.
    #
    def decode(self, log_entry_element):
        #print log_entry_element.toprettyxml()
        mission_sequence_element = log_entry_element.getElementsByTagName(MissionSequence.MISSION_SEQUENCE_TAG).item(0)
        if mission_sequence_element:
            # If a mission sequence element appears, then decode it
            self.mission_sequence = MissionSequence(mission_sequence_element = mission_sequence_element)

        # Initialize note
        self.note = self.mission_sequence.getNote()

###############################################################################
# A log entry that describes a staged mission adjustment made by the user to
# a running mission on a glider.
#
class StagedAdjustmentEntry(EventLogEntry):
    '''
    Attributes:
        mission_adjustment - (MissionAdjustment) Glider mission adjustment that
          has been staged on the dockserver for a partiular glider.  Includes all
          filenames (ma) and their content associated with this adjustment.

    XML Representation:
    <logEntry
      id="1234"
      dateTime="2008-05-08T14:40:10 +0000"
      nextInThreadPage="20111113"
      nextInThreadID="12345"
      previousInThreadPage=""
      previousInThreadID=""
      gliderName="ann"
      event="Staged Adjustment"
      authority= "dockserver"
    >
      <!-- The following content is used to create the text Note for the event.
           NOTE: If event is STAGED_ADJUSTMENT and this element is absent, then it's a cancel mission adjustment
           log entry.
        -->
      <maFiles>
        <!-- All ma files of the adjustment -->
        <maFile>goto_l01.ma</maFile>
        <maFile>goto_l03.ma</maFile>
        <maFile>sample03.ma</maFile>
      </maFiles>
    </logEntry>
    '''
    # Class Constants

    ###########################################################################
    # Create and initialize a staged adjustment log book entry from the passed
    # XML log entry element.
    #
    # Arguments:
    #  log_entry_element - (xml.dom.Element) XML representation of a glider
    #    mission adjustment.  Typcially from packets sent by dockserver.
    #
    def __init__(self, log_entry_element):
        super(StagedAdjustmentEntry, self).__init__(log_entry_element)

        self.mission_adjustment = None

        if log_entry_element:
            # If "receiving" an event log entry, then decode it
            self.decode(log_entry_element)

    ###########################################################################
    # Transforms the passed XML representation of a staged adjustment log entry
    # to its class representation.
    #
    # Arguments:
    #  log_entry_element - (xml.dom.Element) XML log entry to transform.
    #
    def decode(self, log_entry_element):
        #print log_entry_element.toprettyxml()
        ma_files_element = log_entry_element.getElementsByTagName(MissionAdjustment.MA_FILES_TAG).item(0)
        self.mission_adjustment = MissionAdjustment(ma_files_element = ma_files_element)
        if self.mission_adjustment.isEmpty():
            self.note = 'Mission Adjustment Cancelled'
        else:
            self.note = str(self.mission_adjustment)

###############################################################################
# Represents a file transfer entry in the log book between glider and dockserver.
# File transfer data comes as a packet from dockserver.
#
class FilesTransferredEntry(EventLogEntry):
    '''
    Attributes:
        files_transferred - (list) List of filenames transferred between the glider
          and dockserver.

    XML Representation:
    <logEntry
      id="1234"
      dateTime="2008-05-08T14:40:10 +0000"
      nextInThreadPage="20111113"
      nextInThreadID="12345"
      previousInThreadPage=""
      previousInThreadID=""
      gliderName="ann"
      event="Files to Glider | Files from Glider"
      authority= "dockserver"
    >
      <!-- For FILES_TO_GLIDER and FILES_FROM_GLIDER events,
           the following content is used to create the text Note for the event.
        -->
      <filesTransferred>
        <!-- Files transferred by zmodem to/from the glider and dockserver.
          -->
        <file>goto_l01.ma</file>
        <file>...</file>
        <file>...</file>
        <file>...</file>
      </filesTransferred>
    </logEntry>
    '''
    # Class Constants
    # XML tags and attribute names
    FILE_TAG = 'file'

    ###########################################################################
    # Transforms the passed glider log book files transferred entry as an XML
    # element into its class representation.
    #
    # Arguments:
    #  log_entry_element - (xml.dom.Element) XML of a files transferred log entry
    #    received in a dockserver packet.
    #
    def __init__(self, log_entry_element):
        super(FilesTransferredEntry, self).__init__(log_entry_element)

        self.files_transferred = []

        if log_entry_element:
            # If "receiving" an event log entry, then decode it
            self.decode(log_entry_element)

    ###########################################################################
    # Transforms the passed XML log entry representation into its class representation.
    #
    # Arguments:
    #  log_entry_element - (xml.dom.Element) XML log entry to transform.
    #
    def decode(self, log_entry_element):
        #print log_entry_element.toprettyxml()
        file_node_list = log_entry_element.getElementsByTagName(self.FILE_TAG)
        for file_element in file_node_list:
            # decode filenames
            file_name = file_element.firstChild.data
            self.files_transferred.append(file_name)

        # Initialize note
        self.note = ''
        for file_name in self.files_transferred:
            self.note = self.note + file_name + ' '

###############################################################################
# Represents a collection of glider sensor values captured from a single glider
# surface dialog (config.srf).
#
class GliderSurfaceSensors(object):
    '''
    Attributes:
        base_time - (string) Base time in the format: "Nov 16 14:43:36 2011".
          All sensors sampled at this time minus each ones seconds ago value.
        sensors - (dict) Keys are sensor names as string; values are corresponding
          instances of GliderSensor.

    XML Representation:

    <sensors dateTime="Nov 16 14:43:36 2011">
      <sensor name="m_battery" value="13.1215" unit="volts" secondsAgo="921.541"/>
      <sensor name="m_vacuum" value="6.5018" unit="inHg" secondsAgo="17440.7"/>
      <sensor name="m_strobe_ctrl" value="0" unit="bool" secondsAgo="422845"/>
    </sensors>
    '''
    # Class Constants
    # XML tags and attribute names
    DATE_TIME_ATTRIBUTE = "dateTime"
    SENSOR_TAG = "sensor"

    ###########################################################################
    # Transforms the XML representation of sensor collections sent in packet
    # communications with dockserver into their class representations.
    #
    # Arguments:
    #  sensors_element - (xml.dom.Element) XML representation of a sensor collection
    #    used in packet communications with dockserver.
    #
    def __init__(self, sensors_element):
        self.sensors = {}
        self.base_time = sensors_element.getAttribute(self.DATE_TIME_ATTRIBUTE)
        sensor_nodelist = sensors_element.getElementsByTagName(self.SENSOR_TAG)
        for sensor_element in sensor_nodelist:
            sensor = GliderSensor(sensor_element = sensor_element)
            self.sensors[sensor.getName()] = sensor

    ###########################################################################
    # Returns the base time formatted as "Nov 16 14:43:36 2011".
    #
    # Returns:
    #  string - base time from which all sensor sample times are derived.
    #
    def getBaseTime(self):
        return self.base_time

    ###########################################################################
    # Returns a dictionary of sensors data from a single glider surface dialog.
    #
    # Returns:
    #  dictionary - Keys are sensor names as string; values are GliderSensor
    #    instances.
    #
    def getSensors(self):
        return self.sensors

    ###########################################################################
    # Returns a string representation of this sensor collection.
    #
    # Returns:
    #  string - all sensor information formatted as a string for status display.
    #
    def getStatusString(self):
        sensorString = self.getBaseTime()+'\n'
        for sensor_name, sensor in self.getSensors().iteritems():
            sensorString += sensor.statusString()
            sensorString += '\n'
        return sensorString

###############################################################################
# Represents a glider sensor as displayed in a glider's surface dialog
# (sensors defined in config.srf).
#
class GliderSensor(object):
    '''
    Attributes:
        name - (string) Glider sensor name as it appears in masterdata.
        value - (float) Value of sensor at base time + seconds ago
        unit - (string) Engineering units of sensor value.
        seconds_ago - (float) Age of sensor sample.

    XML Representation:

    <sensor name="m_battery" value="13.1215" unit="volts" secondsAgo="921.541"/>
    '''
    # Class Constants
    # XML tags and attributes
    NAME_ATTRIBUTE = "name"
    VALUE_ATTRIBUTE = "value"
    UNIT_ATTRIBUTE = "unit"
    SECONDS_AGO_ATTRIBUTE = "secondsAgo"

    ###########################################################################
    # Create and initialize a new GliderSensor instance from the passed arguments.
    # If sensor_element is not None, it overrides all other arguments.
    #
    # Arguments:
    #  name - (string) Sensor name as it appears in masterdata.
    #  unit - (string) Engineering units of sensor values.
    #  value - (string) Sensor value from base time minus seconds ago.
    #  sensor_element - (xml.dom.Element) XML representation of a sensor's name,
    #    unit, value, and seconds ago.
    #
    def __init__(self, name = None, unit = None, value = None, sensor_element = None):

        if sensor_element:
            # If an XML sensor element has been passed, then take all sensor information
            # from the element
            self.name = sensor_element.getAttribute(self.NAME_ATTRIBUTE)
            self.value = float(sensor_element.getAttribute(self.VALUE_ATTRIBUTE))
            self.unit = sensor_element.getAttribute(self.UNIT_ATTRIBUTE)
            self.seconds_ago = float(sensor_element.getAttribute(self.SECONDS_AGO_ATTRIBUTE))
        else:
            # If no XML sensor element passed, then take sensor information from passed
            # parameters
            self.name = name
            self.value = float(value)
            self.unit = unit
            self.seconds_ago = 0.0

    ###########################################################################
    # Returns this sensor's name as it appears in masterdata.
    #
    # Returns:
    #  string - sensor name.
    #
    def getName(self):
        return self.name

    ###########################################################################
    # Returns the value of this sensor sampled at base time - seconds ago.
    #
    # Returns:
    #  float - this sensor's value
    #
    def getValue(self):
        return self.value

    ###########################################################################
    # Returns this sensor's engineering units.
    #
    # Returns:
    #  string - this sensor's engineering units
    #
    def getUnit(self):
        return self.unit

    ###########################################################################
    # Returns the number of seconds since base time that this sensor value as
    # sampled.
    #
    # Returns:
    #  int - seconds since base time that this sensor value was produced.
    #
    def getSecondsAgo(self):
        return self.seconds_ago

    ###########################################################################
    # Returns a string description of this sensor.
    #
    # Returns:
    #  string - Human readable representation of this sensor.
    #
    def __str__(self):
        return 'Sensor: ' + self.getName() + ' Value: ' + str(self.getValue()) + ' Unit: ' + self.getUnit() + ' Secs ago: ' + str(self.getSecondsAgo())

    ###########################################################################
    #  Returns this sensor formmatted for status output.
    #
    # Returns:
    #  string - this sensor formatted for status output.
    #
    def statusString(self):
        return self.getName()+'('+self.getUnit()+') = ' + str(self.getValue()) + ' Secs ago: ' + str(self.getSecondsAgo())

    ###########################################################################
    # Returns this sensor formatted for GUI choice selection.
    #
    # Returns:
    #  string - this sensor formatted for GUI choice selection.
    #
    def choiceString(self):
        return self.getName()+'('+self.getUnit()+') ' + str(self.getValue())

class GliderLinkProxy(object):
    '''
    Purpose: Proxy class for a glider link on a dockserver.  Glider links represent the
      combination of a glider, serial port, and communication device (freewave, modem)

    Attributes:
      glider_name - (string) Glider name as known on dockserver
      port - (string) Serial port identifier on the dockserver (eg., ttyUSB0)
      device - (string) Physical device used to comunicate with the glider
        (eg., freewave or modem).
      is_docked - (bool) True if the glider is in communication with the dockserver over this serial port.
    '''

    # Class constants
    # xml tags and attribute names
    GLIDER_NAME_ATTRIBUTE = 'gliderName'
    PORT_ATTRIBUTE = 'port'
    DEVICE_ATTRIBUTE = 'device'
    STATUS_ATTRIBUTE = 'status'

    DOCKED = '1'

    def __init__(self, glider_link_element = None):
        '''
        Purpose: Transform the passed XML representation of a glider link into its class representation.
        '''
        self.glider_name = None
        self.port = None
        self.device = None
        self.is_docked = False
        if glider_link_element:
            self.decode(glider_link_element)

    def decode(self, glider_link_element):
        '''
        Purpose: Extract the data from the xml representation
        '''
        if(glider_link_element.hasAttribute(GliderLinkProxy.GLIDER_NAME_ATTRIBUTE)):
            self.glider_name = glider_link_element.getAttribute(GliderLinkProxy.GLIDER_NAME_ATTRIBUTE)
        if(glider_link_element.hasAttribute(GliderLinkProxy.PORT_ATTRIBUTE)):
            self.port = glider_link_element.getAttribute(GliderLinkProxy.PORT_ATTRIBUTE)
        if(glider_link_element.hasAttribute(GliderLinkProxy.DEVICE_ATTRIBUTE)):
            self.device = glider_link_element.getAttribute(GliderLinkProxy.DEVICE_ATTRIBUTE)
        if(glider_link_element.hasAttribute(GliderLinkProxy.STATUS_ATTRIBUTE)):
            status = glider_link_element.getAttribute(GliderLinkProxy.STATUS_ATTRIBUTE)
            self.is_docked = (status == GliderLinkProxy.DOCKED)


    def toXml(self, packet_document):
        '''
        Purpose: Transform this class to its xml representation

        Returns:
          (xml.dom.Element) - An xml representation of this glider link proxy.

          <gliderLink gliderName="darwin" port="ttyUSB0" device="freewave"/>
        '''
        glider_link_element = packet_document.createElement(Packet.GLIDER_LINK_TAG)
        glider_link_element.setAttribute(self.GLIDER_NAME_ATTRIBUTE, self.glider_name)
        glider_link_element.setAttribute(self.PORT_ATTRIBUTE, self.port)
        glider_link_element.setAttribute(self.DEVICE_ATTRIBUTE, self.device)
        return glider_link_element

    def getGliderName(self):
        '''
        Purpose: Return the glider name associated with this glider link.

        Returns:
          (string) - The glider name
        '''
        return self.glider_name

    def getPort(self):
        '''
        Purpose: Return the serial port identifier used by this glider to communicate with the
          dockserver.

        Returns:
          (string) - Serial port identifier used by this glider to communicate with dockserver.
            (eg., ttyUSB0)
        '''
        return self.port

    def getDevice(self):
        '''
        Purpose: Returns the communication device used by the glider to talk with dockserver.

        Returns:
          (string) - freewave, modem, or direct.
        '''
        return self.device

    def isDocked(self):
        '''
        Purpose: Returns the docked / not docked status of this glider link.

        Returns:
          bool - True if this glider link is docked.  Otherwise, False.
        '''
        return self.is_docked

    def __eq__(self, other):
        '''
        Purpose: Defines when two GliderLinkProxy instances are equal.

        Returns:
          (boob) - True if the glider name, serial port identifier, and device type are the same.
        '''
        if type(self) is not type(other):
            return False
        if self.getGliderName() == other.getGliderName() and self.getPort() == other.getPort() and self.getDevice() == other.getDevice():
            return True
        return False

    def __ne__(self, other):
        '''
        Purpose: Defines when two GliderLinkProxy instances are not equal.

        Returns:
          (boob) - True if this instance is not equal to the passed instance.
        '''
        return not self.__eq__(other)

    def __cmp__(self, other):
        '''
        Purpose: compare two instances of GliderLinkProxy based only on their device type.
          'direct' > 'freewave' > 'modem'

        Returns:
          (int) - 0 if equal device types.  -1 if self device < other device. otherwise, return 1
        '''
        if type(self) is not type(other):
            raise TypeError('Can not compare different types: ' + str(type(self) + ' and ' + str(type(other))))
        if self.getDevice() == other.getDevice:
            return 0
        if self.getDevice() < other.getDevice():
            return 1
        else:
            return -1

    def __str__(self):
        '''
        Purpose: Returns a string representation of this glider link proxy.
        '''
        return 'Name: ' + self.getGliderName() + ' Serial Port: ' + self.getPort() + ' Device: ' + self.getDevice() + ' Docked: ' + str(self.isDocked())

###############################################################################
# This class represents information about generic exceptions releated to dockserver
# and glider packet functionality.
#
class PacketError(Exception):
    '''
    Attributes:
      source - (object) the source object of the exception.
      message - (string) a description of the exception.
      base_type - (string) exception trace information
      base_value - (string) exception trace information
    '''
    # Class constants

    ###########################################################################
    # Create a new exception.
    #
    # Arguments:
    #  source - (object) The source of the exception.
    #  message - (string) A description of the exception error.
    #
    def __init__(self, source, message):
        self.source = source
        self.message = message
        self.base_type = str(sys.exc_info()[0])
        self.base_value = str( sys.exc_info()[1])

    ###########################################################################
    # Returns the source object of this exception.
    #
    # Returns:
    #  An object instance.
    #
    def getSource(self):
        return self.source

    ###########################################################################
    # Returns a description of this exception as a string
    #
    # Returns:
    #  A description of this exception as a string.
    #
    def getMessage(self):
        return self.message

    ###########################################################################
    # Returns a string representation of this exception.
    #
    # Returns:
    #   string - description of this exception
    #
    def __str__(self):
        return 'Message: ' + self.message + ' Base Type: ' + self.base_type + ' Base Value: ' + self.base_value
