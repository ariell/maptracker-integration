# track.py:
# collect glider positions from dockserver
# 2018-12-10 dpingal@teledyne.com Added surface, mission, sensors messages
# 2018-12-15 dpingal@teledyne.com parse GPS from dialog
import calendar
import proxy_config
import time
import proxy
from pprint import pprint

# slocum represents lat / lon as degrees / minutes dddmm.mmm
def dm2deg(dm):
    deg = int(dm / 100.0)
    frac = (dm - deg * 100.0) / 60.0
    return deg + frac

class GliderClient(object):
    lastFix = {}

    def __init__(self, glider, data_callback=None):
        self.glider_proxy = glider
        # Subscribe to GliderProxy events
        self.glider_proxy.addSubscriber(proxy.GliderProxy.ON_DIALOG, self.onDialog)        
        self.glider_proxy.addSubscriber(proxy.GliderProxy.ON_DOCK, self.onDock)
        self.glider_proxy.addSubscriber(proxy.GliderProxy.ON_MISSION_STATUS, self.onMissionStatus)
        self.glider_proxy.addSubscriber(proxy.GliderProxy.ON_POSITION, self.onPosition)
        self.glider_proxy.addSubscriber(proxy.GliderProxy.ON_SENSORS, self.onSensors)
        self.data_callback = data_callback
        self.collect_dialog = []


    def publish(self, proxy, data):
        data['name'] = proxy.getName()
        if self.data_callback is not None:
            self.data_callback(data)
    
    def openConnection(self):
        self.glider_proxy.openConnection()

    def closeConnection(self):
        # Unsubscribe to events before closing down connection.
        self.glider_proxy.removeSubscriber(proxy.GliderProxy.ON_DIALOG, self.onDialog)                
        self.glider_proxy.removeSubscriber(proxy.GliderProxy.ON_DOCK, self.onDock)
        self.glider_proxy.removeSubscriber(proxy.GliderProxy.ON_MISSION_STATUS, self.onMissionStatus)          
        self.glider_proxy.removeSubscriber(proxy.GliderProxy.ON_POSITION, self.onPosition)
        self.glider_proxy.removeSubscriber(proxy.GliderProxy.ON_SENSORS, self.onSensors)        
        self.glider_proxy.closeConnection()

    def onDock(self, proxy, is_docked):
        self.publish(proxy, { 'surface' : is_docked })
        
    def onDialog(self, proxy, glider_link_proxy, dialog):
        for c in dialog:
            self.collect_dialog.append(c)
            if c == '\n':
                line = ''.join(self.collect_dialog).strip()
                self.collect_dialog = []
                pos = line.find('GPS Location:')
                if pos >= 0:
                    loc = line[pos:].split()
                    name = proxy.getName()
                    lat = dm2deg(float(loc[2]))
                    lon = dm2deg(float(loc[4]))
                    data = {'name': name, 'lat': lat, 'lon': lon}
                    self.publish(proxy, data)
                    

    def onMissionStatus(self, proxy, mission_status):
        mission_name = mission_status.getMissionName()
        since = calendar.timegm(mission_status.getDateTime().timetuple())
        self.publish(proxy, { 'mission' : mission_name, 'since' : since })
        
    def onSensors(self, proxy, surface_sensors):
        result = {}
        for sensor in surface_sensors.getSensors().values():
            name = sensor.getName()[2:]
            result[name] = {'value' : sensor.getValue(),
                'unit' : sensor.getUnit(),
                'age' : sensor.getSecondsAgo() }
        self.publish(proxy, { 'sensors' : result })
        
    def onPosition(self, proxy, position):
        name = proxy.getName()      
        # position looks like: ' 4214.500 N -7004.198 E  Sun Dec 09 00:34:11 2018'
        # Split it...
        fields = str(position).split()

        # then reassemble time part, convert, and save it
        timestamp = calendar.timegm(time.strptime(' '.join(fields[4:])))
        if name in GliderClient.lastFix and timestamp != GliderClient.lastFix[name]:

            # convert formats for lat/lon
            lat = dm2deg(float(fields[0]))
            lon = dm2deg(float(fields[2]))
            data = {'name': name, 'lat': lat, 'lon': lon}
            self.publish(proxy, data)

        GliderClient.lastFix[name] = timestamp


def start_glider_receiver(dockserver, glider_names, data_callback=None):
    # Create a new dockserver proxy
    test_dock_proxy = proxy.DockProxy(dockserver, 6564)

    # Open a connection and do not subscribe to its event log events.
    test_dock_proxy.openConnection()

    # Get the glider proxys corresponding to gliders managed by the actual dockserver.
    # Note this is a synchronous call.  It returns after dockserver has responded to the
    # request for it's managed gliders.
    glider_proxys = test_dock_proxy.getGliders()

    clients = {}
    # Ask user for glider to connect
    for name in glider_names:
        if name in glider_proxys:
            # If the user enters a managed glider, then
            user_glider_proxy = glider_proxys[name]
            # Associate the glider proxy with its 'user interface'
            clients[name] = GliderClient(user_glider_proxy, data_callback=data_callback)
            clients[name].openConnection()
        else:
            print ('Not a managed glider: ' + name)

    # wait for done, close everything
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        for client in clients.values():
            client.closeConnection()
        test_dock_proxy.closeConnection()


def dummy_callback(data):
    print("Received glider data:")
    pprint(data)


if __name__ == '__main__':
    try:
        start_glider_receiver(proxy_config.dockserver, proxy_config.gliders, data_callback=dummy_callback)
    except proxy.ProxyError as p_error:
        print (str(p_error))
