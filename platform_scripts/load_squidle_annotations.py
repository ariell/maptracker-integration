import argparse
from time import sleep

import re
from platform_tools.platformdata import post_data
from geojson import Point, FeatureCollection, LineString, Feature
import pandas as pd
import os
import requests
import traceback
from PIL import Image
#from io import BytesIO
import urllib, cStringIO
from datetime import datetime


SQ_URL = "http://10.23.10.205"
#DATE="2018-12-10"
#USER="camilli"
MT_UPLOADS = "/home/mt/themaptracker/uploads"
TS_FORMAT = "%Y%m%dT%H%M%S.%f"


#datetime.strptime('Jun 1 2005  1:33PM', '%b %d %Y %I:%M%p')
LAST_TS = 0

#API_GET_ANNOTATION_SETS='/api/annotation_set?q={"filters":[{"name":"created_at","op":"gt","val":"%s"},{"name":"user","op":"has","val":{"name":"username","op":"eq","val":"%s"}}]}'
API_GET_ANNOTATION_SETS='/api/annotation_set?q={"filters":[{"name":"created_at","op":"gt","val":"%s"}]}&results_per_page=100'
API_EXPORT_ANNOTATION_SET = '/api/annotation_set/%d/export?format=json&file=text&return=pose&return=mediapaths&return=labelinfo&filter=&filter=has-label&transform=aggregate'
API_EXPORT_EVENTS = '/api/media_collection/%d/export?format=json&file=text&return=pose&return=events&return=mediapaths&filter=has-event'
API_GET_LATEST_CAMPAIGN = '/api/campaign?q={"order_by":[{"field":"created_at","direction":"desc"}],"limit":1,"single":true}'

parser = argparse.ArgumentParser()
parser.add_argument("-k", "--platform_key", default="SQ.ANNOTATIONS", type=str, help="Platform key to post to maptracker")
parser.add_argument("-c", "--color", default="yellow", type=str, help="Colour to plot mission")
parser.add_argument("-p", "--port", default=8080, type=int, help="Port for maptracker server")
parser.add_argument("-u", "--url", default="http://10.23.10.205", type=str, help="URL for maptracker server (format: http://ipaddress)")
parser.add_argument("-d", "--delay", default=0, type=int, help="Continuously update at defined delay (otherwise just once)")    # Set options


def annotation2feature(r):
    ll = [r["pose_lon"], r["pose_lat"]]
    point = Point(ll)
    return Feature(geometry=point, properties={
        "options": {
            "radius": 3, "fillColor": r.get("class_color", "red"),
            "color": "black", "weight": 1, "opacity": 0.5,
            "fillOpacity": 0.5
        },
        "info": {
            "Depth": r["pose_dep"], "Alt": r["pose_alt"], "Lat":r["pose_lat"],
            "Lon":r["pose_lon"], "  Label": r["class_name"], " Comment":r["comment"],
            "Set": r["set"], "Time": r["pose_timestamp"],
            "  IMG": "<img style='width:100%;min-width:200px' src='/uploads"+r["media_path"]+"'>"
        }
    })

def event2feature(r):
    ll = [r["pose_lon"], r["pose_lat"]]
    point = Point(ll)
    return Feature(geometry=point, properties={
        "options": {
            "radius": 6, "fillColor": "yellow",
            "color": "white", "weight": 1, "opacity": 0.5,
            "fillOpacity": 0.5
        },
        "info": {
            "Depth": r["pose_dep"], "Alt": r["pose_alt"], "Lat":r["pose_lat"],
            "Lon":r["pose_lon"], " Event type": r["event_type"], " Event":r["event"],
            "Time": r["event_timestamp"],
            "  IMG": "<img style='width:100%;min-width:200px' src='/uploads"+r["media_path"]+"'>"
        }
    })

def process_annotations(platform_key, port, url, color):
    #get_url = SQ_URL + API_GET_ANNOTATION_SETS % (date, username)
    last_campaign = requests.get(SQ_URL + API_GET_LATEST_CAMPAIGN).json()
    annotation_set_url = SQ_URL + API_GET_ANNOTATION_SETS % (last_campaign.get("created_at"))
    set_list = requests.get(annotation_set_url).json()
    set_ids = [s.get("id") for s in set_list.get("objects",[])]                             # set of annotation_set ids
    col_ids = list(set([s.get("media_collection_id") for s in set_list.get("objects",[])])) # unique collection ids

    frames_list = []
    annotation_features = []
    for id in set_ids:
        try:
            set_data = requests.get(SQ_URL + API_EXPORT_ANNOTATION_SET % (id)).json()
            set_name = set_data.get("name")
            for a in set_data.get("objects"):
                if isinstance(a.get("pose_lat"), float) and isinstance(a.get("pose_lon"), float):
                    a["set"] = set_name
                    annotation_features.append(annotation2feature(a))
                    frames_list.append(a.get("media_path"))
        except Exception as ex:
            print("WARNING: {}\n{}".format(ex, a))

    for id in col_ids:
        evt_data = requests.get(SQ_URL + API_EXPORT_EVENTS % (id)).json()
        for e in evt_data.get("objects"):
            try:
                if isinstance(a.get("pose_lat"), float) and isinstance(a.get("pose_lon"), float):
                    annotation_features.append(event2feature(e))
                    frames_list.append(e.get("media_path"))
            except Exception as ex:
                print("WARNING: {}\n{}".format(ex, e))

    annotation_features = FeatureCollection(annotation_features)
    frames_list = list(set(frames_list))

    create_thumbnails(frames_list)

    post_data(platform_key, annotation_features, data_type="mission", maptracker_port=port, maptracker_url=url)

    # current_missions = missions_to_geojson(CURRENT_MP_PATTERN.format(base_path), color=color)
    # #print(current_missions)
    # post_data(platform_key, current_missions, data_type="mission", maptracker_port=port, maptracker_url=url)


def create_thumbnails(frames_list):
    for f in frames_list:
        fname = MT_UPLOADS+f
        if not os.path.isfile(fname):
            print("Saving frame: {}...".format(fname))
            if not os.path.isdir(os.path.dirname(fname)):
                os.makedirs(os.path.dirname(fname))
            file = cStringIO.StringIO(urllib.urlopen(SQ_URL+f).read())
            im = Image.open(file)

            # img = Image.open(BytesIO(requests.get(SQ_URL+f).content))
            # im = Image.open(img)
            # convert to thumbnail image
            im.thumbnail((200, 200), Image.ANTIALIAS)
            # don't save if thumbnail already exists
            # prefix thumbnail file with T_
            im.save(fname, "JPEG")

    print ("Now rsync stuff...")


def main():
    # Parse input args
    args = parser.parse_args()

    if args.delay > 0:
        while True:
            try:
                process_annotations(args.platform_key, args.port, args.url, args.color)
            except Exception as e:
                traceback.print_exc()
                print ("*** ERROR: cannot process mission {}".format(e))
            sleep(args.delay)
    else:
        process_annotations(args.platform_key, args.port, args.url, args.color)

if __name__ == "__main__":
    main()
