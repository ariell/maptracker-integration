from random import random
import requests
from time import sleep, time
from platform_tools.platformdata import APIPlatform, get_args
from platform_tools.generate_fake_data import get_fake_log_message, get_fake_mission, get_fake_platform_data
import thread


# Platform key: must be unique
platform_key = "ROV.SUBASTIEN"
data_url = "http://10.23.10.205:8085/framegrabber/data"

NOM_SPEED = 0.2   # nominal speed in m/s
NOM_UNCER = 50.0  # nominal uncertainty m

class SOISubastienPlatform(APIPlatform):
    def __init__(self, **kwargs):
        APIPlatform.__init__(self, **kwargs)
        self.last_data_ts = 0

    def get_http_data(self, platform_key):
        """
        NOTE: This should be a thread that subscribes to a UDP feed or similar to receive nav updates. It should
        construct the nav data object as done here, but when new data is received.
        """
        while True:
            try:
                response = requests.get(data_url).json()
                data = response.get("data", {})
                ts = data.get("last_pose_time") or 0
                lat = data.get("pose", {}).get("lat")
                lon = data.get("pose", {}).get("lon")
                if ts > self.last_data_ts and lat and lon:
                    platform_data = {
                        # All alert key-value pairs are optional. For no alerts, include empty dict, ie {}
                        # The keys can be anything, but keep them short for display.
                        # The values need to be int 0/1, with 1=on, 0=off.
                        "alert": {},

                        # required: lat, lon, heading
                        # optional, but required for dash display: roll, pitch, bat
                        # optional, speed, uncertainty, alt, depth
                        # all values must be floats
                        "pose": {
                            "lat": round(lat, 8),  # float, decimal degrees
                            "lon": round(lon, 8),  # float, decimal degrees
                            "depth": round(data.get("pose", {}).get("dep", 0), 2),  # float, decimal degrees
                            "alt": round(data.get("pose", {}).get("alt", 0), 2),  # float, decimal degrees
                            "heading": round(data.get("posedata", {}).get("heading", 0), 2),  # float, degrees, range: 0:360
                            "pitch": round(data.get("posedata", {}).get("pitch", 0), 2),  # float, degrees
                            "roll": round(data.get("posedata", {}).get("roll", 0), 2),  # float, degrees
                            "speed": NOM_SPEED,  # float, m/s
                            "uncertainty": NOM_UNCER  # float, m
                        },

                        # All stat key-value pairs are optional. For no alerts, include empty dict, ie {}
                        # optional, but required for dash display: bat
                        # The keys can be anything, but keep them short for display.
                        # The values can be anything, with the exception of bat, which must be an int
                        "stat": {
                            "conductivity": data.get("posedata", {}).get("conductivity"),
                            "oxygen": data.get("posedata", {}).get("oxygen"),
                            "oxygen_temp": data.get("posedata", {}).get("oxygen_temp"),
                            "salinity": data.get("posedata", {}).get("salinity"),
                            "temperature": data.get("posedata", {}).get("temperature"),
                        }
                    }
                    self.update_platform_data(platform_key, platform_data)
                    self.last_data_ts = ts

            except Exception as e:
                platform.log_platform_message(platform_key, "danger", "ERROR updating vehicle data: {}".format(e))

            sleep(1)


if __name__ == "__main__":

    # get cli args
    args = get_args()

    # Instantiate class
    platform = SOISubastienPlatform(maptracker_url=args["url"], maptracker_port=args["port"], command_server_port=args["command_port"])

    # Start data threads
    platform.get_http_data(platform_key)
