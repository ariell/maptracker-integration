from platform_tools.platformdata import APIPlatform, parser
from supportlibs.docserverlib.track import start_glider_receiver
from supportlibs.docserverlib.proxy_config import dockserver as default_docserver, gliders as default_glider_names
from pprint import pprint

from time import time

# Platform key: must be unique
PLATFORM_PREFIX = "SLOCUM."
# default_docserver = "10.23.9.101"
# default_glider_names = ['kilroy', 'nemesis']

NOMINAL_SPEED = 0.5  # m/s
NOMINAL_UNC = 10.0  # m

DEBUG = False

parser.add_argument("-d", "--docserver", default=default_docserver, type=str, help="IP for docserver (format: X.X.X.X)")
parser.add_argument("-g", "--gliderid", help="Add a glider name/ID to track", default=[],  action='append', dest='glider_names')
parser.add_argument("--debug", help="Debug", default=DEBUG,  action='store_true')

default_data_structure = dict(
    alert={},
    pose=dict(lat=None, lon=None, speed=NOMINAL_SPEED, uncertainty=NOMINAL_UNC),
    stat={}
)

class SlocumPlatform(APIPlatform):
    def __init__(self, docserver=default_docserver, glider_names=default_glider_names, **kwargs):
        self.docserver = docserver
        self.glider_names = glider_names
        APIPlatform.__init__(self, **kwargs)
        self.data_cache = {}

    def update_platformdata(self, data):
        platform_prefix = PLATFORM_PREFIX + data.get("name", "glider")

        if platform_prefix not in self.data_cache:
            self.data_cache[platform_prefix] = default_data_structure.copy()

        platform_data, do_post = self.parse_data(data, self.data_cache[platform_prefix])
        if do_post:
            self.update_platform_data(platform_prefix, self.data_cache[platform_prefix])

    def parse_data(self, datarecv, platform_data):
        print("***Received data from: {}".format(datarecv.get("name", "glider")))
        if DEBUG:
            print(datarecv)
        sensor_data = datarecv.get("sensors", None)
        if sensor_data is not None:
            print("{}: Got SENSOR message!".format(time()))
            stat = {k: "{} {}".format(round(val["value"],1), val["unit"]) for k,val in sensor_data.iteritems()}
            alert = {}
            if "leakdetect_voltage" in sensor_data:
                alert["leak"] = sensor_data.get("leakdetect_voltage").get("value") < 2.4
            if "battery" in sensor_data:
                stat["bat"] = int(min(100,(sensor_data.get("battery").get("value") - 12)/(17-12)*100))
                alert["bat<12V"] = sensor_data.get("battery").get("value") <= 12
            platform_data["stat"].update(stat)
            platform_data["alert"].update(alert)

        if "mission" in datarecv:
            print("{}: Got MISSION message!".format(time()))
            platform_data["stat"]["mission"] = datarecv.get("mission", None)

        if "lat" in datarecv and "lon" in datarecv:
            print("{}: Got POSITION message!".format(time()))
            platform_data["pose"]["lat"] = round(datarecv.get("lat"),8)  # float, decimal degrees
            platform_data["pose"]["lon"] = round(datarecv.get("lon"),8)  # float, decimal degrees

        platform_data["stat"]["dockserver"] = self.docserver

        do_post = platform_data.get("pose",{}).get("lat") is not None and platform_data.get("pose",{}).get("lon") is not None


        if DEBUG:
            print("\n***Parsed data: {}".format(datarecv.get("name", "glider")))
            pprint(platform_data)
        return platform_data, do_post




    def start_threads(self):
        start_glider_receiver(self.docserver, self.glider_names, data_callback=self.update_platformdata)


if __name__ == "__main__":
    # get cli args
    args = parser.parse_args()

    DEBUG = args.debug

    print(args.glider_names)

    # Instantiate class
    platform = SlocumPlatform(
        maptracker_url=args.url, maptracker_port=args.port, command_server_port=args.command_port,
        docserver=args.docserver, glider_names=args.glider_names
    )

    # Start data threads
    while True:
        try:
            platform.start_threads()
        except KeyboardInterrupt:
            break
        except Exception as e:
            platform.log_platform_message("TDGLIDER", "danger", "ERROR with glider dockserver: {}".format(e))
            print("ERROR: {}, trying again.".format(e))
